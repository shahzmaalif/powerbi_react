import './App.css';
import React, { useState, useEffect, useRef } from 'react';
import {BrowserRouter as Router, Routes, Route, useNavigate } from "react-router-dom";
import Login from './pages/login';
import Otp from './pages/Otp';
import Mainpage from './pages/Mainpage';
import Report from './pages/Report';
import FigmaLogin from './pages/FigmaLogin';
import FigmaOtp from './pages/FigmaOtp';
import ReactGA from 'react-ga';
import TagManager from 'react-gtm-module'
import NewFigmaLogin from './pages/NewFigmaLogin';
import NewFigmaSIgnIn from './pages/NewFigmaSIgnIn';
import NewFigmaOTP from './pages/NewFigmaOTP';
import NewReport from './pages/NewReport';
import NewMainPage from './pages/NewMainPage';
import Frontpage from './pages/Frontpage';
import Article from './pages/Article';
import ArticleDetail from './pages/ArticleDetail';
import Searchpage from './pages/Searchpage';
import PowerbiCompany from './pages/PowerbiCompany';
import DjangoEmbed from './pages/DjangoEmbed';
import Internet from './pages/Internet';
import Front from './pages/Front';
import MyProvider from './utils/contexts/MyProvider';
import { Provider } from 'react-redux';
import {store} from './utils/redux/store'
import NewFigmaSignUp from './pages/NewFigmaSignUp';
import ApprovedRequest from './pages/ApprovedRequest';
import Contact from './pages/Contact';
import FrontPage3 from './pages/FrontPage3';
import Industries from './pages/Industries';
import Signup from './pages/Signup';
import SignIn from './pages/SignIn';
import Companies from './pages/Companies';
import Brands from './pages/Brands';
import Otp3 from './pages/Otp3';
import Sub from './pages/Sub';
import Report3 from './pages/Report3';
import Pdf from './pages/Pdf';
import Invitations from './pages/Invitations';
import CompanyBoard from './pages/CompanyBoard';
import RedirectionPage from './pages/RedirectionPage';
import Testing from './pages/Testing';
import clevertap from 'clevertap-web-sdk';
import { CLEVERTAP_ACC_ID, AUTO_LOGOUT_TIME } from "./constants/constants";
import RevenuePool from './pages/RevenuePool';
import CompanyListing from './pages/CompanyListing';
import Portfolio from './pages/Portfolio';
import AdminConsole from './pages/AdminConsole';
import Logtable from './components/ChangeLogs/logtable';

const AllRoutes = React.memo(() => {
  let [Email, setEmail] = useState('');
  let [RealEmail, setRealEmail] = useState('');
  let [Token, setToken] = useState('');
  let [IsAdmin, setIsAdmin] = useState(false)
  let [IsSignedIn, setIsSignedIn] = useState(false)
  let [IsLoggedIn, setIsLoggedIn] = useState(false)
  let [Reportname, setReportname] = useState('')
  let [ReportVersionID, setReportVersionID] = useState('')
  let [clientID, setClientID] = useState(0)


  
  useEffect(() => {
    clevertap.init(CLEVERTAP_ACC_ID);
    console.log("Clevertap - Acc ID -", CLEVERTAP_ACC_ID);
    clevertap.spa = true;
    clevertap.enablePersonalization = true;
    const setUserData = () => {
      clevertap.spa = true;
      clevertap.enablePersonalization = true;
      clevertap.privacy.push({ optOut: false });
      clevertap.privacy.push({ useIP: true });

      // if (navigator.geolocation) {
      //   navigator.geolocation.getCurrentPosition(
      //     (position) => {
      //       clevertap.privacy.push({ optOut: false });
      //       clevertap.privacy.push({ useIP: true });
      //     },
      //     (error) => {
      //       clevertap.privacy.push({ optOut: false });
      //       clevertap.privacy.push({ useIP: false });
      //     }
      //   );
      // } else {
      //   console.log("Error - Geolocation is not supported by this browser.");
      // }
    };
    const timeoutId = setTimeout(() => {
      setUserData();
    }, 500);
    return () => clearTimeout(timeoutId);
  }, []);


  let userLogin = (email) => {
    setIsLoggedIn(true);
    setEmail(email)
    // setPseudoEmail(pseudo_email)
  }

  let getRealEmail=(real_email)=>{
    setRealEmail(real_email);
  }

  let getReportName = (repname) =>{
    setReportname(repname);
    console.log('reportnameapp = ', Reportname )
  }

  let getReportVersionID = (id) => {
    setReportVersionID(id)
  }

  let SignedInStatus = (status) => {
    setIsSignedIn(status)
  }

  let setTokenVal = (Token)=>{
    setToken(Token)
  }
  
  let setClientId = (clientID)=>{
    setClientID(clientID)
  }

  // prod  tracking id ga
  // const TRACKING_ID = 'UA-241888110-1'

// local tracking id
  const TRACKING_ID = 'UA-241614253-1'

  // prod args
//   const tagManagerArgs = {
//     gtmId: 'GTM-MXCJ6SF'
// }

// testting args
const tagManagerArgs = {
  gtmId: 'GTM-P3W82CC'
}
  TagManager.initialize(tagManagerArgs)

  // react ga prod
  // ReactGA.initialize(TRACKING_ID)


  return (
    <Routes>
      {/* <Route path = "/" element = {<Login userLogin={userLogin}/>}></Route> */}
      <Route path = "/signin" element = {<FigmaLogin userLogin={userLogin} getRealEmail={getRealEmail} setClientID={setClientID}/>}></Route>
      <Route path = "/signup3" element = {<NewFigmaSignUp userLogin={userLogin} getRealEmail={getRealEmail} setClientID={setClientID}/>}></Route>
      {/* <Route path = '/login' element = {<NewFigmaLogin/>}></Route> */}
      <Route path = '/signin4' element={<NewFigmaSIgnIn/>}  ></Route>
      <Route path = '/otp4' element = {<NewFigmaOTP/>}></Route>
      <Route path = '/frontpage' element = {<Front/>}></Route>
      {/* <Route path = "/otp" element = {<Otp Token={Token} getReportName={getReportName}/>}/> */}
      {/* <Route path = '/signup' element = {<Front/>}></Route> */}
      <Route path = "/FigmaOtp" element = {<FigmaOtp email = {RealEmail} IsLoggedIn={IsLoggedIn} SignedInStatus={SignedInStatus} setTokenVal = {setTokenVal}/>}/>
      {/* <Route path = "/mainpage" element = {<Mainpage Email = {'maruti@redseerconsulting.com'} Token={Token} Reportname={Reportname} IsAdmin = {IsAdmin} getReportVersionID = {getReportVersionID}/>}/> */}
      {/* <Route path = "/mainpage" element = {<Mainpage email = {RealEmail} pseudo_email={Email} IsSignedIn={IsSignedIn} Token = {Token} getReportName={getReportName} clientID={clientID}/>}/> */}
      <Route path = "/report" element = {<Report Token={Token} email = {RealEmail} pseudo_email={Email} ReportName = {Reportname}/>}/>
      {/* <Route path = 'newreport' element = {<NewReport/>}></Route> */}
      <Route path = '/newmainpage' element = {<NewMainPage/>}></Route>
      <Route path = '/search' element = {<Searchpage/>}></Route>
      <Route path = '/article' element = {<Article/>}></Route>
      <Route path='/articled' element = {<ArticleDetail/>}></Route>
      <Route path = '/powerbicompany' element = {<PowerbiCompany/>}></Route>
      <Route path = '/djangoembed' element = {<DjangoEmbed/>}></Route>
      <Route path = '/approvedrequest' element = {<ApprovedRequest/>}></Route>
      <Route path = '/contact' element = {<Contact/>}></Route>
      <Route path='/' element = {<FrontPage3/>}></Route>
      <Route path='/industries' element = {<Industries/>}></Route>
      <Route path='/signup' element = {<Signup/>}></Route>
      <Route path='/signin3' element = {<SignIn/>}></Route>
      <Route path = '/companies' element = {<Companies/>}></Route>
      <Route path = '/brands' element = {<Brands/>}></Route>
      <Route path = '/otp' element = {<Otp3/>}></Route>
      <Route path = '/Sub3' element = {<Sub/>}></Route>
      <Route path = '/Report3' element={<Report3/>}></Route>
      <Route path = '/pdf' element={<Pdf/>}></Route>
      <Route path = '/invitations' element={<Invitations/>}></Route>
      <Route path = '/redirect' element={<RedirectionPage/>}></Route>
      <Route path = '/test' element={<Testing/>}></Route>
      <Route path = '/company-board' element={<CompanyBoard/>}></Route>
      <Route path = '/revpool' element={<RevenuePool/>}></Route>
      <Route path = '/company-listing' element={<CompanyListing/>}></Route>
      <Route path = '/portfolio' element={<Portfolio/>}></Route>
      <Route path = '/changeLogs' element={<Logtable/>}></Route>
      <Route path = '/admin-console' element={<AdminConsole/>}></Route>
    </Routes>
  )
})

const MainApp = () => {
  const timeoutRef = useRef(null);
  const navigate = useNavigate();

  let handleSignOut = () => {
    let prop_email = window.localStorage.getItem("email");
    let prop_token = window.localStorage.getItem("token");
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/logout/?email=${prop_email}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${prop_token}`,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        console.log("logout= ", res);
      })
      .catch((error) => {
        console.error(error);
      });
    localStorage.clear();
    navigate("/");
  };

  const delayedFunction = () => {
    console.log("Inactive session, logging out ...");
    handleSignOut();
  };

  const resetLogoutTimer = (delay) => {
    if (window.localStorage.getItem("loginStatus") === "true" && delay) {
      clearTimeout(timeoutRef.current);
      // console.log("User interacting-", delay);
      timeoutRef.current = setTimeout(() => {
        delayedFunction();
      }, delay);
    }
  };

  const handleUserActivity = () => {
    resetLogoutTimer(AUTO_LOGOUT_TIME);
  };

  useEffect(() => {
    resetLogoutTimer(AUTO_LOGOUT_TIME);
    window.addEventListener("mousemove", handleUserActivity);
    window.addEventListener("keydown", handleUserActivity);
    window.addEventListener("click", handleUserActivity);
    window.addEventListener("scroll", handleUserActivity);

    return () => {
      window.removeEventListener("mousemove", handleUserActivity);
      window.removeEventListener("keydown", handleUserActivity);
      window.removeEventListener("click", handleUserActivity);
      window.removeEventListener("scroll", handleUserActivity);
      clearTimeout(timeoutRef.current);
    };
  }, []);

  return <AllRoutes />;
};

function App() {
  return (
    <Provider store = {store}>
      <MyProvider>
        <Router>
          <MainApp/>
        </Router>
      </MyProvider>
    </Provider>

  );
}

export default App;
