import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { increment, decrement } from './slices';  // Import the counter slice actions
import { fetchData, fetchapiData } from './slices';  // Import the async thunk action
import { saveToDb, getFromDb } from './idb';  // Import the idb utility functions

function CounterAndFeatureComponent() {
  const dispatch = useDispatch();
  const [localData, setLocalData] = useState(null);

  // Using selectors to get data from Redux state
  const counter = useSelector(state => state.counter);
  const featureData = useSelector(state => state.feature.data);
  const featureStatus = useSelector(state => state.feature.status);
  const featureDataAPI = useSelector((state) => state.featureData);
  
  useEffect(() => {
    // Dispatch the async thunk action to fetch data
    dispatch(fetchData());

    // Example: Save the counter to IndexedDB when it changes
    saveToDb('counter', counter);
  }, [counter, dispatch]);

  useEffect(() => {
    // Example: Load the counter from IndexedDB on component mount
    async function loadCounterFromDb() {
      const savedCounter = await getFromDb('counter');
      if (savedCounter !== undefined) {
        // This is a simplistic example and wouldn't work as is because we're not dispatching an action.
        // In a real scenario, you'd dispatch an action to set the counter state in Redux.
        console.log("Loaded counter from DB:", savedCounter);
      }
    }
    loadCounterFromDb();
  }, [dispatch]);

  return (
    <div>
      <h2>Counter</h2>
      <button onClick={() => dispatch(increment())}>Increment</button>
      <button onClick={() => dispatch(decrement())}>Decrement</button>
      <p>Counter Value: {counter}</p>

      <h2>Feature Data</h2>
      {featureStatus === 'loading' && <p>Loading...</p>}
      {featureStatus === 'succeeded' && (
        <div>
          {/* Display the feature data here, for simplicity we're just converting it to JSON string */}
          <pre>{JSON.stringify(featureData, null, 2)}</pre>
        </div>
      )}
      {featureStatus === 'failed' && <p>Error loading data!</p>}
    </div>
  );
}

export default CounterAndFeatureComponent;
