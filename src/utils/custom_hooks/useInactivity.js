import { useEffect } from 'react';
import React from 'react'
import MyContext from '../contexts/MyContext';

const useInactivityLogout = (logoutFunction, duration = 60 * 60 * 1000) => {
  const { lastActivity, setLastActivity } = React.useContext(MyContext);

  useEffect(() => {
    const checkInactivity = () => {
      const currentTime = Date.now();
      if (currentTime - lastActivity > duration) {
        logoutFunction();
      }
    };

    const interval = setInterval(checkInactivity, duration);

    return () => clearInterval(interval);
  }, [lastActivity, logoutFunction, duration]);

  useEffect(() => {
    const activityEvents = ['mousedown', 'mousemove', 'keydown', 'scroll', 'touchstart'];

    const refreshLastActivity = () => setLastActivity(Date.now());

    activityEvents.forEach((event) => window.addEventListener(event, refreshLastActivity));

    return () => {
      activityEvents.forEach((event) => window.removeEventListener(event, refreshLastActivity));
    };
  }, [setLastActivity]);
};

export default useInactivityLogout;
