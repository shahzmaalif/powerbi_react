import React from "react";
import styled from "styled-components";

const PiechartIcon = ({
  width = 18,
  height = 18,
  color = "#20a6ff",
  rotate = 0,
  onIconClick = () => null,
}) => {
  return (
    <IconWrapper rotate={rotate} onClick={onIconClick}>
      <svg
        viewBox="0 0 24 24"
        width={`${width}px`}
        height={`${height}px`}
        fill="none"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <path d="M21.21 15.89A10 10 0 1 1 8 2.83" stroke={`${color}`} />
        <path d="M22 12A10 10 0 0 0 12 2v10z" stroke={`${color}`} />
      </svg>
    </IconWrapper>
  );
};

export default PiechartIcon;

const IconWrapper = styled.div`
  display: flex;
  transform: rotate(${(props) => props.rotate}deg);
  :hover {
    cursor: pointer;
  }
`;
