import React from "react";
import GridIcon from "../svg/GridIcon";
import ListIcon from "../svg/ListIcon";

const ListGridToggle = ({ onToggle, value }) => {
  const handleToggle = () => {
    onToggle(value === "left" ? "right" : "left");
  };

  return (
    <div style={{ display: "flex", alignItems: "center" }}>
      <div style={{ marginRight: "8px" }}>List</div>
      <div style={{ marginRight: "8px" }}>
        <ListIcon />
      </div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          marginRight: "8px",
          cursor: "pointer",
        }}
        onClick={handleToggle}
      >
        <div
          style={{
            width: "40px",
            height: "20px",
            background: "#0099FF",
            display: "flex",
            alignItems: "center",
            justifyContent: value === "right" ? "flex-end" : "flex-start",
            transition: "0.3s",
            borderRadius: "10px",
            padding: "1px",
          }}
        >
          <div
            style={{
              width: "18px",
              height: "18px",
              background: "white",
              borderRadius: "9px",
              boxShadow: "2px 2px 4px 0px #00000040 inset",
            }}
          />
        </div>
      </div>
      <div style={{ marginRight: "8px" }}>
        <GridIcon />
      </div>
      <div style={{ marginRight: "8px" }}>Grid</div>
    </div>
  );
};

export default ListGridToggle;
