// src/components/MasterTable/ToolTip/index2.js
import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";

const ToolTip = ({ value, toolTip }) => {
  const [isTooltipVisible, setTooltipVisible] = useState(true);
  const [quadrant, setQuadrant] = useState(3);
  const wrapperRef = useRef(null);

  const updateQuadrant = () => {
    if (wrapperRef.current) {
      const rect = wrapperRef.current.getBoundingClientRect();
      const centerX = rect.left + rect.width / 2;
      const centerY = rect.top + rect.height / 2;
      const windowWidth = window.innerWidth;
      const windowHeight = window.innerHeight;
      let newQuadrant = 1;
      if (centerX < windowWidth / 2) {
        newQuadrant = centerY < windowHeight / 2 ? 1 : 3;
      } else {
        newQuadrant = centerY < windowHeight / 2 ? 2 : 4;
      }
      console.log("Quadrant-",newQuadrant, tooltipDimensions)
      setQuadrant(newQuadrant);
    }
  };

  /* popup size */
  useEffect(() => {
    updateQuadrant();
  }, [wrapperRef, isTooltipVisible]);

  const [tooltipDimensions, setTooltipDimensions] = useState({
    width: 0,
    height: 0,
  });
  const popupRef = useRef(null);
  useEffect(() => {
    const updateDimensions = () => {
      const { width, height } = popupRef.current.getBoundingClientRect();
      console.log("DATA____",Math.floor(width),Math.floor(height))
      setTooltipDimensions({
        width: Math.floor(width),
        height: Math.floor(height),
      });
    };
    if (isTooltipVisible && popupRef.current) updateDimensions();
  }, []);

  /* cursor position */
  const [cursorPosition, setCursorPosition] = useState({ x: 100, y: 100 });

  const handleMouseMove = (e) => {
    const wrapperRect = wrapperRef.current.getBoundingClientRect();
    const relativeX = e.clientX - wrapperRect.left - 4;
    const relativeY = e.clientY - wrapperRect.top;

    setCursorPosition({ x: relativeX, y: relativeY });
  };

  useEffect(() => {
    document.addEventListener("mousemove", handleMouseMove);
    return () => {
      document.removeEventListener("mousemove", handleMouseMove);
    };
  }, []);

  return (
    <Wrapper
      ref={wrapperRef}
      onMouseEnter={() => setTooltipVisible(true)}
      onMouseLeave={() => setTooltipVisible(false)}
    >
      {value}
      {isTooltipVisible && toolTip && (
        <PopupWrap
          ref={popupRef}
          quadrant={quadrant}
          width={tooltipDimensions.width}
          height={tooltipDimensions.height}
        >
          {/* <IconDiv> */}
            <TriangleIcon
              ref={popupRef}
              rotation={0}
              quadrant={quadrant}
              width={tooltipDimensions.width}
              height={tooltipDimensions.height}
              x={cursorPosition.x}
              y={quadrant === 1 ? cursorPosition.y : 0}
            />
          {/* </IconDiv> */}
          {toolTip}
        </PopupWrap>
      )}
    </Wrapper>
  );
};

export default ToolTip;

const Wrapper = styled.div`
  position: relative;
  display: inline-block;
  width: 100%;
`;
const PopupWrap = styled.div`
  padding: 4.5px;
  background-color: #ffffff;
  z-index: 5;
  box-shadow: 2px 2px 4px 0px #00000040;
  border: 0.75px solid #ededed;
  border-radius: 5px;
  font-size: 12px;
  font-weight: 600;
  line-height: 16px;
  text-align: center;
  color: #262e40;
  position: absolute;
  cursor: default;
  ${(props) => props.quadrant === 1 && `bottom: -${props.height}px;`}
  ${(props) => props.quadrant === 1 && `left: 0px;`}

  ${(props) => props.quadrant === 2 && `bottom: -${props.height}px;`}
  ${(props) => props.quadrant === 2 && `right: 0px;`}

  ${(props) => props.quadrant === 3 && `top: -${props.height}px;`}
  ${(props) => props.quadrant === 3 && `left: 0px;`}

  ${(props) => props.quadrant === 4 && `top: -${props.height}px;`}
  ${(props) => props.quadrant === 4 && `right: 0px;`}
`;

const IconDiv = styled.div`
  height: 1px;
  background-color: pink;
  width: 100%;
  position: relative;
`;
const TriangleIcon = styled.div`
  width: 0;
  height: 0;
  border-left: 2px solid transparent;
  border-right: 2px solid transparent;
  transform: rotate(${(props) => props.rotation}deg);
  -webkit-clip-path: polygon(50% 0%, 0% 100%, 100% 100%);
  position: relative;
  // top: ${(props) => props.y}px;
  // left: ${(props) => props.x}px;
  ${(props) => props.quadrant === 1 && `bottom: 13px;`}
  ${(props) => props.quadrant === 1 && `left: ${props.x}px;`}

  ${(props) => props.quadrant === 2 && `bottom: 13px;`}
  ${(props) => props.quadrant === 2 && `left: ${  + props.x}px;`}
  padding: 4px;
  background-color: pink;
  z-index: 5;
  cursor: default;
`;
