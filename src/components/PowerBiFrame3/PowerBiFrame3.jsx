import React, { useState, useEffect, useRef, Suspense } from 'react'
import TreeMenu from 'react-simple-tree-menu';
import styled from 'styled-components';
import {  TailSpin } from  'react-loader-spinner'
import { ListGroupItem, ListGroup } from 'reactstrap';
import Modal from 'react-modal';
import {PowerBIEmbed} from 'powerbi-client-react';
import {models} from 'powerbi-client';
import { Navigate, useLocation } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { IoIosArrowForward, IoIosArrowDown } from "react-icons/io";
import DynamicComponent from '../../pages/DynamicComponent'
import DjangoEmbed from '../../pages/DjangoEmbed';
import Frontpage from '../../pages/Frontpage';
import Internet from '../../pages/Internet';
import Subscription from '../../pages/Subscription';
import Sub from '../../pages/Sub';
import MyDropdown from '../DropDown/dropdown';
import LinkDropDown from '../LinkDropDown/LinkDropDown';
import { FaAmazon, FaTrafficLight,FaUsers, FaDeezer,FaAlignLeft, FaKeyboard, FaCity } from 'react-icons/fa';
import{MdOutlineSummarize, MdMonetizationOn, MdInsights, MdDashboard, MdOutlinePersonalVideo} from 'react-icons/md';
import { ImProfile } from "react-icons/im";
import {SiCoveralls, SiSimpleanalytics,SiFlipkart, SiPaytm, SiTata} from "react-icons/si"
import {FiUsers} from "react-icons/fi"
import {AiOutlineFileSearch, AiOutlineMobile , AiTwotoneVideoCamera} from 'react-icons/ai'
import { GiBreakingChain, GiHealthNormal, GiCarWheel, GiClothes,GiMedicines, GiVideoConference, GiHamburgerMenu } from "react-icons/gi";
import { RiMoneyDollarBoxFill } from "react-icons/ri";
// import {GoFileSymlinkDirectory} from 'react-icons/go'
import { BiCategory, BiBookContent, BiCartAlt, BiFridge } from "react-icons/bi";
import { IoFastFood,  } from "react-icons/io5";
import {FaCarAlt, FaBusinessTime ,FaBabyCarriage, FaRegFileAudio, FaTruck} from 'react-icons/fa';
import {BsGear,  BsFillCloudArrowDownFill, BsTag} from 'react-icons/bs'
import MyContext from '../../utils/contexts/MyContext';
import ArticlesList from "../ArticlesList"
import ArticleDetail from "../ArticleDetail"
import {
  ANALYSIS_ENDPOINT,
  CONTENT_ARTICLE_ENDPOINT,
  CONTENT_REPORT_ENDPOINT,
} from "../../constants/constants";
import axios from 'axios';
import QueryFooter from '../QueryFooter/QueryFooter';
import { useSelector } from 'react-redux';
import { selectnewreportaccessData } from '../../utils/redux/slices';
import { useDispatch } from 'react-redux';
import { setNewReportAccessData } from '../../utils/redux/slices';
import { saveToDb } from '../../utils/redux/idb';
import { getFromDb } from '../../utils/redux/idb';
import Fintable from '../FinTable/Fintable';
import BeadGraph from "../BeadGraph";
import RelatedDataSets from "../RelatedDataSets";
import ReadyReckoner from "../ReadyReckoner";
import Testing from '../../pages/Testing';
import ArialStacked from '../ArialStacked';
import WaterFallWalletShare from '../WaterFallWalletShare/WaterFallWalletShare';
import Swot from '../Swot/Swot';
import {
  onQuickLinkClickCT,
  onCompanyProfileClickCT,
  onDownloadExcelClickCT,
  onSectorLoadMoreClickCT,
  onCPLoadMoreClickCT,
  onNewsletterClickCT,
  onReportSubscriptionRequestClickCT,
  onCurrencySwitchClickCT,
  onYearSwitchClickCT,
  onReadContentClickCT,
  onReportPageVisitCT,
} from "../../utils/clevertap";
import { Carousel } from 'react-responsive-carousel';
import MyStackedBarChart from '../Bargraph/bars';

const PowerBiFrame3 = (parent_props) => {
    const [newReportPages, setnewReportPages] = useState([])
    const [allNodes,setallNodes] = useState([])
    const [treearr, setTreearr] = useState([])
    const [treeziparr, setTreeziparr] = useState([])
    const [labelSelected, setLabelSelected] = useState(null);
    const [myPages, setMyPages] = useState([]);
    const [showLoader, setshowLoader] = useState(false);
    const [treemenucollapse, settreeMenuColapse] = useState(false);
    const [activeIndex, setActiveIndex] = useState(0);
    const [yearIndex, setyearIndex] = useState(0);
    const [currencytype, setCurrencyType] = useState('')
    const [currencyval, setCurrencyVal] = useState(79)
    const [currencyarr, setCurrencyArr] = useState([])
    const [conversiontype, setConversionType] = useState('Custom')
    const [moneymodalIsOpen, setMoneyModalIsOpen] = useState(false);
    const [showDropDown, setShowDropDown] = useState(false);
    const [modalIsOpen, setIsOpen] = useState(false);
    const [pdfAddress, setPdfAddress] = useState(false);
    const [loadMore, setLoadMore] = useState(false)
    const [loadCount, setLoadCount] = useState(0)
    const [dropDownData, setDropDownData] = useState([])
    const [dummynodes, setDummyNodes]  = useState([])
    const [filterarr, setfilterarr] = useState([])
    const [selectedOption, setSelectedOption] = useState(null);
    const [moneyOption, setMoneyOption] = useState(null);
    const [yearOption, setYearOption] = useState(null);
    const [selectedpage, setSelectedPage] = useState('Consumer Internet')
    const [reportarr, setNewReportArr] = useState([])
    const [showReport, setshowReport] = useState(false);
    const [FirstPage, setFirstPage] = useState([]);
    const [showcurrencybar, setshowCurrencyBar] = useState(true);
    const [isCurrentReportSubscribed, setCurrentReportSubscribed] = useState(false);
    const [pageEnterTime, setPageEnterTime] = useState(null);
    const [filterVal, setFilterVal] = useState(null)
    const [showExcel, setShowExcel] = useState(false);
    const [excelLoader,setExcelLoader] = useState(false);
    const [showSection1, setShowSection1] = useState(true);
    const [section2Pages, setSection2Pages] = useState([])
    const [isAnalysisLoading, setIsAnalysisLoading] = useState(false);
    const [redseerAnalysis, setRedseerAnalysis] = useState(null)
    const [section3Pages, setSection3Pages] = useState([])
    const [section4Pages, setSection4Pages] = useState([])
    const [showSection4, setShowSection4] = useState(false);
    const [showSection5, setShowSection5] = useState(false);
    const [showSection6rev, setShowSection6rev] = useState(false)
    const [showSection7swot, setShowSection7swot] = useState(false)
    const [showSectionReadyRec, setShowSectionReadyRec] = useState(false)
    const [showRelatedDataSet, setShowRelatedDataSet] = useState(false)
    const [showSectionIncome, setShowSectionIncome] = useState(false);
    const [quickLinks, setQuickLinks] = useState([])
    const [selectedArticle, setSelectedArticle] = useState(null)
    const [showArticleDetail, setShowArticleDetail] = useState(false)
    const [firstpageLoad, setFirstPageLoad] = useState(false)
    const [showLoaddiv, setShowLoaddiv] = useState(true);   
    const [showsub, setshowsub] = useState(false)
    const newrepaccessdata = useSelector(selectnewreportaccessData);
    const { setcurr_reportid, setreportplayers } = React.useContext(MyContext);
    const { treeData, setTreeData } = React.useContext(MyContext);
    const { alltreedata, setalltreedata } = React.useContext(MyContext);
    const dispatch = useDispatch();

    const showIncomeStatement = useRef(false);

    
    let [iconDict, setIconDict] = useState({
        'Sector Summary':<MdOutlineSummarize/>,
        'Sector Summary 2.0':<MdOutlineSummarize/>,
        'Overall':<SiCoveralls/>,
        'Traditional Brands':<BsTag/>,
        'SAAS':<BsFillCloudArrowDownFill/>,
        'Category':<BiCategory/>,
        'Amazon Specific':<FaAmazon />,
        'Paytm mall':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/paytm.png'/>,
        'Flipkart':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/flipkart.png'/>,
        'Shopclues':<BiCartAlt/>,
        'Traffic':<FaTrafficLight/>,
        'Engagement':<FaUsers/>,
        'Streams Analysis':<SiSimpleanalytics/>,
        'Monetization':<MdMonetizationOn/>,
        'User Profile':<FiUsers/>,
        'Tatacliq':<SiTata/>,
        'Engagement Profile':<AiOutlineFileSearch/>,
        'Sector Insights':<MdInsights/>,
        'Content':<BiBookContent/>,
        'Top Line Estimates':<FaDeezer/>,
        'Fulfilment Metrics':<FaAlignLeft/>,
        'Unit Economics':<MdMonetizationOn/>,
        'Keyboard':<FaKeyboard/>,
        'City Split':<FaCity/>,
        'Supply Chain Metrics':<GiBreakingChain/>,
        'Revenue Metrics':<RiMoneyDollarBoxFill/>,
        'Fintech':<RiMoneyDollarBoxFill/>,
        'Operational Metrics':<BiCartAlt/>,
        'Online Retail':<BiCartAlt/>,
        'Social Commerce Specific':<MdMonetizationOn/>,
        'Food Delivery':<IoFastFood/>,
        'Used Cars':<FaCarAlt/>,
        'Real Money Gaming':<RiMoneyDollarBoxFill/>,
        'Edtech':<BiBookContent/>,
        'eHealth':<GiHealthNormal/>,
        'Mobility':<GiCarWheel/>,
        // 'D2C Omni':<GoFileSymlinkDirectory/>,
        'Eb2b':<FaBusinessTime/>,
        // 'Consumer Internet':<ImConnection/>,
        // 'Baby Care':<FaBabyCarriage/>,
        // 'Mobile':<AiOutlineMobile/>,
        // 'Fashion':<GiClothes/>,
        'Electronics & Large/Small appliances':<BiFridge/>,
        'Epharma':<GiMedicines/>,
        // 'Grocery':<GiFruitBowl/>,
        'OTT_Video':<AiTwotoneVideoCamera/>,
        'OTT Audio':<FaRegFileAudio/>,
        'Content S&M':<BiBookContent/>,
        'Sector Summary (WIP)':<MdOutlineSummarize/>,
        'Sector Summary 2.0 (WIP)':<MdOutlineSummarize/>,
        'Company Profile (WIP)':<ImProfile />,
        'Overall (WIP)':<SiCoveralls/>,
        'Category (WIP)':<BiCategory/>,
        'Amazon Specific (WIP)':<FaAmazon />,
        'Amazon':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/amazon.png'/>,
        'Traffic (WIP)':<FaTrafficLight/>,
        'Engagement (WIP)':<FaUsers/>,
        'Streams Analysis (WIP)':<SiSimpleanalytics/>,
        'Monetization (WIP)':<MdMonetizationOn/>,
        'User Profile (WIP)':<FiUsers/>,
        'Engagement Profile (WIP)':<AiOutlineFileSearch/>,
        'Sector Insights (WIP)':<MdInsights/>,
        'Online Education (WIP)':<BiBookContent/>,
        'Top Line Estimates (WIP)':<FaDeezer/>,
        'Online Education':<BiBookContent/>,
        'Fulfilment Metrics (WIP)':<FaAlignLeft/>,
        'Unit Economics (WIP)':<MdMonetizationOn/>,
        'Keyboard (WIP)':<FaKeyboard/>,
        'Dashboard (WIP)':<MdDashboard/>,
        'City Split (WIP)':<FaCity/>,
        'Supply Chain Metrics (WIP)':<GiBreakingChain/>,
        'Revenue Metrics (WIP)':<RiMoneyDollarBoxFill/>,
        'Operational Metrics (WIP)':<BiCartAlt/>,
        'Online Retail ':<BiCartAlt/>,
        'Social Commerce Specific (WIP)':<MdMonetizationOn/>,
        'Food Tech (WIP)':<IoFastFood/>,
        'Used Cars (WIP)':<FaCarAlt/>,
        'Real Money Gaming ':<RiMoneyDollarBoxFill/>,
        'EdTech (WIP)':<BiBookContent/>,
        'eHealth (WIP)':<GiHealthNormal/>,
        'Ride Hailing':<GiCarWheel/>,
        // 'D2C Omni (WIP)':<GoFileSymlinkDirectory/>,
        'eB2B (WIP)':<FaBusinessTime/>,
        // 'Consumer Internet (WIP)':<ImConnection/>,
        'Baby Care (WIP)':<FaBabyCarriage/>,
        'Mobile (WIP)':<AiOutlineMobile/>,
        'Fashion (WIP)':<GiClothes/>,
        'Electronics & Large/Small appliances (WIP)':<BiFridge/>,
        'Epharma (WIP)':<GiMedicines/>,
        // 'Grocery (WIP)':<GiFruitBowl/>,
        'Shortform Video (WIP)':<GiVideoConference/>,
        'OTT_Video (WIP)':<AiTwotoneVideoCamera/>,
        'OTT Audio (WIP)':<FaRegFileAudio/>,
        'Content S&M (WIP)':<BiBookContent/>,
        'Digital Content':<MdOutlinePersonalVideo/>,
        'eLogistics':<FaTruck/>,
        'Meesho':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/meesho.jpeg'/>,
        'Tatacliq':<img  style = {{'width':'15px', 'height':'10px'}}src = '/Images/tatacliq.png'/>,
        'Snapdeal':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/snapdeal.jpeg'/>,
        'Ajio':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/a.jpeg'/>,
        'Clubfactory':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/clubfactory.jpeg'/>,
        'Jabong':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/jabong.jpeg'/>,
        'Koovs':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/koovs.jpeg'/>,
        'LimeRoad':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/limeroad.jpeg'/>,
        'Myntra':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/myntra.jpeg'/>,
        'Nykaa Fashion':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/Nykaa_fashion.png'/>,
        'Shein':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/shein.jpeg'/>,
        'Nykaa':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/nykaa.jpeg'/>,
        'Purplle':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/purplle.jpeg'/>,
        'Pepperfry':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/pepperfry.jpeg'/>,
        'Urban Ladder':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/urbanladder.jpeg'/>,
        'Delhivery':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/delhivery.png'/>,
        'Ecom Express':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/ecomexpress.png'/>,
        'Shadowfax':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/shadowfax.png'/>,
        'Xpressbees':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/xpressbee.png'/>,
        'Dailyhunt':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/Dailyhunt.png'/>,
        'Sharechat':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/sharechat.png'/>,
        'Glance':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/glance.png'/>,
        'Instagram':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/insta.png'/>,
        'YouTube':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/yt.png'/>,
        'Facebook':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/fb.png'/>,
        'Apple Music':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/applemusic.png'/>,
        'YouTube Music':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/ytmusic.png'/>,
        'Resso':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/resso.png'/>,
        'Spotify':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/spotify.png'/>,
        'Wynk':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/wynk.png'/>,
        'JioSaavn':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/jiosaavn.png'/>,
        'Gaana':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/gaana.png'/>,
        'AZ Prime Video':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/amazonprime.png'/>,
        'MX Player':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/mxplayer.png'/>,
        'Netflix':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/netflix.png'/>,
        'Voot':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/voot.png'/>,
        'Josh':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/josh.png'/>,
        'Moj':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/moj.png'/>,
        'Hotstar':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/hotstar.png'/>,
        'Moj Lite+ (MX TAKATAK)':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/hotstar.png'/>,
        'Winzo':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/winzo.png'/>,
        'MPL':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/mpl.png'/>,
        'Rush':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/rush.png'/>,
        'Junglee Rummy':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/junglee_rummy.png'/>,
        'Ace2Three':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/a23.png'/>,
        'Rummy Culture':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/rummy_culture.png'/>,
        'Rummy Circle':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/rummy_circle.png'/>,
        'Spartan Poker':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/spartan_poker.png'/>,
        'Pokerbaazi':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/pokerbaazi.png'/>,
        'Adda52':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/adda52.png'/>,
        'Zupee':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/zuppee.png'/>,
        'MyTeam11':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/MyTeam11.png'/>,
        'My11Circle':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/MyTeam11Circle.png'/>,
        'Dream11':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/Dream11.png'/>,
        'BB Daily':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/bb_daily.png'/>,
        'Supr Daily':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/supr_daily.png'/>,
        'Milk Basket':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/milkbasket.png'/>,
        'Country Delight':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/country_delight.png'/>,
        'FTH Daily':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/fth_daily.png'/>,
        'Blinkit':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/blinkit.png'/>,
        'BB Now':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/bb_now.png'/>,
        'Swiggy Instamart':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/Swiggy.png'/>,
        'Dunzo Grocery':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/dunzo.png'/>,
        'Zepto':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/zepto.png'/>,
        'BB Now':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/bb_now.png'/>,
        'Swiggy Stores':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/Swiggy.png'/>,
        'Bigbasket':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/bb_now.png'/>,
        'Jiomart':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/jiomart.png'/>,
        'Amazon Grocery':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/amazon_grocery.png'/>,
        'Flipkart Grocery':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/flipkart.png'/>,
        'Dmart Ready':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/dmart.png'/>,
        'Pickily':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/pickily.png'/>,
        'Fraazo':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/fraazo.png'/>,
        'Swiggy Genie':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/Swiggy.png'/>,
        'Swiggy':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/swiggy1.png'/>,
        'Zomato':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/Zomato.png'/>,
        'Citymall':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/citymall.png'/>,
        'Dealshare':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/dealshare.png'/>,
        'Grofers':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/grofers.png'/>,
        'Apollo 247':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/apollo247.png'/>,
        'Flipkart Health Plus':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/flipkart_health_plus.png'/>,
        'Netmeds':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/netmeds.png'/>,
        'PharmEasy':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/Pharmeasy.png'/>,
        'TATA 1mg':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/1mg.png'/>,
        'Medplus':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/medplus.png'/>,
        'Redcliffe Labs':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/redcliff_labs.png'/>,
        'Healthians':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/healthians.png'/>,
        'OrangeHealth':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/orangehealth.png'/>,
        'Dhani':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/dhani.png'/>,
        'Truemeds':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/truemeds.png'/>,
        'MedPlus':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/netmeds.png'/>,
        'Ola':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/ola_cabs.png'/>,
        'Uber':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/uber.png'/>,
        'Rapido':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/rapido.png'/>,
        'Cars24':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/cars_24.png'/>,
        'Spinny':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/spinny.png'/>,
        'Ola Cars':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/ola_cabs.png'/>,
        'CarDekho':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/cardekho.png'/>,
        'FirstCry':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/firstcry.png'/>,
        'Zee5':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/zee5.png'/>,
        'Whatsapp':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/whatsapp.png'/>,
        'Public':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/public.png'/>,
        'Messenger':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/messenger.png'/>,
        'Telegram':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/telegram.png'/>,
        'Inshorts':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/inshots.png'/>,
        'AZ Prime Music':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/az_prime_music.png'/>,
        'Snapchat':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/snapchat.png'/>,
        'SonyLiv':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/sonyliv.png'/>,
        'Zivame':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/zivame.png'/>,
        'Sephora':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/sephora.png'/>,
        'Shopperstop':<img  style = {{'width':'10px', 'height':'10px'}}src = '/Images/shopperstop.png'/>,
        'Probo':<img style = {{'width':'10px', 'height':'10px'}}src = '/Images/probo.png'/>,
        'Playerzpot':<img style = {{'width':'10px', 'height':'10px'}}src = '/Images/playerzpot.png'/>,
        'Howzat':<img style = {{'width':'10px', 'height':'10px'}}src = '/Images/howzat.png'/>,
        'Vision11':<img style = {{'width':'10px', 'height':'10px'}}src = '/Images/vision11.png'/>,
        'Fantasy Akhada':<img style = {{'width':'10px', 'height':'10px'}}src = '/Images/fanntasyakhada.png'/>,
        'First Games':<img style = {{'width':'10px', 'height':'10px'}}src = '/Images/firstgames.png'/>,
      })
    
    const navigate = useNavigate();
    const useQuery = () => new URLSearchParams(useLocation().search);
    const newsletter_name = useQuery().get("newsletter_name") || window.localStorage.getItem('newsletter_name') || "";
    const newsletter_month = useQuery().get("newsletter_month") || window.localStorage.getItem('newsletter_month') || "";
    const newsletter_user = useQuery().get("newsletter_user") || window.localStorage.getItem('newsletter_user') || "";
    const newsletter_email = useQuery().get("newsletter_email") || window.localStorage.getItem('newsletter_email') || "";

    const url_val = useQuery().get("val");
    const url_industry_id = useQuery().get("industry_id");
    const url_key = useQuery().get("key");
    const url_filter = useQuery().get("filter");
    const url_filter_value = useQuery().get("filter_value");
    const url_source = useQuery().get("source") || window.localStorage.getItem('source');
    const url_content_id = useQuery().get("content_id") || window.localStorage.getItem("content_id");
    const url_content_type = useQuery().get("content_type") || window.localStorage.getItem("content_type");

      function findParents(data, label, key_val, parents = []) {
        for (let node of data) {
            // If the current node matches the given label and key_val
            if (node.label.toLowerCase() === label.toLowerCase() && node.key_val === key_val) {
                // Add the current node's label and key to the parents array
                parents.push([node.label,node.key, node.filter_value, node.filter] );
                return parents; // return the parents including current node
            }
      
            // If the current node has children, recurse
            if (node.nodes && node.nodes.length) {
                const newParents = [...parents, [node.label,node.key, node.filter_value, node.filter]];
                const found = findParents(node.nodes, label, key_val, newParents);
                if (found) return found;
            }
        }
        return null; // If not found
      }


    useEffect(()=>{
      let client_id = window.localStorage.getItem("clientID")
      let rep_id = parent_props.initialOpenNodes[0]
      let prop_token = window.localStorage.getItem('token')
      setshowLoader(true)
      console.log('treedata_useeffect = ', alltreedata)
      const result = findParents(alltreedata, parent_props.value, parseInt(rep_id));
      console.log('useeffect_result = ',parent_props.filter, parent_props.filter_value, result, parseInt(rep_id))
      if(result){
        // window.localStorage.setItem('treeziparr', result)
        window.localStorage.setItem('treeziparr', JSON.stringify(result));
        console.log('treeziparr_res =', result)
        setTreeziparr(result)
      }
      else{
        let res  = (window.localStorage.getItem('treeziparr'))
        if (res) {
          res = JSON.parse(res);
      }
        res?.push([parent_props.value, rep_id])
        setTreeziparr(res)
      }
      let filterValue = parent_props.filter_value.replace(/\band\b/g, "&");
      console.log('filterValue = ', filterValue)
      setSelectedPage(parent_props.value)
      setFilterVal(filterValue)
      setfilterarr(parent_props.filter.split(','))

      const getquicklinks = async() =>{
        const quicklinknodes =  await getFromDb('quicklinksnodesarr');
        console.log('quicklinksnodes = ', quicklinknodes[0].label)
        setQuickLinks(quicklinknodes )
      }
      getquicklinks()
      if(newrepaccessdata.length>0){
          let res  = newrepaccessdata
          setNewReportArr(res)
          let rep_arr= res
          handleClickTree(parent_props.value, rep_id , true , -1 , rep_id, rep_arr)
      }else{
        const getdatafromdb = async()=>{
          let storedData = []
          try{
            storedData = await getFromDb('newreportaccessdata');
          }catch{
            console.log('error going to fecth data')
          }
        if (storedData.length>0) {
          dispatch(setNewReportAccessData(storedData));
          let res = storedData
          setNewReportArr(res)
          let rep_arr= res
          handleClickTree(parent_props.value, rep_id , true , -1 , rep_id, rep_arr)
        }else{
          console.log('fetching')
          fetch(`${process.env.REACT_APP_API_ENDPOINT}/newreportaccess/?client_id=${client_id}`, {
            method:'GET',
            headers:{
              'Content-Type': 'application/json',
            },
          })
          .then(res=>res.json())
          .then(
            res=>{
              dispatch(setNewReportAccessData(res));
              saveToDb('newreportaccessdata',res)
              setNewReportArr(res)
              let rep_arr= res
              handleClickTree(parent_props.value, rep_id , true , -1 , rep_id, rep_arr)
            }
          )
        }
      }
      getdatafromdb();
        
      }
    },[])

    useEffect(() => {
      if (showLoader === false) {
        const showCPProfile = () => {
          const companyProfileData = JSON.parse(
            window.localStorage.getItem("company_profile_data")
          );
          if (companyProfileData) {
            onPlayerClick(companyProfileData);
            window.localStorage.removeItem("company_profile_data");
          }
        };
        const timeoutId = setTimeout(() => {
          showCPProfile();
        }, 400);
        return () => clearTimeout(timeoutId);
      }
    }, [showLoader]);
  

    useEffect(() => {
      setIsAnalysisLoading(true);
      axios
        .get(`${ANALYSIS_ENDPOINT}/${parent_props.initialOpenNodes[0]}/`)
        .then((response) => response.data)
        .then((analysis) => {
          if(analysis.status===200){
            setRedseerAnalysis(analysis.data);
          }
          setIsAnalysisLoading(false);
        })
        .catch((error) => {
          console.error("Error in fetching analysis-", error);
          setIsAnalysisLoading(false);
        });
    }, []);

    useEffect(()=>{
      if(selectedArticle){
        setShowArticleDetail(true);
      }
    },[selectedArticle])

    let getNodeName = (key,all_nodes) =>{
      let nodes = all_nodes
      for(let i =0; i<nodes.length;i++){
        // search in top layer
        if(nodes[i].key==key){
          // //console.log('lognode=',nodes[i])
          return nodes[i].label
        }else{
          if(nodes[i].nodes.length>0){
            let node_name = getNodeName(key, nodes[i].nodes)
            if(node_name){
              return node_name
            }
          }else{
            //console.log('node array was empty')
          }
        }

      }
      // return labels correponding to key
    }

    const getParents = (props)=>{
      let parent_string_arr = props.parent.split('/')
      // ['9', '25', '26', '44']
      let parents = []
      let all_nodes = allNodes
      // my pages is a list with tree structure
      if(parent_string_arr[0]!==''){
        for(let i =0; i<parent_string_arr.length; i++){
          let nodename = getNodeName(parent_string_arr[i],all_nodes)
          if(nodename){
            parents.push(nodename)
          }
        }
      }
      return [parents, parent_string_arr]
    }
    
    let handleSignOut = ()=>{
      localStorage.clear();
      navigate('/')
    }

    let handleClickTree = (reportname, key, finalized, index = -1 , key_id = -1 , report_arr = reportarr)=>{
       window.reports = []
        if(showArticleDetail){
          setShowArticleDetail(false)
          setSelectedArticle(null)
        }

        if(showSection1===false){
          setShowSection1(true)
        }

        
        console.log('key_id=',parseInt(key_id))
        console.log('index=', index)
        window.localStorage.setItem('report' , reportname)
        if(index===treearr.length-1 & key===-2){
          // disable last click. also disable clicks on non links
          return
        }if (key===-2 & dummynodes.includes(reportname)){
          return
        }
        if (index===0){
          return
        }
        if (index === 1){
          
          if ([25,72,67,95,280].includes(parseInt(key_id))){
            return
          } 
        }

        if(key_id==9){
          // update to take from backend
          window.localStorage.setItem('report' , reportname)
          setTreeziparr([])
          setSelectedPage(reportname)
          console.log(reportname)
          setshowCurrencyBar(false)
          setshowReport(false)
          return
        }
        let found = false
        console.log('reportname = ', reportname)
        for (let i =0; i<report_arr.length;i++){
          if(report_arr[i].report_name===reportname){
            window.localStorage.setItem('start_date',report_arr[i].start_date)
            window.localStorage.setItem('end_date', report_arr[i].end_date)
            setShowExcel(report_arr[i].excel_access)
            console.log('start_date=',  window.localStorage.getItem("start_date"))
            console.log('end_date', window.localStorage.getItem("end_date"))
            found = true
            break
          }
        }
        if (found===false){
          setShowExcel(false)
          window.localStorage.setItem('start_date', null)
          window.localStorage.setItem('end_date', null)
        }
        setshowLoader(true)
        let nodechildren_got = false
        // console.log('key=', key)
        if(key>0){
          fetch(`${process.env.REACT_APP_API_ENDPOINT}/nodechildren/?key=${key}`, {
            method:'GET',
            headers:{
              'Content-Type': 'application/json',
            },
          })
          .then(res=>res.json())
          .then(
            res =>{
              // key=25 fro onlien retail
              if(key === 25){
                setShowDropDown(false)
              }
              else if (res.length>=1){
                setShowDropDown(true)
                for(let i=0; i<res.length; i++){
                  res[i].url = `/Report3/?val=${res[i].label}&key=${res[i].key}&filter_value=${res[i].filter_value}&filter=${res[i].filter}&industry_id=${res[i].industry_id}`
                  if(iconDict[res[i].label]){
                    res[i].icon = iconDict[res[i].label]
                  } else if (res[i].image){
                    res[i].icon = <img style = {{'width':'10px', 'height':'10px'}} src={res[i].image} />
                  }
                  else {
                    res[i].icon = <GiHamburgerMenu/>
                  }
                }
                setreportplayers(res)
                setDropDownData(res)
              }else{
                setShowDropDown(false)
              }
              nodechildren_got = true
            }
          )
        }
        
        if (key === -1){
          nodechildren_got = true
          window.localStorage.setItem('report' , reportname)
          setSelectedPage(reportname)
          setShowDropDown(true)
          if (finalized===true){
            window.localStorage.setItem('finalized', 'true')
          }else{
            window.localStorage.setItem('finalized', 'false')
          }
        }
        if (key === -2){
          nodechildren_got = true
          window.localStorage.setItem('report' , reportname)
          setSelectedPage(reportname)
          setSelectedOption(null)
          setfilterarr(['1'])
          // check where the clicked report lies in array if it lies in range we show dropdown
          for(let i= 0; i<treearr.length; i++){
            if(treearr[i]===reportname){
              if(i>1){
                // breadcrumbs will be of type consumer inter/OR/hori/somthing. we only want ot show dirpp down for after 2nd breadcrumb
                setShowDropDown(true)
                break
              }else{
                setShowDropDown(false)
                break
              }
            }
          }

          console.log('index = ', index,treearr.length)
          // console.log('foundnode = ', getNodeDetails(myPages,reportname))
          if(true){
            let arr = treearr
            for(let i = treearr.length-1; i>index; i--){
              arr.pop()
            }
            setTreearr(arr)
            let zip_arr = treeziparr
            for (let i=treeziparr.length-1;i>index;i--){
                zip_arr.pop()
            }
            setTreeziparr(zip_arr)
          }
        }
        let prop_token = window.localStorage.getItem('token')
        let rep_id = parseInt(key_id)
        setcurr_reportid(rep_id)
        fetch(`${process.env.REACT_APP_API_ENDPOINT}/newreportpages/?rep_id=${rep_id}&client_id=${window.localStorage.getItem("clientID")}`, {
          method:'GET',
          headers:{
            'Content-Type': 'application/json',
            Authorization: `Token ${prop_token}`
          }
        })
        .then(res=>{
          if (res.status === 401 || res.status ===500) {
            console.log('User is not authenticated');
            handleSignOut()
          }
           return res.json()})
        .then(
          res=>{
            let section1Data = [];
            if (res.length < 1) {
              setshowReport(false);
              setshowCurrencyBar(false);
              setCurrentReportSubscribed(false);
            } else {
              if (res[res.length - 1]["link"] === "Sub") {
                res.pop();
                console.log("res = ", res);
                setshowsub(true);
              } else {
                setshowsub(false);
              }

              section1Data=res.filter((val)=>(val.section_type===null || val.page_name==='section 1'))
              const subData = section1Data.filter((val)=>(val.page_name==='Sub')).length;
              setCurrentReportSubscribed(subData>0? false : true);
              
              let firstpage = section1Data.shift();
              let firstpage_arr = [firstpage];
              if(firstpage){
                setFirstPage(firstpage_arr);
              }
              setshowReport(true);
            }
            let section1pages = section1Data;
            if (section1pages.length === 0) {
              setShowLoaddiv(false);
            }
            let sec2pages = res.filter((val)=>(val.section_type==='section 2'))
            let sec3pages = res.filter((val)=>(val.section_type==='section 3'))
            let sec4pages = res.filter((val)=>(val.section_type==='section 4'))
            let sec5pages = res.filter((val)=>(val.section_type==='section 5'))
            let sec6pages = res.filter((val)=>(val.section_type==='section 6'))
            let sec7pages = res.filter((val)=>(val.section_type==='section 7'))
            let IncomePages = res.filter((val)=>(val.section_type==='income_pages'))
            let readyReckonerPages = res.filter((val)=>(val.section_type==='ready_reckoner'))
            let relatedDataSetPage = res.filter((val)=>(val.section_type==='related_data_set'))
            let IncomeStatementPages = res.filter((val)=>(val.section_type==='income_statement'))
            if(sec3pages.length>0){
              setSection3Pages(sec3pages)
            }
            IncomePages.length>0?setShowSectionIncome(true):setShowSectionIncome(false)
            if (sec4pages.length>0){
              setShowSection4(true)
            }else{
              setShowSection4(false)
            }
            if (sec5pages.length>0){
              setShowSection5(true)
            }else{
              setShowSection5(false)
            }

            if(sec6pages.length>0){
              setShowSection6rev(true)
            }else{
              setShowSection6rev(false)
            }

            if(sec7pages.length>0){
              setShowSection7swot(true)
            }else{
              setShowSection7swot(false)
            }
            if(readyReckonerPages.length>0){
              setShowSectionReadyRec(true)
            }else{
              setShowSectionReadyRec(false)
            }
            if(relatedDataSetPage.length>0){
              setShowRelatedDataSet(true)
            }else{
              setShowRelatedDataSet(false)
            }
            if(IncomeStatementPages.length>0){
              showIncomeStatement.current=true
            }else{
              showIncomeStatement.current=false
            }

            setSection4Pages(sec4pages)

            setnewReportPages(section1pages)
            setSection2Pages(sec2pages)
            setshowLoader(false)
            
          }
        ).catch((error) => {
          console.log('error')
          // handleSignOut()
        })

        //console.log(reportname)
      }
    
  let handlelinkClick = (val) => {
    onQuickLinkClickCT(val.label, val.key, val.subscribed);
    let filter_value = val.filter_value;
    try {
      filter_value = filter_value.replace(/&/g, "and");
    } catch (err) {
      console.log("& absent");
    }

    navigate(
      `/Report3/?val=${val.label}&key=${val.key}&filter_value=${filter_value}&filter=${val.filter}&industry_id=${val.industry_id}`
    );
    // becuase report3 wont reload on changing address
    window.location.reload();
  };

    let handleLoadMore = ()=>{
      setLoadMore(!loadMore)
      setLoadCount(prevLoadCount => prevLoadCount + 1)
      console.log('newreppages = ', newReportPages)
      for(let i = 0; i<window.reports.length;i++){
        window.reports[i].getActivePage().then(
          (activePage=>{
            if(i!==0){
              let active_ht = activePage.defaultSize.height
            let active_width = activePage.defaultSize.width
            let x = i-1
            let width = document.getElementsByClassName('report-style-class-newreport0')[0].offsetWidth;
            let ht = ((active_ht/active_width)*width)
            console.log(active_ht,active_width, width,ht, 'report-style-class-newreport'+x+1 ,window.reports.length)
            document.getElementsByClassName('report-style-class-newreport'+x+1)[0].style.height = ht+'px';
            }
            }))

      }
      // CLEVERTAP*****
      if (!loadMore) {
        const currReportId = window.localStorage.getItem("rep_id");
        const currReportName = window.localStorage.getItem("report");
        if (selectedOption) {
          const selectedCP = platform_option.filter(
            (opt) => opt.label === selectedOption
          )?.[0];
          onCPLoadMoreClickCT(
            selectedCP.label,
            selectedCP.key,
            currReportName,
            currReportId,
            isCurrentReportSubscribed
          );
        } else {
          onSectorLoadMoreClickCT(
            currReportName,
            currReportId,
            isCurrentReportSubscribed
          );
        }
      }
      // *****CLEVERTAP
    }

    let handleCompanyLoadMore = () =>{
      console.log('click')
      window.location.href=`/company-board?industry_id=${window.localStorage.getItem('industry_id')}`;
    }

  // CLEVERTAP*****
  const onSubmitResultToCT = (submitted) => {
    const currReportId = window.localStorage.getItem("rep_id");
    const currReportName = window.localStorage.getItem("report");
    if (selectedOption) {
      const selectedCP = platform_option.filter(
        (opt) => opt.label === selectedOption
      )?.[0];
      onReportSubscriptionRequestClickCT(
        submitted,
        currReportName,
        currReportId,
        "Company Profile",
        selectedCP.label,
        selectedCP.key   
      );
    } else {
      onReportSubscriptionRequestClickCT(
        submitted,
        currReportName,
        currReportId,
        "Sector Profile",
      );
    }
  };
  const onMouseEnterPage = () => {
    // setPageEnterTime(new Date());
    console.log('ms')
  };
  const onMouseLeavePage = (page_name, page_type, section_type) => {
    // if (pageEnterTime) {
    //   const pageLeftTime = new Date();
    //   const pageSpentTime = pageLeftTime - pageEnterTime;
    //   setPageEnterTime(null);
    //   const currReportId = window.localStorage.getItem("rep_id");
    //   const currReportName = window.localStorage.getItem("report");
    //   if (selectedOption) {
    //     const selectedCP = platform_option.filter(
    //       (opt) => opt.label === selectedOption
    //     )?.[0];
    //     onReportPageVisitCT(
    //       page_name,
    //       page_type,
    //       section_type || "section 1",
    //       pageSpentTime,
    //       currReportName,
    //       currReportId,
    //       "Company Profile",
    //       selectedCP.label,
    //       selectedCP.key
    //     );
    //   } else {
    //     onReportPageVisitCT(
    //       page_name,
    //       page_type,
    //       section_type || "section 1",
    //       pageSpentTime,
    //       currReportName,
    //       currReportId,
    //       "Sector Profile"
    //     );
    //   }
    // }
    console.log('hello')
  };
  useEffect(() => {
    const timeoutId = setTimeout(() => {
      if (
        newsletter_name &&
        newsletter_month &&
        newsletter_user &&
        newsletter_email
      ) {
        onNewsletterClickCT(
          newsletter_name,
          newsletter_month,
          "clicked",
          newsletter_email,
          "Sector Report Page"
        );
        window.localStorage.removeItem("newsletter_name");
        window.localStorage.removeItem("newsletter_month");
        window.localStorage.removeItem("newsletter_user");
        window.localStorage.removeItem("newsletter_email");
      }
    }, 1000);
    if (url_val && url_key) {
      navigate(
        `/Report3/?val=${url_val}&key=${url_key}&filter_value=${url_filter_value}&filter=${url_filter}&industry_id=${parent_props.industry_id}`
      );
    }
    if (url_content_id && url_content_type) {
      const content_url = `${
        url_content_type === "article"
          ? CONTENT_ARTICLE_ENDPOINT
          : CONTENT_REPORT_ENDPOINT
      }/${url_content_id}`;
      const content_type_name = `${
        url_content_type === "article" ? "Article" : "Report"
      }`;
      const fetchPageData = async (page) => {
        try {
          const response = await axios.get(content_url);
          const docResponse = response.data;
          setSelectedArticle({
            type: url_content_type,
            typeName: content_type_name,
            data: docResponse,
          });
        } catch (error) {
          console(error);
        }
      };
      fetchPageData();
      window.localStorage.removeItem("source");
      window.localStorage.removeItem("content_id");
      window.localStorage.removeItem("content_type");
    }
    return () => clearTimeout(timeoutId);
  }, []);
  // *****CLEVERTAP

    const onOptionSelect = (option) => {
      // CLEVERTAP*****
      const currReportId = window.localStorage.getItem("rep_id");
      const currReportName = window.localStorage.getItem("report");
      window.localStorage.setItem('report_name', option.label)
      window.localStorage.setItem('report_id', option.key_val)
      onCompanyProfileClickCT(
        option.name,
        option.key,
        currReportName,
        currReportId
      );
      // *****CLEVERTAP
        window.localStorage.setItem('player_id', option.player_id)
        setFilterVal(option.filter_value)
        if(option.filter!=null)
       { setfilterarr(option.filter.split(','))}
        // when we are selecting from drop down we need to remove last element of breadcrumb and add new selected in its place
        if(selectedOption!==null){
          treearr.pop()
          treearr.push(option.label)
          treeziparr?.pop()
          treeziparr?.push([option.label, option.key_val])
        }else{
          treearr?.push(option.label)

          treeziparr?.push([option.label, option.key_val])
        }
        setSelectedOption(option.label);
        setshowCurrencyBar(true)
        handleClickTree(option.label, -1 , option.finalized , -1 , option.key_val)
      };

    const fintableSelect = (label, key_val) =>{
      console.log(label)
      let treeziparr = window.localStorage.getItem('treeziparr')
      treeziparr?.push([label, key_val])
      window.localStorage.setItem('treeziparr', treeziparr)
    }

    const onPlayerClick =(option)=>{
      console.log('node = ',option)

      window.localStorage.setItem('player_id', option.player_id)
      setFilterVal(option.filter_value)
      if(option.filter!=null)
      { setfilterarr(option.filter.split(','))}
      treeziparr?.push([option.label, option.key_val])
      setSelectedOption(option.label);
      setshowCurrencyBar(true)
      handleClickTree(option.label, -1 , option.finalized , -1 , option.key_val)

  } 


    const onYearSelect = (option) => {
      // CLEVERTAP*****
      const currReportId = window.localStorage.getItem("rep_id");
      const currReportName = window.localStorage.getItem("report");
      if (selectedOption) {
        const selectedCP = platform_option.filter(
          (opt) => opt.label === selectedOption
        )?.[0];
        onYearSwitchClickCT(
          option.label,
          currReportName,
          currReportId,
          "Company Profile",
          selectedCP.label,
          selectedCP.key,
        );
      } else {
        onYearSwitchClickCT(
          option.label,
          currReportName,
          currReportId,
          "Sector Profile",
        );
      }
      // *****CLEVERTAP
      setYearOption(option.label)
      if(option.label==='CY'){
        window.localStorage.setItem('year', 'CY')
      }else{
        window.localStorage.setItem('year', 'FY')
      }

      const year_converter = {
        $schema: "http://powerbi.com/product/schema#advanced",
        target: {
            table: "Date Parameter",
            column: "Year_Type"
        },
        filterType: models.FilterType.Advanced,
        logicalOperator: "Is",
        conditions: [
            {
                operator: "Is",
                value: window.localStorage.getItem('year')
            }
        ]
      }
      for(let i= 0;i<window.reports.length; i++){
        window.reports[i].getActivePage().then(
          (activePage=>{
            activePage.getVisuals().then(
              (visuals=>{
                let slicers = visuals.filter(function (visual) {
                  return visual.type === "slicer";
              });
                slicers.forEach(async (slicer) => {
                const state = await slicer.getSlicerState();  

                if(state.targets[0].column==='Year_Type'){
                  let target_slicer = visuals.filter(function (visual) {
                    return visual.type === "slicer" && visual.name === slicer.name;             
                })[0];
                  await target_slicer.setSlicerState({ filters: [year_converter] });
                }
            })      
              })
            )
          })
        )
      }
      };


      const onMoneySelect = (option) => {
        // CLEVERTAP*****
        const currReportId = window.localStorage.getItem("rep_id");
        const currReportName = window.localStorage.getItem("report");
        if (selectedOption) {
          const selectedCP = platform_option.filter(
            (opt) => opt.label === selectedOption
          )?.[0];
          onCurrencySwitchClickCT(
            option.label,
            currReportName,
            currReportId,
            "Company Profile",
            selectedCP.label,
            selectedCP.key,
          );
        } else {
          onCurrencySwitchClickCT(
            option.label,
            currReportName,
            currReportId,
            "Sector Profile",
          );
        }
        // *****CLEVERTAP
        setMoneyOption(option.label)
        if(option.label==='INR'){
          window.localStorage.setItem('currency', 'INR')
          //console.log(window.localStorage.getItem('currency'))
          setCurrencyType('INR')
        }else{
          window.localStorage.setItem('currency', 'USD')
          //console.log(window.localStorage.getItem('currency'))
          setCurrencyType('USD')
        }
  
        const money_converter = {
          $schema: "http://powerbi.com/product/schema#advanced",
          target: {
              table: "Currency Table",
              column: "Currency"
          },
          filterType: models.FilterType.Advanced,
          logicalOperator: "Is",
          conditions: [
              {
                  operator: "Is",
                  value: window.localStorage.getItem('currency')
              }
          ] 
        }
        for(let i = 0; i<window.reports.length; i++){
          window.reports[i].getActivePage().then(
            (activePage=>{
              activePage.getVisuals().then(
                (visuals=>{
                  let slicers = visuals.filter(function (visual) {
                    return visual.type === "slicer";
                });
                  slicers.forEach(async (slicer) => {
                  const state = await slicer.getSlicerState();  
                  
                  if(state.targets[0].column==='Currency'){
                    let target_slicer = visuals.filter(function (visual) {
                      return visual.type === "slicer" && visual.name === slicer.name;             
                  })[0];
                    await target_slicer.setSlicerState({ filters: [money_converter] });
                  }
              })      
                })
              )
            })
          )
        }
      };


    const DEFAULT_PADDING = 16;
    const ICON_SIZE = 3;
    const LEVEL_SPACE = 10

    const ToggleIcon = ({ on }) => <span style={{ marginRight: 8 }}>{on ? <IoIosArrowDown/> : <IoIosArrowForward/>}</span>;

    const ListItem = ({
      level = 0,
      hasNodes,
      isOpen,
      label,
      subscribed = false,
      searchTerm,
      openNodes,
      toggleNode,
      matchSearch,
      focused,
      key,
      ...props
    }) => (
      <ListGroupItem
      // props enabling clicking/navigating nodes
        {...props}
        style={{
          paddingLeft:  ICON_SIZE + level * LEVEL_SPACE,
          cursor: level!==0?'pointer':'auto',
          boxShadow: focused ? '0px 0px 5px 0px #222' : 'none',
          zIndex: focused ? 999 : 'unset',
          position: 'relative',
          // backgroundColor:'#18183E', 
          backgroundColor:labelSelected===label & level !== 0?'#2323C8':'#18183E',
          color:level===0?'white':subscribed?'white':'grey',
          fontWeight:subscribed?'bold':'normal',
          border:'none',
          // zIndex:10,
          fontSize:level===0?20:17,
          // '&:hover':{    does not work. Onhover does not work inline styling
          //   backgroundColor:'red'
          // }
        }}
        onClick={e => {
            // this onclick conflicts with oclick defined below
            // make your get parents function here
          // window.reports = []
          setSelectedOption(null)
          setLabelSelected(label)

          if(hasNodes===false){
            window.localStorage.setItem('report', label)
            
            if(props.finalized){
              setshowCurrencyBar(true)
              window.localStorage.setItem('finalized', 'true')
            }else{
              setshowCurrencyBar(false)
              window.localStorage.setItem('finalized', 'false')
            }
            console.log('props for filter=',props )
            if(props.filter!==null && props.filter!==''){
              setfilterarr(props.filter.split(','))
              window.localStorage.setItem('filterval', props.filter_value)
              setFilterVal(props.filter_value)
              console.log('filterval = ', props.filter_value)
            }else{
              setfilterarr([])
              setFilterVal(null)
            }
            setSelectedPage(label)
            let arr = currencyarr
            let currency_type = window.localStorage.getItem('currency')
            if(currency_type==='INR'){
              setActiveIndex(0)
              window.localStorage.setItem('currency', 'INR')
            }else{
              setActiveIndex(1)
              window.localStorage.setItem('currency', 'USD')
            }
            let year_type = window.localStorage.getItem('year')
            if(year_type==='CY'){
              setyearIndex(0)
              window.localStorage.setItem('year', 'CY')
            }else{
              setyearIndex(1)
              window.localStorage.setItem('year', 'FY')
            }
          }

          if(hasNodes && toggleNode){
            if(level===0){
              console.log('toggle diabled')
            }else{
              toggleNode()
              console.log('props = ', props)
              if(props.filter!==null && props.filter!==''){
                setfilterarr(props.filter.split(','))
                window.localStorage.setItem('filterval', props.filter_value)
                setFilterVal(props.filter_value)
                console.log('filterval = ', props.filter_value)
              }else{
                setfilterarr([])
                setFilterVal(null)
              }
              if([25,67].includes(props.key_val)){
                setSelectedPage(label)
                let p_arr = getParents(props)
                let parent_arr = p_arr[0]
                parent_arr.push(label)
                let key_arr = p_arr[1]
                key_arr.push(props.key_val.toString())
                setTreearr(parent_arr)
                let zip = parent_arr.map(function(e, i) {
                    return [e, key_arr[i]];
                    });
                setTreeziparr(zip)
                handleClickTree(label, props.key_val, props.node_type,-1, props.key_val)
                if(props.key_val===25){
                  setshowCurrencyBar(true)
                  window.localStorage.setItem('finalized', 'false')
                }else{
                  window.localStorage.setItem('finalized', 'true')
                }
              }
            }
          }else{
            let p_arr = getParents(props)
            let key_arr =  p_arr[1]
            key_arr.push(props.key_val.toString())
            let parent_arr = p_arr[0]
            parent_arr.push(label);
            // const zip = (parent_arr, key_arr) => parent_arr.map((k, i) => [k, key_arr[i]]);

            var zip = parent_arr.map(function(e, i) {
            return [e, key_arr[i]];
            });
            setTreeziparr(zip)
            setTreearr(parent_arr)
            handleClickTree(label, props.key_val, props.node_type,-1, props.key_val)
          }
          e.stopPropagation();
        }}
      >
        <div style={{display:'flex', justifyContent:'space-between'}}>
        <div>{iconDict[label]} {label}</div>
          {hasNodes && (
            <div
              style={{ display: 'inline-block' }}
              // onClick={e => {
              //   if(hasNodes && toggleNode){
              //     toggleNode()
              //   }
              //   // hasNodes && toggleNode && toggleNode();
              //   e.stopPropagation();
              // }}
            >
              {/* <ToggleIcon on={isOpen} /> */}
              {level!==0?<ToggleIcon on={isOpen} />:<ToggleButton  display = {treemenucollapse?'block':'none'} onClick = {handleTreeMenuCollapse}><GiHamburgerMenu/></ToggleButton>}
            </div>
          )}
          
        </div>
      </ListGroupItem>
    );
    
    const handlepdf = (()=>{

    })

    let handleCurrencyClick = (index) => {
      setActiveIndex(index);
      if(index===0){
        window.localStorage.setItem('currency', 'INR')
        //console.log(window.localStorage.getItem('currency'))
        setCurrencyType('INR')
      }else{
        window.localStorage.setItem('currency', 'USD')
        //console.log(window.localStorage.getItem('currency'))
        setCurrencyType('USD')
      }

      const money_converter = {
        $schema: "http://powerbi.com/product/schema#advanced",
        target: {
            table: "Currency Table",
            column: "Currency"
        },
        filterType: models.FilterType.Advanced,
        logicalOperator: "Is",
        conditions: [
            {
                operator: "Is",
                value: window.localStorage.getItem('currency')
            }
        ] 
      }
      for(let i = 0; i<window.reports.length; i++){
        window.reports[i].getActivePage().then(
          (activePage=>{
            activePage.getVisuals().then(
              (visuals=>{
                let slicers = visuals.filter(function (visual) {
                  return visual.type === "slicer";
              });
                slicers.forEach(async (slicer) => {
                const state = await slicer.getSlicerState();  
                
                if(state.targets[0].column==='Currency'){
                  let target_slicer = visuals.filter(function (visual) {
                    return visual.type === "slicer" && visual.name === slicer.name;             
                })[0];
                  await target_slicer.setSlicerState({ filters: [money_converter] });
                }
            })      
              })
            )
          })
        )
      }
      // let curr = window.localStorage.getItem('currency')
      // let year = window.localStorage.getItem('year')
      // let email = window.localStorage.getItem('email')
      // const uploadData = new FormData();
      // uploadData.append('email', email);
      // uploadData.append('year', year);
      // uploadData.append('currency', curr)
      // fetch(`${process.env.REACT_APP_API_ENDPOINT}/usercurrency/`, {
      //     method: 'POST',
      //     body: uploadData
      //   }).then(data => data.json())
      //   .then( data => {
      //         console.log(data)
      //     })
      //   .catch(error => {
      //     // setSignIn(false);
      //     alert('System Error.Contact Admin')
      //     console.log(error)
      // })

    };
    let handleYearClick = (index) =>{
      setyearIndex(index);
      if(index===0){
        window.localStorage.setItem('year', 'CY')
      }else{
        window.localStorage.setItem('year', 'FY')
      }

      const year_converter = {
        $schema: "http://powerbi.com/product/schema#advanced",
        target: {
            table: "Date Parameter",
            column: "Year_Type"
        },
        filterType: models.FilterType.Advanced,
        logicalOperator: "Is",
        conditions: [
            {
                operator: "Is",
                value: window.localStorage.getItem('year')
            }
        ]
      }
      for(let i= 0;i<window.reports.length; i++){
        window.reports[i].getActivePage().then(
          (activePage=>{
            activePage.getVisuals().then(
              (visuals=>{
                let slicers = visuals.filter(function (visual) {
                  return visual.type === "slicer";
              });
                slicers.forEach(async (slicer) => {
                const state = await slicer.getSlicerState();  

                if(state.targets[0].column==='Year_Type'){
                  let target_slicer = visuals.filter(function (visual) {
                    return visual.type === "slicer" && visual.name === slicer.name;             
                })[0];
                  await target_slicer.setSlicerState({ filters: [year_converter] });
                }
            })      
              })
            )
          })
        )
      }


    }

    let handleGearClick = ()=>{
      let val = window.localStorage.getItem('currency_val')
      let conversion_type = window.localStorage.getItem('conversion_type')
      if(conversion_type!==null){
        setConversionType(conversion_type)
      }else{
        setConversionType('Custom')
      }
      if(val!==null){
        console.log(val)
        setCurrencyVal(val)
      }else{
        setCurrencyVal(80)
      }
      setMoneyModalIsOpen(true)
    }


    let incCurrency = ()=>{
      let new_curr = currencyval+1
      if(new_curr>85){
        new_curr=85
      }
      setCurrencyVal(new_curr)
    }
    let decCurrency = ()=>{
      let new_curr = currencyval-1
      if(new_curr<65){
        new_curr = 65
      }
      setCurrencyVal(new_curr)

    }

    let handlemodalSubmitClicked = ()=>{

      if(conversiontype==='Custom'){
        //console.log('custom')
        // //console.log(currencyval)
        window.localStorage.setItem('conversion_type', 'Custom')
        console.log(window.localStorage.getItem('conversion_type'))
        console.log(currencyval)
        window.localStorage.setItem('currency_val', currencyval)
        const currency_valuation = {
          $schema: "http://powerbi.com/product/schema#advanced",
          target: {
              table: "Currency input",
              column: "Currency input"
          },
          filterType: models.FilterType.Advanced,
          logicalOperator: "Is",
          conditions: [
              {
                  operator: "Is",
                  value: currencyval
              }
          ] 
        }
        const usd_selector  = {
          $schema: "http://powerbi.com/product/schema#advanced",
          target: {
              table: "Currency_USD_Type",
              column: "Type"
          },
          filterType: models.FilterType.Advanced,
          logicalOperator: "Is",
          conditions: [
              {
                  operator: "Is",
                  value: conversiontype
              }
          ] 
        }
        //console.log('wrs=', window.reports)

        for (let i =0;i<window.reports.length;i++){
          window.reports[i].getActivePage().then(
            (activePage=>{
              activePage.getVisuals().then(
                (visuals=>{
                  // //console.log('visuals=',visuals)
                  let slicers = visuals.filter(function (visual) {
                    return visual.type === "slicer";
                }
                );
                  slicers.forEach(async (slicer) => {
                    const state = await slicer.getSlicerState();    
                    //console.log("Slicer name: \"" + slicer.name + "\"\nSlicer state:\n", state);

                    //not using state as it will change on page load.page laod code for 1st
                    
                    if(state.targets[0].column==="Type"){
                      let target_slicer = visuals.filter(function (visual) {
                        return visual.type === "slicer" && visual.name === slicer.name;             
                    })[0];
                      await target_slicer.setSlicerState({ filters: [usd_selector] });
                    }

                    if(state.targets[0].column==="Currency input"){
                      let custom_usd_slicer = visuals.filter(function (visual) {
                        return visual.type === "slicer" && visual.name === slicer.name;             
                    })[0]
                      await custom_usd_slicer.setSlicerState({ filters: [currency_valuation] });
                    }

              })      
                  // //console.log('slicer=', slicers)
                })
              )
            })
          )
        }
      }else{
        window.localStorage.setItem('conversion_type', 'Dynamic')
        console.log(window.localStorage.getItem('conversion_type'))
        const usd_selector  = {
          $schema: "http://powerbi.com/product/schema#advanced",
          target: {
              table: "Currency_USD_Type",
              column: "Type"
          },
          filterType: models.FilterType.Advanced,
          logicalOperator: "Is",
          conditions: [
              {
                  operator: "Is",
                  value: conversiontype
              }
          ] 
        }
        for(let i=0;i<window.reports.length;i++){
          window.reports[i].getActivePage().then(
            (activePage=>{
              activePage.getVisuals().then(
                (visuals=>{
                  // //console.log('visuals=',visuals)
                  let slicers = visuals.filter(function (visual) {
                    return visual.type === "slicer";
                }
                );
                  slicers.forEach(async (slicer) => {
                    const state = await slicer.getSlicerState();    
                    //console.log("Slicer name: \"" + slicer.name + "\"\nSlicer state:\n", state);

                    //not using state as it will change on page load.page laod code for 1st
                    
                    if(state.targets[0].column==="Type"){
                      let target_slicer = visuals.filter(function (visual) {
                        return visual.type === "slicer" && visual.name === slicer.name;             
                    })[0];
                      await target_slicer.setSlicerState({ filters: [usd_selector] });
                    }

              })      
                  // //console.log('slicer=', slicers)
                })
              )
            })
          )
        }
      }
      setMoneyModalIsOpen(false);
    }
    // vestigial in new version
    let handleTreeMenuCollapse = ()=>{
      settreeMenuColapse(!treemenucollapse)
      
      for(let i = 0; i<window.reports.length;i++){
        window.reports[i].getActivePage().then(
          (activePage=>{
            if(i==0){
              let active_ht = activePage.defaultSize.height
            let active_width = activePage.defaultSize.width
            let width = document.getElementsByClassName('report-style-class-newreport'+i)[0].offsetWidth;
            let ht = ((active_ht/active_width)*width)
            console.log(active_ht,active_width, width,ht)
            document.getElementsByClassName('report-style-class-newreport'+i)[0].style.height = ht+'px';
            }else{
              let active_ht = activePage.defaultSize.height
            let active_width = activePage.defaultSize.width
            let x = i-1
            let width = document.getElementsByClassName('report-style-class-newreport'+x+1)[0].offsetWidth;
            let ht = ((active_ht/active_width)*width)
            console.log(active_ht,active_width, width,ht)
            document.getElementsByClassName('report-style-class-newreport'+x+1)[0].style.height = ht+'px';
            }
            }))

      }
      }


    let downloadExcel = ()=>{
        setExcelLoader(true)
        setShowExcel(false)
        let client_id = window.localStorage.getItem("clientID")
        let report_name = window.localStorage.getItem("report")
        report_name = parent_props.value
        let report_id = window.localStorage.getItem("rep_id")
        let prop_token = window.localStorage.getItem("token")
        console.log(client_id, report_name)
        fetch(`https://api.benchmarks.digital/newexcel/?client_id=${client_id}&report_name=${report_name}`,{
            method:'GET',
            cache: 'no-store',
            headers:{
              'Content-Type': 'application/json',
              // Authorization: `Token ${prop_token}`
            }
          }).then((res) => res.json())
          .then(
            res => {
                console.log('link= ', res)
                const link = document.createElement('a');
                link.href = res['excel_link'];
                link.download = report_name;
                link.click();
            
                // window.open(res['excel_link'], "_blank")

                setExcelLoader(false)
                setShowExcel(true)
                // onDownloadExcelClickCT(
                //   report_name,
                //   report_id,
                //   client_id.key,
                //   true,
                // )
            }
            )
            .catch( error => {
              onDownloadExcelClickCT(
                report_name,
                report_id,
                client_id.key,
                false,
              )
              console.error(error)
            })
      }


    const platform_option = dropDownData
    const cyfy_option = [{label:'FY'}, {label:'CY'}]
    const dollar_option = [{label:'INR'}, {label:'USD'}]


    const datefilter = {
        $schema: "http://powerbi.com/product/schema#basic",
      target: {
      
      
      
            table: "date_table",
      
      
      
            column: "date"
      
      
      
        },
      
      
      
        filterType: models.FilterType.Advanced,
      
      
      
        logicalOperator: "And",
      
      
      
        conditions: [
      
      
      
            {
                operator: "GreaterThanOrEqual",
                value: window.localStorage.getItem("start_date")+"T00:00:00.000Z"
            },
      
      
      
            {
      
      
      
                operator: "LessThanOrEqual",
      
      
      
                value: window.localStorage.getItem("end_date")+"T00:00:00.000Z"
      
      
      
            }
      
      
      
        ]
      
      
      
      };
      const datefilter_max = {
        $schema: "http://powerbi.com/product/schema#basic",
      target: {
            table: "db_date_table",
            column: "DB_Date"
        },
        filterType: models.FilterType.Advanced,
        logicalOperator: "And",
        conditions: [
            {
              operator:"GreaterThanOrEqual",
              value: window.localStorage.getItem("start_date")+"T00:00:00.000Z"
            },
            {
                operator: "LessThanOrEqual",
                value: window.localStorage.getItem("end_date")+"T00:00:00.000Z"
            }
        ]
      };
      const currencyFilter = {$schema:"http://powerbi.com/product/schema#basic",target:{table:"CurrencyTable", "column":"Currency"},operator:"In",values:["INR"],filterType:models.FilterType.BasicFilter};
      
      const TestProdfilter = {

        $schema: "http://powerbi.com/product/schema#basic",
    
        target: {
    
            table: "Workspace_Table",
    
            column: "Workspace_Name"
    
        },
    
        operator: "In",
    
        
        values:[process.env.REACT_APP_ENVIRONMENT]
    
    };
    
     
    

      const basicFilter = {
        $schema: "http://powerbi.com/product/schema#basic",
        target: {
          table: "content_data main_data",
        "column": "Players"},
      
        operator: "In",
        values:[filterVal],
        // values: [window.localStorage.getItem('report')],
        filterType: models.FilterType.BasicFilter
      };

      const ssFilter = {
        $schema: "http://powerbi.com/product/schema#basic",
        target: {
          table: "ss_content_data player",
        "column": "Players"},
      
        operator: "In",
        values:[filterVal],
        // values: [window.localStorage.getItem('report')],
        filterType: models.FilterType.BasicFilter
      };
      
      const categoryfilter = {$schema:"http://powerbi.com/product/schema#basic",
      target:{table:"ss_content_data parameter",column:"Group_Categories"},
      operator:"In",values:[filterVal]};

      const industryfilter = {
        $schema: "http://powerbi.com/product/schema#basic",
    target: {
        table: "content_data industry",
        column: "industry_name"
    },
    operator: "In",
    values: [filterVal]
      }

      const sectorfilter = {
        $schema: "http://powerbi.com/product/schema#basic",
        target: {
            table: "Grocery_Sector_Table",
            column: "Sector"
        },
        operator: "In",
        values: [filterVal]
      }

      const medicalCategoryFilter = {
        $schema:"http://powerbi.com/product/schema#basic",
        target:{table:"ss_content_data parameter",column:"Group_Categories"},
        operator:"In",values:[filterVal]
      } 

      let filter_arr = [datefilter, datefilter_max, TestProdfilter]
      // let filter_arr = []
      // if (parent_props.initialOpenNodes=='275'){
      //   filter_arr.push(basicFilter)
      // }
      for(let i=0; i<filterarr.length;i++){
        if(filterarr[i]==1){
          filter_arr.push(basicFilter)
        }else if(filterarr[i]==='categories'){
          filter_arr.push(categoryfilter)
        } else if(filterarr[i]==='industry'){
          filter_arr.push(industryfilter)
        } else if (filterarr[i]==='sector'){
          console.log('sectorfilter')
          filter_arr.push(sectorfilter)
        } else if (filterarr[i]==='medical'){
          console.log('medicalfilter')
          filter_arr.push(medicalCategoryFilter)
        } else if (filterarr[i]==='ssFilter'){
          console.log('ssFilter')
          filter_arr.push(ssFilter)
        }
      }
    

    if(window.localStorage.getItem('loginStatus')!=='true'){
        return <Navigate to = "/"/>
      }

      // const treeData = [
      //   {
      //     key: 'first-level-node-1',
      //     label: 'Death',
      //     name:'pain', // any other props you need, e.g. url
      //     nodes: [
      //       {
      //         key: 'second-level-node-1',
      //         label: 'cote',
      //         name:'killer',
      //         nodes: [
      //           {
      //             key: 'third-level-node-1',
      //             label: 'code',
      //             name:'purgatory',
      //             subscribed:true,
      //             nodes: [] // you can remove the nodes property or leave it as an empty array
      //           },
      //         ],
      //       },
      //     ],
      //   },
      //   {
      //     key: 'first-level-node-2',
      //     label: 'geass',
      //     name:'plain',
      //     subscribed:true
      //   },
      // ];

     

      function afterOpenModal() {
        // references are now sync'd and can be accessed.
        // subtitle.style.color = '#f00';
        //console.log('modalopen')
      }
      function closeModal() {
        setIsOpen(false);
        setMoneyModalIsOpen(false);
      }

  return (
    <div>
        <BodyContainer>
          <SideMenuContainer width={treemenucollapse ? '20vw' : '0px'}>
              <TreeMenu
              style = {{width:'25vw'}}
              data={myPages}
              initialOpenNodes = {parent_props.initialOpenNodes}
              onClickItem={({ key, label, ...props }) => {
              }}
              >
                {({ search, items, searchTerm }) => {
                  // const nodesForRender = getNodesForRender(items, searchTerm);
                  return (
                  <div style={{paddingLeft:'10px', marginTop:'5px'}}>
                    <ListGroup>
                      {items.map(props => (
                        <ListItem  {...props} />
                      ))}
                    </ListGroup>
                  </div>
            )}}
            </TreeMenu>
          </SideMenuContainer>

          {showLoader===false?<PowerBiDiv width={treemenucollapse ? '80vw' : '100vw'}>
                  <div style={{'backgroundColor':'#FFFFFF', 'minHeight':'10vh'}}>
                    <Header1>
                        <LHSTitlediv>{selectedpage}</LHSTitlediv>
                        <RHSTitlediv>
                          
                          {/* {window.localStorage.getItem('email').endsWith('@redseerconsulting.com')?<ChangeLogbutton  onClick={() => {
  window.location.href = `/changeLogs/?industry_id=${parent_props.industry_id}&industry_name=${selectedpage}&pass_key=${url_key}`;
}}>Data Revision History</ChangeLogbutton>:null} */}
                      </RHSTitlediv>
                    </Header1>
                    
                    <div className='breadcrumbs' style={treemenucollapse?{'marginLeft':'3vw' ,'marginBottom':'10px', 'color':'grey'}:{'marginLeft':'3.5vw' ,'marginBottom':'10px', 'color':'grey', 'marginRight':'3.5vw'}}>                       

                      <div>
                        {parent_props.headarr.length > 0 ? (
                          <>
                            {treeziparr?.map((val, i) => (
                              <BreadCrumbSpan
                                index = {i}
                                onClick={(e) => {
                                  if(i>1){
                                    window.location.href = `/Report3/?val=${val[0]}&key=${val[1]}&filter=${val[3]}&filter_value=${val[2]}&industry_id=${parent_props.industry_id}`
                                  }
                                  
                                  // handleClickTree(val[0], -2, "-1", i, val[1]);
                                }}
                                key={i}
                              >
                                {val[0]} /{" "}
                              </BreadCrumbSpan>
                            ))}
                            {showSection1===false && (<BreadCrumbSpan>Redseer Analysis / </BreadCrumbSpan>)}
                            {showArticleDetail && (
                              <BreadCrumbSpan>Content / </BreadCrumbSpan>
                            )}
                          </>
                        ) : (
                          <span className="breadcrumbs">Consumer Internet</span>
                        )}
                      </div>
                      <div>
                      {!quickLinks?null:'Quick Links:'} {quickLinks?.map((val, i) => (
                              <BreadCrumbQuickSpan
                                index = {i}
                                onClick={(e) => {
                                  handlelinkClick(val);
                                }}
                                key={i}
                              >
                                {val.label}{i < quickLinks.length - 1 ? ' | ' : ''}
                              </BreadCrumbQuickSpan>
                            ))}

                      </div>

                    </div>
                    
                    
                    <div>
                      <Modal
                      isOpen={moneymodalIsOpen}
                      onAfterOpen={afterOpenModal}
                      onRequestClose={closeModal}
                      style={customStyles}
                      contentLabel="Example Modal"
                    >
                      <div>Please select USD conversion rate</div>
                      <form>
                        <input 
                            type="radio" 
                            id="html" 
                            name="fav_language"  
                            onClick={() => setConversionType('Dynamic')} 
                            value='Dynamic' 
                            checked={conversiontype === 'Dynamic'}
                        />
                        <label style={{'marginLeft':'5px'}}>Dynamic</label><br/>

                        <input 
                            type="radio" 
                            id="css" 
                            name="fav_language" 
                            onClick={() => setConversionType('Custom')} 
                            value="Custom" 
                            checked={conversiontype === 'Custom'}
                        />
                        <label style={{'marginLeft':'5px'}}> Custom</label><br/>
                    </form>

                      <div>
                        <button style={{'marginRight':'5px', 'borderRadius':'5px', 'width':'30px'}} onClick = {decCurrency}>-</button>
                        <input  value={currencyval}/>
                        <button style={{'marginLeft':'5px', 'borderRadius':'5px', 'width':'30px'}} onClick = {incCurrency}>+</button>
                      </div>
                      <button style={{'color':'white', 'backgroundColor':'#4867AA','marginTop':'10px', 'borderRadius':'6px'}} onClick={handlemodalSubmitClicked}>Submit</button>
                    </Modal>
                    </div>
                  </div>
                  {showReport?<PowerBiDiv>
                    
                    {!showArticleDetail && showSection4?<div><Testing industry_id={parent_props.industry_id} report = {parent_props.value} dropDownData={dropDownData} onPlayerClick={onPlayerClick}/></div>:null}
                    {/* {!showArticleDetail ? (
                      <QueryFooter/>
                    ) : null} */}
                   
                    {!showArticleDetail && <Header>
                        {showSection1?<Monthdiv>Time Trend</Monthdiv>:<Monthdiv>Redseer Analysis</Monthdiv>}
                        <RHSTitlediv>
                        <ExcelDiv>
                        {excelLoader?<TailSpin
                        height="20"
                        width="20"
                        color="#18183E"
                        ariaLabel="tail-spin-loading"
                        radius="1"
                        // wrapperStyle={{ position: "fixed", top: "50%", left: "60%", transform: "translate(-50%, -50%)"}}
                        wrapperClass=""
                        visible={true}
                      />:null}
                       {showExcel?<ExcelButton onClick={()=>downloadExcel()}>Export Data</ExcelButton>:null}
                      </ExcelDiv>
                          <MyDropdown options={cyfy_option}
                          onOptionSelect={onYearSelect}
                          prev_value = {yearOption}
                          default_head = {window.localStorage.getItem('year')?window.localStorage.getItem('year'):'CY'}                          
                          />
                          <Geardiv>
                          <BsGear style={{ 'fontSize': "20px" }} onClick={()=>handleGearClick()}/>
                          </Geardiv>
                          <MyDropdown options={dollar_option}
                          onOptionSelect={onMoneySelect}
                          prev_value = {moneyOption}
                          default_head = {window.localStorage.getItem('currency')?window.localStorage.getItem('currency'):'INR'}
                          />
                          {showSection1 && platform_option.length>0?<LinkDropDown options={platform_option}
                          onOptionSelect={onOptionSelect}
                          prev_value = {selectedOption}
                          default_head = {'Select Platform'}
                          buttonstyle = {{minWidth:'125px'}}
                          style = {{backgroundColor:'red', minWidth:'125px'}}
                          />:null}
                      </RHSTitlediv>
                     
                    </Header>}
                  {!showArticleDetail ? showSection1?<div>
                    {console.log('test')}
                    {FirstPage.length>0 && FirstPage.map((index,i) => {
                      if (index.page_type==='component'){
                        const Component = DynamicComponent(index.address);
                        console.log('firstpage')
                        return (
                          <Suspense key={index} fallback={<div>Loading...</div>}>
                            <Component val = {index.component_variable}/>
                          </Suspense>
                        );
                      }else if(index.page_type==='httpsAddress'){
                        let url = ''
                        if (index.component_variable) {
                            const jsonString = index.component_variable.replace(/'/g, '"');
                            // Parse the JSON string into an object
                            const componentObject = JSON.parse(jsonString);
                            if (componentObject.name.startsWith("$")) {
                                // Replace the value with the desired value, e.g., 'Maaza'
                                componentObject.name = parent_props.value;
                              }
                            // Create a query string from the componentObject
                            const queryParams = new URLSearchParams(componentObject).toString();
                            console.log('query_params = ', queryParams)
                            url = `${index.address}?${queryParams}`;
                          }else{
                            url = index.address
                          }
                    
                        // const url = queryParams ? `${index.address}?${queryParams}` : index.address;
                        console.log('url = ', url)
                        return(
                          <div onMouseEnter={onMouseEnterPage} onMouseLeave={()=>onMouseLeavePage(index.page_name, index.page_type, index.section_type)}>
                          <DjangoEmbed address={url}/>
                          </div>
                        )
                      }else if (index.page_type==='pdf'){
                        return(
                          <iframe src={index['url']} width="100%" height="800px" frameBorder="0" scrolling="yes" allowFullScreen='yes' title="Analysis ppt merged.pdf"></iframe>
                        )
                      }else{
                        if(index.component_variable){
                    
                          let jsonObj = JSON.parse(index.component_variable.replace(/'/g, '"'))
                          for (let key in jsonObj) {
                            if (jsonObj.hasOwnProperty(key)) {
                                let value = jsonObj[key]
                                if (value.startsWith("$")) {
                                  value = parent_props.value;
                                }
                              if(key==='player'){
                                const basicFilter1 = {
                                  $schema: "http://powerbi.com/product/schema#basic",
                                  target: {
                                    table: "content_data main_data",
                                  "column": "Players"},
                    
                                  operator: "In",
                                  values:[value],
                                  // values: [window.localStorage.getItem('report')],
                                  filterType: models.FilterType.BasicFilter
                                };
                                if(index.free_page===true){
                                  filter_arr = []
                                }
                                filter_arr.push(basicFilter1)
                              }else if(key==='categories'){
                                const categoryfilter1 = {$schema:"http://powerbi.com/product/schema#basic",
                              target:{table:"ss_content_data parameter",column:"Group_Categories"},
                              operator:"In",values:[value]};
                                filter_arr.push(categoryfilter1)
                              } else if(key==='industry'){
                                const industryfilter1 = {
                                  $schema: "http://powerbi.com/product/schema#basic",
                                  target: {
                                      table: "content_data industry",
                                      column: "industry_name"
                                  },
                                  operator: "In",
                                  values: [value]
                                }
                                filter_arr.push(industryfilter1)
                              } else if (key==='sector'){
                                const sectorfilter1 = {
                                  $schema: "http://powerbi.com/product/schema#basic",
                                  target: {
                                      table: "Grocery_Sector_Table",
                                      column: "Sector"
                                  },
                                  operator: "In",
                                  values: [value]
                                }
                                filter_arr.push(sectorfilter1)
                              } else if (key==='medical'){
                                  const medicalCategoryFilter1= {
                                  $schema:"http://powerbi.com/product/schema#basic",
                                  target:{table:"ss_content_data parameter",column:"Group_Categories"},
                                  operator:"In",values:[value]
                                }
                                filter_arr.push(medicalCategoryFilter1)
                              }
                            }
                          }
                          }
                        console.log('firstpagelog')
                        return(
                          <div key={index.id} onMouseEnter={onMouseEnterPage} onMouseLeave={()=>onMouseLeavePage(index.page_name, index.page_type, index.section_type)}>
                            {/* {index.has_address===true &&(<><DjangoEmbed address={index.address}/></>)} */}
                            {true && (<>
                            <PowerBIEmbed
                             embedConfig = {{
                              type: 'report',   // Supported types: report, dashboard, tile, visual and qna
                              id: index['powerbi_report_id'],
                              //get from props
                              embedUrl:index['url'],
                              accessToken: index['embed'],
                              tokenType: models.TokenType.Embed,
                              filters: filter_arr,
                              settings: {
                                // background:models.BackgroundType.Transparent,
                                layoutType:models.LayoutType.Custom,
                                customLayout:{
                                  displayOption:models.DisplayOption.FitToPage
                                },
                                panes: {
                                  filters: {
                                    expanded: false,
                                    visible: false,
                                  },
                                },
                                navContentPaneEnabled:false
                              }
                            }}
                            eventHandlers = {
                              new Map([
                                ['loaded', async function (event, report) {
                                  //console.log('Report loaded');
                                  // setRenderComponentB(true)
                                  if(true){
                                    const filter = {
                                      $schema: "http://powerbi.com/product/schema#advanced",
                                      target: {
                                          table: "content_data player",
                                          column: "player_name"
                                      },
                                      filterType: models.FilterType.Advanced,
                                      logicalOperator: "Is",
                                      conditions: [
                                          {
                                              operator: "Is",
                                              value: window.localStorage.getItem("player_name")
                                          }
                                      ]
                                  };
                                  // filter if there is a player in dropdown menu instead of page
                                  const filter_player_dropdown = {
                                    $schema: "http://powerbi.com/product/schema#advanced",
                                      target: {
                                          table: "content_data main_data",
                                          column: "Players"
                                      },
                                      filterType: models.FilterType.Advanced,
                                      logicalOperator: "Is",
                                      conditions: [
                                          {
                                              operator: "Is",
                                              value: window.localStorage.getItem('drop_dn_player_name')
                                          }
                                      ]
                                  }
                                  const money_converter = {
                                    $schema: "http://powerbi.com/product/schema#advanced",
                                    target: {
                                        table: "Currency Table",
                                        column: "Currency"
                                    },
                                    filterType: models.FilterType.Advanced,
                                    logicalOperator: "Is",
                                    conditions: [
                                        {
                                            operator: "Is",
                                            value: window.localStorage.getItem('currency')
                                        }
                                    ]
                                  }
                                  const year_converter = {
                                    $schema: "http://powerbi.com/product/schema#advanced",
                                    target: {
                                        table: "Date Parameter",
                                        column: "Year_Type"
                                    },
                                    filterType: models.FilterType.Advanced,
                                    logicalOperator: "Is",
                                    conditions: [
                                        {
                                            operator: "Is",
                                            value: window.localStorage.getItem('year')
                                        }
                                    ]
                                  }
                                  const currency_valuation = {
                                    $schema: "http://powerbi.com/product/schema#advanced",
                                    target: {
                                        table: "Currency input",
                                        column: "Currency input"
                                    },
                                    filterType: models.FilterType.Advanced,
                                    logicalOperator: "Is",
                                    conditions: [
                                        {
                                            operator: "Is",
                                            value: 71
                                        }
                                    ]
                                  }
                                  const usd_selector  = {
                                    $schema: "http://powerbi.com/product/schema#advanced",
                                    target: {
                                        table: "Currency_USD_Type",
                                        column: "Type"
                                    },
                                    filterType: models.FilterType.Advanced,
                                    logicalOperator: "Is",
                                    conditions: [
                                        {
                                            operator: "Is",
                                            value: 'Custom'
                                        }
                                    ]
                                  }
                    
                                  report.getActivePage().then(
                                    (activePage=>{
                                      let active_ht = activePage.defaultSize.height
                                      let active_width = activePage.defaultSize.width
                                      let width = document.getElementsByClassName('report-style-class-newreport'+i)[0].offsetWidth;
                                      let ht = ((active_ht/active_width)*width)
                                      const removeLink = () => {
                                        const links = document.querySelectorAll('link[href^="https://content.powerapps.com/resource/powerappsportal/dist/preform"]');
                                        links.forEach((link) => {
                                          link.remove();
                                        });
                                      };
                                      removeLink()
                                    //   if(i==0){
                                    //     if(window.localStorage.getItem('finalized')==='false'){
                                    //       document.getElementsByClassName('report-style-class-newreport'+i)[0].style.marginTop = '-12vh';
                                    //     }
                                    //   }
                                      document.getElementsByClassName('report-style-class-newreport'+i)[0].style.height = ht+'px';
                                      document.getElementsByClassName('report-style-class-newreport'+i)[0].style.backgroundColor = '#F9FDFF'
                                      document.getElementsByClassName('report-style-class-newreport'+i)[0].children[0].style.border = '0px';
                                      document.getElementsByClassName('report-style-class-newreport'+i)[0].children[0].style.backgroundColor = '#F9FDFF';
                                      // document.getElementsByClassName('report-style-class-newreport'+i)[0].querySelector('iframe').style.marginLeft = '-10px';
                                      activePage.getVisuals().then(
                                        (visuals=>{
                                          // //console.log('visuals=',visuals)
                                          let slicers = visuals.filter(function (visual) {
                                            return visual.type === "slicer";
                                        }
                                        );
                                          slicers.forEach(async (slicer) => {
                                            const state = await slicer.getSlicerState();
                                            if(state.targets[0].column==="player_name"){
                                              let target_slicer = visuals.filter(function (visual) {
                                                return visual.type === "slicer" && visual.name === slicer.name;
                                            })[0];
                                              await target_slicer.setSlicerState({ filters: [filter] });
                                            }
                                            if(state.targets[0].column==='Players' && window.localStorage.getItem('filter_on_company')==='true'){
                                              let target_slicer = visuals.filter(function (visual) {
                                                return visual.type === "slicer" && visual.name === slicer.name;
                                            })[0];
                                              await target_slicer.setSlicerState({ filters: [filter_player_dropdown] });
                                            }
                                            if(state.targets[0].column==='Currency'){
                                              let target_slicer = visuals.filter(function (visual) {
                                                return visual.type === "slicer" && visual.name === slicer.name;
                                            })[0];
                                              await target_slicer.setSlicerState({ filters: [money_converter] });
                                            }
                                            if(state.targets[0].column==='Year_Type'){
                                              let target_slicer = visuals.filter(function (visual) {
                                                return visual.type === "slicer" && visual.name === slicer.name;
                                            })[0];
                                              await target_slicer.setSlicerState({ filters: [year_converter] });
                                            }
                                      })
                                        })
                                      )
                                    })
                                  )
                                  }
                                }],
                                ['rendered', function () {
                                }],
                                ['buttonClicked', function(event, report){
                                  let ques_name = event.detail['title']
                                  let company_name = ''
                                  window.report.getActivePage().then(
                                    (activePage=>{
                                      activePage.getVisuals().then(
                                        (visuals=>{
                                          let slicers = visuals.filter(function (visual) {
                                            return visual.type === "slicer";
                                        });
                                          slicers.forEach(async (slicer) => {
                                          const state = await slicer.getSlicerState();
                                      })
                                        })
                                      )
                                    })
                                  )
                                }],
                                ['error', function (event) {console.log('powerbi_error=',event.detail);}]
                              ])
                            }
                    
                            cssClassName = { "report-style-class-newreport"+i}
                            getEmbeddedComponent = {async(embeddedReport) => {
                              if(window.reports === undefined) {
                                window.reports=[]
                              }
                              window.reports.push(embeddedReport);
                              // console.log(window.reports)
                            }
                    
                          }
                           />
                           </>)}
                          </div>
                        )
                      }
                                    })}
                                    {showLoaddiv?<Loaddiv><Textdiv onClick =  {handleLoadMore}>{loadMore?<>Hide</>:<>Load More</>}<IoIosArrowDown/></Textdiv></Loaddiv>:null}
                    
                                    <div style={loadCount>0 && loadCount%2 != 0? {} : { display: 'none' }}>
                                      {loadCount>0 && newReportPages.map((index,i) => {
                                        if (index.page_type==='component'){
                                          const Component = DynamicComponent(index.address);
                                          console.log('others')
                                          return (
                                            <Suspense key={index} fallback={<div>Loading...</div>}>
                                              <Component val = {index.component_variable}/>
                                            </Suspense>
                                          );
                                        }else if(index.page_type==='httpsAddress'){
                                          let url = ''
                                          if (index.component_variable) {
                                              const jsonString = index.component_variable.replace(/'/g, '"');
                                              // Parse the JSON string into an object
                                              const componentObject = JSON.parse(jsonString);
                                              if (componentObject.name.startsWith("$")) {
                                                  // Replace the value with the desired value, e.g., 'Maaza'
                                                  componentObject.name = parent_props.value;
                                                }
                                              // Create a query string from the componentObject
                                              const queryParams = new URLSearchParams(componentObject).toString();
                                              console.log('query_params = ', queryParams)
                                              url = `${index.address}?${queryParams}`;
                                            }else{
                                              url = index.address
                                            }
                                      
                                          // const url = queryParams ? `${index.address}?${queryParams}` : index.address;
                                          console.log('url = ', url)
                                          return(
                                            <div onMouseEnter={onMouseEnterPage} onMouseLeave={()=>onMouseLeavePage(index.page_name, index.page_type, index.section_type)}>
                                            <DjangoEmbed address={url}/>
                                            </div>
                                          )
                                        }else{
                                          if(index.component_variable){
                                      
                                          let jsonObj = JSON.parse(index.component_variable.replace(/'/g, '"'))
                                          console.log('filterobj=', jsonObj)
                                          for (let key in jsonObj) {
                                            if (jsonObj.hasOwnProperty(key)) {
                                                let value = jsonObj[key]
                                                if (value.startsWith("$")) {
                                                  value = parent_props.value;
                                                }
                                              if(key==='player'){
                                                const basicFilter1 = {
                                                  $schema: "http://powerbi.com/product/schema#basic",
                                                  target: {
                                                    table: "content_data main_data",
                                                  "column": "Players"},
                                      
                                                  operator: "In",
                                                  values:[value],
                                                  // values: [window.localStorage.getItem('report')],
                                                  filterType: models.FilterType.BasicFilter
                                                };
                                                if(index.free_page===true){
                                                  filter_arr = []
                                                }
                                                console.log(index.free_page, filter_arr)
                                                filter_arr.push(basicFilter1)
                                              }else if(key==='categories'){
                                                const categoryfilter1 = {$schema:"http://powerbi.com/product/schema#basic",
                                              target:{table:"ss_content_data parameter",column:"Group_Categories"},
                                              operator:"In",values:[value]};
                                                filter_arr.push(categoryfilter1)
                                              } else if(key==='industry'){
                                                const industryfilter1 = {
                                                  $schema: "http://powerbi.com/product/schema#basic",
                                                  target: {
                                                      table: "content_data industry",
                                                      column: "industry_name"
                                                  },
                                                  operator: "In",
                                                  values: [value]
                                                }
                                                filter_arr.push(industryfilter1)
                                              } else if (key==='sector'){
                                                const sectorfilter1 = {
                                                  $schema: "http://powerbi.com/product/schema#basic",
                                                  target: {
                                                      table: "Grocery_Sector_Table",
                                                      column: "Sector"
                                                  },
                                                  operator: "In",
                                                  values: [value]
                                                }
                                                filter_arr.push(sectorfilter1)
                                              } else if (key==='medical'){
                                                  const medicalCategoryFilter1= {
                                                  $schema:"http://powerbi.com/product/schema#basic",
                                                  target:{table:"ss_content_data parameter",column:"Group_Categories"},
                                                  operator:"In",values:[value]
                                                }
                                                filter_arr.push(medicalCategoryFilter1)
                                              }
                                            }
                                          }
                                          }
                                      
                                          return(
                                            <div key={index.id} onMouseEnter={onMouseEnterPage} onMouseLeave={()=>onMouseLeavePage(index.page_name, index.page_type, index.section_type)}>
                                              {true && (<><PowerBIEmbed
                                              embedConfig = {{
                                                type: 'report',   // Supported types: report, dashboard, tile, visual and qna
                                                id: index['powerbi_report_id'],
                                                //get from props
                                                embedUrl:index['url'],
                                                accessToken: index['embed'],
                                                tokenType: models.TokenType.Embed,
                                                filters: filter_arr,
                                                settings: {
                                                  // background:models.BackgroundType.Transparent,
                                                  layoutType:models.LayoutType.Custom,
                                                  customLayout:{
                                                    displayOption:models.DisplayOption.FitToPage
                                                  },
                                                  panes: {
                                                    filters: {
                                                      expanded: false,
                                                      visible: false,
                                                    },
                                                  },
                                                  navContentPaneEnabled:false
                                                }
                                              }}
                                              eventHandlers = {
                                                new Map([
                                                  ['loaded', async function (event, report) {
                                                    
                                                    //console.log('Report loaded');
                                                    if(true){
                                                      const filter = {
                                                        $schema: "http://powerbi.com/product/schema#advanced",
                                                        target: {
                                                            table: "content_data player",
                                                            column: "player_name"
                                                        },
                                                        filterType: models.FilterType.Advanced,
                                                        logicalOperator: "Is",
                                                        conditions: [
                                                            {
                                                                operator: "Is",
                                                                value: window.localStorage.getItem("player_name")
                                                            }
                                                        ]
                                                    };
                                                    // filter if there is a player in dropdown menu instead of page
                                                    const filter_player_dropdown = {
                                                      $schema: "http://powerbi.com/product/schema#advanced",
                                                        target: {
                                                            table: "content_data main_data",
                                                            column: "Players"
                                                        },
                                                        filterType: models.FilterType.Advanced,
                                                        logicalOperator: "Is",
                                                        conditions: [
                                                            {
                                                                operator: "Is",
                                                                value: window.localStorage.getItem('drop_dn_player_name')
                                                            }
                                                        ]
                                                    }
                                                    const money_converter = {
                                                      $schema: "http://powerbi.com/product/schema#advanced",
                                                      target: {
                                                          table: "Currency Table",
                                                          column: "Currency"
                                                      },
                                                      filterType: models.FilterType.Advanced,
                                                      logicalOperator: "Is",
                                                      conditions: [
                                                          {
                                                              operator: "Is",
                                                              value: window.localStorage.getItem('currency')
                                                          }
                                                      ]
                                                    }
                                                    const year_converter = {
                                                      $schema: "http://powerbi.com/product/schema#advanced",
                                                      target: {
                                                          table: "Date Parameter",
                                                          column: "Year_Type"
                                                      },
                                                      filterType: models.FilterType.Advanced,
                                                      logicalOperator: "Is",
                                                      conditions: [
                                                          {
                                                              operator: "Is",
                                                              value: window.localStorage.getItem('year')
                                                          }
                                                      ]
                                                    }
                                                    const currency_valuation = {
                                                      $schema: "http://powerbi.com/product/schema#advanced",
                                                      target: {
                                                          table: "Currency input",
                                                          column: "Currency input"
                                                      },
                                                      filterType: models.FilterType.Advanced,
                                                      logicalOperator: "Is",
                                                      conditions: [
                                                          {
                                                              operator: "Is",
                                                              value: 71
                                                          }
                                                      ]
                                                    }
                                                    const usd_selector  = {
                                                      $schema: "http://powerbi.com/product/schema#advanced",
                                                      target: {
                                                          table: "Currency_USD_Type",
                                                          column: "Type"
                                                      },
                                                      filterType: models.FilterType.Advanced,
                                                      logicalOperator: "Is",
                                                      conditions: [
                                                          {
                                                              operator: "Is",
                                                              value: 'Custom'
                                                          }
                                                      ]
                                                    }
                                      
                                                    report.getActivePage().then(
                                                      (activePage=>{
                                                        let active_ht = activePage.defaultSize.height
                                                        let active_width = activePage.defaultSize.width
                                                        let width = document.getElementsByClassName('report-style-class-newreport'+i+1)[0].offsetWidth;
                                                        let ht = ((active_ht/active_width)*width)
                                                        const removeLink = () => {
                                                          const links = document.querySelectorAll('link[href^="https://content.powerapps.com/resource/powerappsportal/dist/preform"]');
                                                          links.forEach((link) => {
                                                            link.remove();
                                                          });
                                                        };
                                                        removeLink()
                                                      //   if(i==0){
                                                      //     if(window.localStorage.getItem('finalized')==='false'){
                                                      //       document.getElementsByClassName('report-style-class-newreport'+i+1)[0].style.marginTop = '-12vh';
                                                      //     }
                                                      //   }
                                                        document.getElementsByClassName('report-style-class-newreport'+i+1)[0].style.height = ht+'px';
                                                        document.getElementsByClassName('report-style-class-newreport'+i+1)[0].style.backgroundColor = '#F9FDFF'
                                                        document.getElementsByClassName('report-style-class-newreport'+i+1)[0].children[0].style.border = '0px';
                                                        document.getElementsByClassName('report-style-class-newreport'+i+1)[0].children[0].style.backgroundColor = '#F9FDFF';
                                                        // document.getElementsByClassName('report-style-class-newreport'+i+1)[0].querySelector('iframe').style.marginLeft = '-10px';
                                                        activePage.getVisuals().then(
                                                          (visuals=>{
                                                            // //console.log('visuals=',visuals)
                                                            let slicers = visuals.filter(function (visual) {
                                                              return visual.type === "slicer";
                                                          }
                                                          );
                                                            slicers.forEach(async (slicer) => {
                                                              const state = await slicer.getSlicerState();
                                                              //console.log("Slicer name: \"" + slicer.name + "\"\nSlicer state:\n", state);
                                                              if(state.targets[0].column==="player_name"){
                                                                //console.log('slicer_name=',slicer)
                                                                let target_slicer = visuals.filter(function (visual) {
                                                                  return visual.type === "slicer" && visual.name === slicer.name;
                                                              })[0];
                                                                await target_slicer.setSlicerState({ filters: [filter] });
                                                              }
                                                              //not using state as it will change on page load.page laod code for 1st
                                                              if(state.targets[0].column==='Players' && window.localStorage.getItem('filter_on_company')==='true'){
                                                                let target_slicer = visuals.filter(function (visual) {
                                                                  return visual.type === "slicer" && visual.name === slicer.name;
                                                              })[0];
                                                                await target_slicer.setSlicerState({ filters: [filter_player_dropdown] });
                                                              }
                                                              if(state.targets[0].column==='Currency'){
                                                                let target_slicer = visuals.filter(function (visual) {
                                                                  return visual.type === "slicer" && visual.name === slicer.name;
                                                              })[0];
                                                                await target_slicer.setSlicerState({ filters: [money_converter] });
                                                              }
                                                              if(state.targets[0].column==='Year_Type'){
                                                                let target_slicer = visuals.filter(function (visual) {
                                                                  return visual.type === "slicer" && visual.name === slicer.name;
                                                              })[0];
                                                                await target_slicer.setSlicerState({ filters: [year_converter] });
                                                              }
                                                        })
                                                            // //console.log('slicer=', slicers)
                                                          })
                                                        )
                                                      })
                                                    )
                                                    }
                                                  }],
                                                  ['rendered', function () {
                                                  }],
                                                  ['buttonClicked', function(event, report){
                                                    let ques_name = event.detail['title']
                                                    let company_name = ''
                                                    window.report.getActivePage().then(
                                                      (activePage=>{
                                                        activePage.getVisuals().then(
                                                          (visuals=>{
                                                            let slicers = visuals.filter(function (visual) {
                                                              return visual.type === "slicer";
                                                          });
                                                            slicers.forEach(async (slicer) => {
                                                            const state = await slicer.getSlicerState();
                                                            // //console.log("Slicer name: \"" + slicer.name + "\"\nSlicer state:\n", state);
                                                            if(state.targets[0].column==="player_name"){
                                                              company_name=state.filters[0].values[0]
                                                              // company_name=window.sessionStorage.getItem("player_name")
                                                              // openModal(company_name, ques_name)
                                                            }
                                      
                                                        })
                                                            // //console.log('slicer=', slicers)
                                                          })
                                                        )
                                                      })
                                                    )
                                                  }],
                                                  ['error', function (event) {console.log('powerbi_error=',event.detail);}]
                                                ])
                                              }
                                      
                                              cssClassName = { "report-style-class-newreport"+i+1}
                                              getEmbeddedComponent = {async(embeddedReport) => {
                                                if(window.reports === undefined) {
                                                  window.reports=[]
                                                }
                                                window.reports.push(embeddedReport);
                                                // console.log(window.reports)
                                              }
                                      
                                            }
                                            /></>)}
                                            </div>
                                          )
                                        }
                                      })}
                                    </div>
                {showsub?<Sub onSubmitResultToCT={onSubmitResultToCT} />:null}
                {showIncomeStatement.current?<div><MyStackedBarChart width={"100%"} height={"70vh"}/></div>:null}
                <QueryFooter/>
                
                {showSectionReadyRec && <ComponentWrapper><ReadyReckoner width={"100%"} company_id={Number(window.localStorage.getItem('player_id'))} start_date = {window.localStorage.getItem('start_date')} end_date={window.localStorage.getItem('end_date')} /></ComponentWrapper>}
                {showSection5 && <ComponentWrapper><BeadGraph width={"100%"} height={"100%"} industry_id_list={[Number(window.localStorage.getItem('industry_id'))]}/></ComponentWrapper>}
                {showSection6rev &&  <div>
                  <WaterFallWalletShare width={"100%"} height={'70vh'}/>
                </div>}
                {showRelatedDataSet && <ComponentWrapper><RelatedDataSets width={"100%"} height={"100%"} report_id={Number(window.localStorage.getItem('rep_id'))}/></ComponentWrapper>}
                {section2Pages.length > 0 &&
                  redseerAnalysis &&
                  !isAnalysisLoading && (
                    <div>
                      <Analysisdiv>Redseer Analysis</Analysisdiv>
                      <AnalysisBoxWrapper>
                        <AnalysisBox>
                          {/<[^>]+>/i.test(redseerAnalysis.text) ? (
                            <AnalysisBoxBody>
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: redseerAnalysis.text,
                                }}
                              />
                            </AnalysisBoxBody>
                          ) : (
                            <AnalysisBoxBody>
                              {redseerAnalysis.text}
                            </AnalysisBoxBody>
                          )}
                          <AnalysisBoxLink
                            onClick={() => {
                              setShowSection1(false);
                            }}
                          >
                            View Analysis
                          </AnalysisBoxLink>
                        </AnalysisBox>
                      </AnalysisBoxWrapper>
                    </div>
                  )}
                {showSection7swot &&  <div>
                  <h3 style = {{'paddingLeft':'3.5vw'}}>{selectedpage} SWOT  Analysis </h3>
                  <Swot/>
                </div>}
                {/* <QueryFooter/> */}
                {section3Pages.length>0 && <ArticlesList pages={section3Pages} setSelectedArticle={(article)=>{
                    const currReportId = window.localStorage.getItem("rep_id");
                    const currReportName = window.localStorage.getItem("report");
                    if (selectedOption) {
                      const selectedCP = platform_option.filter(
                        (opt) => opt.label === selectedOption
                      )?.[0];
                      onReadContentClickCT(
                        article?.data?.title?.rendered || "",
                        article?.data?.link || "",
                        article?.type || "",
                        window.localStorage.getItem("clientID"),
                        currReportName,
                        currReportId,
                        "Company Profile",
                        selectedCP.label,
                        selectedCP.key,
                      )
                    } else {
                      onReadContentClickCT(
                        article?.data?.title?.rendered || "",
                        article?.data?.link || "",
                        article?.type || "",
                        window.localStorage.getItem("clientID"),
                        currReportName,
                        currReportId,
                        "Sector Profile",
                      )
                    }
                    setSelectedArticle(article)
                  }}/>}
                </div>:<div>
                {section2Pages.map((index,i) => {
                    if (index.page_type==='component'){
                      const Component = DynamicComponent(index.address);
                      console.log('others')
                      return (
                        <Suspense key={index} fallback={<div>Loading...</div>}>
                          <Component val = {index.component_variable}/>
                        </Suspense>
                      );
                    }else if(index.page_type==='httpsAddress'){
                      let url = ''
                      if (index.component_variable) {
                          const jsonString = index.component_variable.replace(/'/g, '"');
                          // Parse the JSON string into an object
                          const componentObject = JSON.parse(jsonString);
                          if (componentObject.name.startsWith("$")) {
                              // Replace the value with the desired value, e.g., 'Maaza'
                              componentObject.name = parent_props.value;
                            }
                          // Create a query string from the componentObject
                          const queryParams = new URLSearchParams(componentObject).toString();
                          console.log('query_params = ', queryParams)
                          url = `${index.address}?${queryParams}`;
                        }else{
                          url = index.address
                        }
                    
                      // const url = queryParams ? `${index.address}?${queryParams}` : index.address;
                      console.log('url = ', url)
                      return(
                        <div onMouseEnter={onMouseEnterPage} onMouseLeave={()=>onMouseLeavePage(index.page_name, index.page_type, index.section_type)}>
                        <DjangoEmbed address={url}/>
                        </div>
                      )
                    }else if(index.page_type === 'pdf'){
                      return(
                        <iframe src={index['url']} width="100%" height="800px" frameBorder="0" scrolling="yes" allowFullScreen='yes' title="Analysis ppt merged.pdf"></iframe>
                      )
                      
                    } else{
                      if(index.component_variable){
                    
                      let jsonObj = JSON.parse(index.component_variable.replace(/'/g, '"'))
                      console.log('filterobj=', jsonObj)
                      for (let key in jsonObj) {
                        if (jsonObj.hasOwnProperty(key)) {
                            let value = jsonObj[key]
                            if (value.startsWith("$")) {
                              value = parent_props.value;
                            }
                          if(key==='player'){
                            const basicFilter1 = {
                              $schema: "http://powerbi.com/product/schema#basic",
                              target: {
                                table: "content_data main_data",
                              "column": "Players"},
                    
                              operator: "In",
                              values:[value],
                              // values: [window.localStorage.getItem('report')],
                              filterType: models.FilterType.BasicFilter
                            };
                            if(index.free_page===true){
                              filter_arr = []
                            }
                            console.log(index.free_page, filter_arr)
                            filter_arr.push(basicFilter1)
                          }else if(key==='categories'){
                            const categoryfilter1 = {$schema:"http://powerbi.com/product/schema#basic",
                          target:{table:"ss_content_data parameter",column:"Group_Categories"},
                          operator:"In",values:[value]};
                            filter_arr.push(categoryfilter1)
                          } else if(key==='industry'){
                            const industryfilter1 = {
                              $schema: "http://powerbi.com/product/schema#basic",
                              target: {
                                  table: "content_data industry",
                                  column: "industry_name"
                              },
                              operator: "In",
                              values: [value]
                            }
                            filter_arr.push(industryfilter1)
                          } else if (key==='sector'){
                            const sectorfilter1 = {
                              $schema: "http://powerbi.com/product/schema#basic",
                              target: {
                                  table: "Grocery_Sector_Table",
                                  column: "Sector"
                              },
                              operator: "In",
                              values: [value]
                            }
                            filter_arr.push(sectorfilter1)
                          } else if (key==='medical'){
                              const medicalCategoryFilter1= {
                              $schema:"http://powerbi.com/product/schema#basic",
                              target:{table:"ss_content_data parameter",column:"Group_Categories"},
                              operator:"In",values:[value]
                            }
                            filter_arr.push(medicalCategoryFilter1)
                          }
                        }
                      }
                      }
                    
                      return(
                        <div key={index.id}>
                          {true && (<><PowerBIEmbed
                           embedConfig = {{
                            type: 'report',   // Supported types: report, dashboard, tile, visual and qna
                            id: index['powerbi_report_id'],
                            //get from props
                            embedUrl:index['url'],
                            accessToken: index['embed'],
                            tokenType: models.TokenType.Embed,
                            filters: filter_arr,
                            settings: {
                              // background:models.BackgroundType.Transparent,
                              layoutType:models.LayoutType.Custom,
                              customLayout:{
                                displayOption:models.DisplayOption.FitToPage
                              },
                              panes: {
                                filters: {
                                  expanded: false,
                                  visible: false,
                                },
                              },
                              navContentPaneEnabled:false
                            }
                          }}
                          eventHandlers = {
                            new Map([
                              ['loaded', async function (event, report) {
                                //console.log('Report loaded');
                                if(true){
                                  const filter = {
                                    $schema: "http://powerbi.com/product/schema#advanced",
                                    target: {
                                        table: "content_data player",
                                        column: "player_name"
                                    },
                                    filterType: models.FilterType.Advanced,
                                    logicalOperator: "Is",
                                    conditions: [
                                        {
                                            operator: "Is",
                                            value: window.localStorage.getItem("player_name")
                                        }
                                    ]
                                };
                                // filter if there is a player in dropdown menu instead of page
                                const filter_player_dropdown = {
                                  $schema: "http://powerbi.com/product/schema#advanced",
                                    target: {
                                        table: "content_data main_data",
                                        column: "Players"
                                    },
                                    filterType: models.FilterType.Advanced,
                                    logicalOperator: "Is",
                                    conditions: [
                                        {
                                            operator: "Is",
                                            value: window.localStorage.getItem('drop_dn_player_name')
                                        }
                                    ]
                                }
                                const money_converter = {
                                  $schema: "http://powerbi.com/product/schema#advanced",
                                  target: {
                                      table: "Currency Table",
                                      column: "Currency"
                                  },
                                  filterType: models.FilterType.Advanced,
                                  logicalOperator: "Is",
                                  conditions: [
                                      {
                                          operator: "Is",
                                          value: window.localStorage.getItem('currency')
                                      }
                                  ]
                                }
                                const year_converter = {
                                  $schema: "http://powerbi.com/product/schema#advanced",
                                  target: {
                                      table: "Date Parameter",
                                      column: "Year_Type"
                                  },
                                  filterType: models.FilterType.Advanced,
                                  logicalOperator: "Is",
                                  conditions: [
                                      {
                                          operator: "Is",
                                          value: window.localStorage.getItem('year')
                                      }
                                  ]
                                }
                                const currency_valuation = {
                                  $schema: "http://powerbi.com/product/schema#advanced",
                                  target: {
                                      table: "Currency input",
                                      column: "Currency input"
                                  },
                                  filterType: models.FilterType.Advanced,
                                  logicalOperator: "Is",
                                  conditions: [
                                      {
                                          operator: "Is",
                                          value: 71
                                      }
                                  ]
                                }
                                const usd_selector  = {
                                  $schema: "http://powerbi.com/product/schema#advanced",
                                  target: {
                                      table: "Currency_USD_Type",
                                      column: "Type"
                                  },
                                  filterType: models.FilterType.Advanced,
                                  logicalOperator: "Is",
                                  conditions: [
                                      {
                                          operator: "Is",
                                          value: 'Custom'
                                      }
                                  ]
                                }
                    
                                report.getActivePage().then(
                                  (activePage=>{
                                    let active_ht = activePage.defaultSize.height
                                    let active_width = activePage.defaultSize.width
                                    let width = document.getElementsByClassName('report-style-class-newreport'+i+1)[0].offsetWidth;
                                    let ht = ((active_ht/active_width)*width)
                                    const removeLink = () => {
                                      const links = document.querySelectorAll('link[href^="https://content.powerapps.com/resource/powerappsportal/dist/preform"]');
                                      links.forEach((link) => {
                                        link.remove();
                                      });
                                    };
                                    removeLink()
                                  //   if(i==0){
                                  //     if(window.localStorage.getItem('finalized')==='false'){
                                  //       document.getElementsByClassName('report-style-class-newreport'+i+1)[0].style.marginTop = '-12vh';
                                  //     }
                                  //   }
                                    document.getElementsByClassName('report-style-class-newreport'+i+1)[0].style.height = ht+'px';
                                    document.getElementsByClassName('report-style-class-newreport'+i+1)[0].style.backgroundColor = '#F9FDFF'
                                    document.getElementsByClassName('report-style-class-newreport'+i+1)[0].children[0].style.border = '0px';
                                    document.getElementsByClassName('report-style-class-newreport'+i+1)[0].children[0].style.backgroundColor = '#F9FDFF';
                                    // document.getElementsByClassName('report-style-class-newreport'+i+1)[0].querySelector('iframe').style.marginLeft = '-10px';
                                    activePage.getVisuals().then(
                                      (visuals=>{
                                        // //console.log('visuals=',visuals)
                                        let slicers = visuals.filter(function (visual) {
                                          return visual.type === "slicer";
                                      }
                                      );
                                        slicers.forEach(async (slicer) => {
                                          const state = await slicer.getSlicerState();
                                          //console.log("Slicer name: \"" + slicer.name + "\"\nSlicer state:\n", state);
                                          if(state.targets[0].column==="player_name"){
                                            //console.log('slicer_name=',slicer)
                                            let target_slicer = visuals.filter(function (visual) {
                                              return visual.type === "slicer" && visual.name === slicer.name;
                                          })[0];
                                            await target_slicer.setSlicerState({ filters: [filter] });
                                          }
                                          //not using state as it will change on page load.page laod code for 1st
                                          if(state.targets[0].column==='Players' && window.localStorage.getItem('filter_on_company')==='true'){
                                            let target_slicer = visuals.filter(function (visual) {
                                              return visual.type === "slicer" && visual.name === slicer.name;
                                          })[0];
                                            await target_slicer.setSlicerState({ filters: [filter_player_dropdown] });
                                          }
                                          if(state.targets[0].column==='Currency'){
                                            let target_slicer = visuals.filter(function (visual) {
                                              return visual.type === "slicer" && visual.name === slicer.name;
                                          })[0];
                                            await target_slicer.setSlicerState({ filters: [money_converter] });
                                          }
                                          if(state.targets[0].column==='Year_Type'){
                                            let target_slicer = visuals.filter(function (visual) {
                                              return visual.type === "slicer" && visual.name === slicer.name;
                                          })[0];
                                            await target_slicer.setSlicerState({ filters: [year_converter] });
                                          }
                                    })
                                        // //console.log('slicer=', slicers)
                                      })
                                    )
                                  })
                                )
                                }
                              }],
                              ['rendered', function () {
                              }],
                              ['buttonClicked', function(event, report){
                                let ques_name = event.detail['title']
                                let company_name = ''
                                window.report.getActivePage().then(
                                  (activePage=>{
                                    activePage.getVisuals().then(
                                      (visuals=>{
                                        let slicers = visuals.filter(function (visual) {
                                          return visual.type === "slicer";
                                      });
                                        slicers.forEach(async (slicer) => {
                                        const state = await slicer.getSlicerState();
                                        // //console.log("Slicer name: \"" + slicer.name + "\"\nSlicer state:\n", state);
                                        if(state.targets[0].column==="player_name"){
                                          company_name=state.filters[0].values[0]
                                          // company_name=window.sessionStorage.getItem("player_name")
                                          // openModal(company_name, ques_name)
                                        }
                    
                                    })
                                        // //console.log('slicer=', slicers)
                                      })
                                    )
                                  })
                                )
                              }],
                              ['error', function (event) {console.log('powerbi_error=',event.detail);}]
                            ])
                          }
                    
                          cssClassName = { "report-style-class-newreport"+i+1}
                          getEmbeddedComponent = {async(embeddedReport) => {
                            if(window.reports === undefined) {
                              window.reports=[]
                            }
                            window.reports.push(embeddedReport);
                            // console.log(window.reports)
                          }
                    
                        }
                         /></>)}
                        </div>
                      )
                    }
                                    })}
                  </div>: <ArticleDetail selectedArticle={selectedArticle}/>}
                  </PowerBiDiv>:<>
                 {window.localStorage.getItem('report')===null?treemenucollapse?<Frontpage/>:<Internet/>:window.localStorage.getItem('report')==='Consumer Internet'?treemenucollapse?<Frontpage/>:<Frontpage/>:<Sub rep_id = "{parent_props.initialOpenNodes[0]}" players = {dropDownData} name = {parent_props.value} onSubmitResultToCT={onSubmitResultToCT}/>}
                  </>}
          </PowerBiDiv>:<div>
            <TailSpin
              height="80"
              width="80"
              color="#2323C8"
              ariaLabel="tail-spin-loading"
              radius="1"
              wrapperStyle={{ position: "fixed", top: "50%", left: "50%", transform: "translate(-50%, -50%)"}}
              wrapperClass=""
              visible={true}/>
              <div style = {{ position: "fixed", top: "60%", left: "50%", transform: "translate(-50%, -50%)"}}>This may take a while</div>
          </div>}
        </BodyContainer>

    </div>
          )
}

export default PowerBiFrame3

const BodyContainer = styled.div`
display:flex;
min-height:90vh;
background-color:#F9FDFF;
overflow-x:hidden;

`

const SideMenuContainer = styled.div`
  overflow-y:hidden;
  /* overflow-x:hidden; */
  /* width:20vw; */
  width:${props => props.width};
  background-color:#18183E;
  color:white;
  z-index:10;
`
const PowerBiDiv = styled.div`
width:${props => props.width};
background-color:#EEF9FF;
display:flex;
flex-direction:column;
`

const Header1 = styled.div`
background-color:white;
padding:3.5vw;
padding-top:2vw;
padding-bottom:5px;
display:flex;
justify-content:space-between;
`

const Header = styled.div`
padding:3.5vw;
padding-top:2vw;
padding-bottom:5px;
display:flex;
justify-content:space-between;
background-color:#EEF9FF;
`

const Currency = styled.div`
  margin-left:${props => props.marginLeft};
  margin-bottom:10px;
  display: grid; 
  grid-auto-rows: 1fr; 
  grid-template-columns: ${props => props.columns}; 
  /* grid-template-columns: 0.5fr 0.15fr 1fr 0fr 0.9fr 1fr 2.95fr 1.2fr 0.3fr; */
  grid-template-rows: 1fr; 
  gap: 0px 0px; 
  grid-template-areas: 
  "Descurr . Inr . Gear Cyfy  ExcelDiv . Dropdn .";
`

const Descurr = styled.div`
  grid-area:Descurr;
  line-height:34px;
  display:flex;
  align-items:center;
`
const Inr = styled.div`
  grid-area:Inr;
  display:flex;
  align-items:center;
`
const Gear = styled.div`
  grid-area:Gear;
  line-height:34px;
  display:flex;
  align-items:center;
  /* background-color:Blue; */
`
const Cyfy = styled.div`
  grid-area:Cyfy;
  display:flex;
  align-items:center;
  /* background-color:Green; */
`

const ChangeLogbutton = styled.button`
border:none;
color:white;
background-color:#0099FE;
border-radius:50px;
padding:10px;
`

const ExcelDiv = styled.div`
  grid-area:ExcelDiv;
  display:flex;
  align-items:center;
  justify-content:space-between;

`
const Dropdn = styled.div`
  grid-area:Dropdn;
`

const Currencybutton = styled.button`
background-color: ${props => props.bgcolor};
color:${props => props.color};
border: 1px solid black;
width:58px;
height:34px;
font-size:14px;
outline: none !important;
`

const customStyles = {
    content: {
      top: '50%',
      left: '55%',
      right: '45%',
      bottom: 'auto',
      height: 230,
      width:400,
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      overflow:'scroll',
    },
  };

  const ToggleButton = styled.button`
display:${props => props.display};
height:35px; 
width:35px; 
display:flex;
align-items:center;
justify-content:center;
background-color:#18183E;
border-radius:50%;
color:white;
border:0px solid black;
outline: none !important;
`
const BreadCrumbSpan = styled.span`
  &:hover{
    /* color:#20a6ff; */
    color: ${props => (props.index > 1 ? '#20a6ff' : 'grey')};
    cursor:pointer;
    /* cursor:${props => (props.index > 1 ? 'pointer' : 'default')}; */
}
`

const BreadCrumbQuickSpan = styled.span`
color:#20a6ff;
  &:hover{
    /* color: ${props => (props.index > 1 ? '#20a6ff' : 'grey')}; */
    cursor:pointer;
    text-decoration:underline;
    /* cursor:${props => (props.index > 1 ? 'pointer' : 'default')}; */
}
`
const ExcelButton = styled.button`
background-color:white;
border: 1px solid #B9BEC1;
border-radius:5px;
padding:10px;
font-size:16px;
outline: none !important;

`

const LHSTitlediv = styled.div`
font-weight:500;
font-size:30px;
`

const RHSTitlediv = styled.div`
display:flex;
gap:10px;
`

const Geardiv = styled.div`
border-radius:5px;
border:1px solid #b9bec1;
background-color:white;
padding: 10px;
cursor: pointer;
`

const Loaddiv = styled.div`
display: flex;
  align-items: center;
  position: relative;
  height: 2px;
  background-color: #D9D9D9;
  margin: 3.5vw;
`
const Loaddiv2 = styled(Loaddiv)`
  margin: 0 3.5vw 7vw;
`
const Textdiv = styled.span`
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  background-color: #EEF9FF;
  padding: 0 10px;
  cursor:pointer;
  color:#5CBEFF;

`;

const Analysisdiv = styled.div`
padding:3.5vw;
padding-top:50px;
padding-bottom:10px;
font-weight:500;
font-size:25px;
background-color:#EEF9FF;
`

const AnalysisBoxWrapper = styled.div`
  padding:3.5vw;
  padding-top:5px;
  position:relative;
  align-items: center;
  background-color: #EEF9FF;
`

const AnalysisBox = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px;
  align-items: center;
  background-color: #FFFFFF;
  border-radius: 4px;
`

const AnalysisBoxBody = styled.div`
  align-items: left;
  background-color: #FFFFFF;
  width: 100%;
`
const AnalysisBoxLink = styled.div`
  display:flex;
  justify-content:center;
  align-items: center;
  background-color: #0099FF;
  color: #FFFFFF;
  margin-top: 10px;
  cursor: pointer;
  border-radius: 50px;
  width: 200px;
  height:50px;

`

const StyledImage = styled.img`
  width: 100%;
  height: 100%;
  /* object-fit: cover; */
`;

const StyledText = styled.span`
  position: absolute;
  top: 50%;
  left: 4.5vw;
  transform: translateY(-50%);
  color:white;
  font-size:20px;
`;

const StyledButton = styled.button`
 position: absolute;
  top: 50%;
  right: 5.5vw;
  transform: translateY(-50%);
  background-color: #007bff;
  color: white;
  border: none;
  padding: 10px 15px;
  cursor: pointer;
  border-radius: 4px;
`;

const Monthdiv = styled.div`
font-weight:500;
font-size:25px;
`
const ComponentWrapper = styled.div`
  padding: calc(3.5vw - 10px);
  padding-top:10px;
`;