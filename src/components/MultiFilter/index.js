import styled from "styled-components";
import { useState, useEffect, useRef } from "react";
import ArrowIcon from "../svg/ArrowIcon.js";
import CheckBox from "../CheckBox";

const MultiFilter = ({
  id,
  labelName,
  variant = "normal",
  type,
  options = [
    {
      name: "Hotstar",
      id: 13,
    },
    {
      name: "Zee5",
      id: 14,
    },
  ],
  keyFieldName,
  valueFieldName,
  selectedOptions = [],
  onSelectedChange = () => {},
  mutileSelect = true,
  placeholder = "Select...",
  searchPlaceholder = "Search...",
  required,
  onBlur,
  disabled = false,
  showCheckBox = true,
  minWidth = "200px",
  width = "100%",
}) => {
  const [searchValue, setSearchValue] = useState("");
  const [isOptionsOpen, setIsOptionsOpen] = useState(false);
  const [visibleOptions, setVisibleOptions] = useState([]);
  const wrapperRef = useRef(null);
  const searchRef = useRef(null);

  useEffect(() => {
    setVisibleOptions(options);
  }, [options]);

  const toggleDropdown = () => {
    setIsOptionsOpen(!isOptionsOpen);
  };

  useEffect(() => {
    if (!isOptionsOpen) {
      setSearchValue("");
      setVisibleOptions(options);
    }
  }, [isOptionsOpen]);

  const onInputChange = (e) => {
    setSearchValue(e.target.value);
    const filteredOption = options.filter((obj) => {
      const substring = e.target.value.toLowerCase();
      const string = obj[valueFieldName].toLowerCase();
      return string.includes(substring);
    });
    setVisibleOptions(filteredOption);
    if (e.key === "Delete" || e.key === "Backspace") {
      console.log("backsapce cliked");
    }
  };

  const onOptionSelect = (option) => {
    const isSelectedClicked = Boolean(
      selectedOptions.filter(
        (obj) => obj[keyFieldName] === option[keyFieldName]
      ).length
    );
    if (isSelectedClicked) {
      const remainingSelected = selectedOptions.filter(
        (obj) => obj[keyFieldName] !== option[keyFieldName]
      );
      onSelectedChange(remainingSelected);
    } else {
      let result = selectedOptions.filter(
        (obj) => obj[keyFieldName] !== option[keyFieldName]
      );
      result.push(option);
      onSelectedChange(result);
    }
  };

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (
        isOptionsOpen &&
        wrapperRef.current &&
        !wrapperRef.current.contains(event.target)
      ) {
        setIsOptionsOpen(false);
      }
    };
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [isOptionsOpen]);

  switch (variant) {
    case "normal": {
      return (
        <Wrapper width={width} minWidth={minWidth}>
          {/* <LabelRow>
            <InputLabel htmlFor={labelName}>{labelName}</InputLabel>
            {is_compulsory && <span className="redtext">*</span>}
          </LabelRow> */}
          <SelectedContainer
            id={id}
            type={type}
            onBlur={(e) => !disabled && onBlur && onBlur(e)}
            disabled={disabled}
            optionsopen={isOptionsOpen.toString()}
            ref={isOptionsOpen ? wrapperRef : null}
          >
            <SelectedOption onClick={() => toggleDropdown()}>
              {selectedOptions.length ? (
                `Selected [${selectedOptions.length}]`
              ) : (
                <Placeholder>{placeholder}</Placeholder>
              )}
            </SelectedOption>
            <ArrowContainer
              disabled={disabled}
              ref={isOptionsOpen ? wrapperRef : null}
            >
              <ArrowIcon
                rotate={isOptionsOpen ? 270 : 90}
                disabled={disabled}
                onIconClick={() => !disabled && toggleDropdown()}
              />
              {isOptionsOpen && (
                <Dropdown>
                  <SearchBox
                    required={required}
                    value={searchValue}
                    onChange={onInputChange}
                    id={id}
                    placeholder={searchPlaceholder}
                    type={type}
                    onBlur={(e) => !disabled && onBlur && onBlur(e)}
                    disabled={disabled}
                    ref={searchRef}
                  ></SearchBox>
                  <DropdownScroll>
                    {visibleOptions.map((option, index) => {
                      const isselected = selectedOptions.filter(
                        (obj) => obj[keyFieldName] === option[keyFieldName]
                      ).length;
                      return (
                        <DropdownOption
                          key={index}
                          isselected={showCheckBox ? false : isselected}
                          onClick={() => {
                            onOptionSelect(option);
                          }}
                        >
                          {showCheckBox && (
                            <CheckBoxWrapper>
                              {" "}
                              <CheckBox value={isselected} />
                            </CheckBoxWrapper>
                          )}
                          {option[valueFieldName]}
                        </DropdownOption>
                      );
                    })}
                  </DropdownScroll>
                </Dropdown>
              )}
            </ArrowContainer>
          </SelectedContainer>
        </Wrapper>
      );
    }
    default: {
      return <p>Invalid Variant Type</p>;
    }
  }
};

export default MultiFilter;

const Wrapper = styled.div`
  display: flex;
  align-items: flex-start;
  min-width: ${(props) => props.minWidth};
  width: ${(props) => props.width};
  height: 40px;
`;

const InputLabel = styled.div`
  padding: 5px;
  font-size: 0.8rem;
  color: rgb(114, 114, 114);
`;

const LabelRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  .redtext {
    color: #f44336;
    font-size: 16px;
  }
`;

const SelectedOption = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 100%;
  padding: 5px 5px 5px 10px;
`;

const SelectedContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: 40px;
  justify-content: space-between;
  align-items: center;
  border: 1px solid #cccccc;
  outline: none;
  border-radius: ${(props) =>
    props.optionsopen === "true" ? "5px 5px 0px 0px" : "5px"};
  border-radius: 5px;
  width: 100%;
  transition: 0.2s ease-out;
  transition-property: border;
  background-color: ${(props) => (props.disabled ? "#efefef4d" : "#FFFFFF")};
  position: relative;
`;

const SearchBox = styled.input`
  padding: 8px;
  width: 100%;
  height: 38px;
  transition: 0.2s ease-out;
  transition-property: border;
  border: none;
  font-size: 16px;
  outline: none;
  background-color: transparent;
  :focus {
    border: 1px solid "#CCCCCC";
    outline: none;
  }
  :hover {
    border: 1px solid "#CCCCCC";
    outline: none;
  }
  :disabled {
  }
`;

const ArrowContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  width: 34px;
  height: 30px;
  cursor: ${(props) => (props.disabled ? "default" : "pointer")};
`;

const Dropdown = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 300px;
  background-color: #ffffff;
  border: 1px solid #cccccc;
  border-radius: 5px;
  box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.2);
  position: absolute;
  top: 42px;
  width: calc(100% + 2px);
  left: -1px;
  z-index: 5;
  cursor: default;
`;

const DropdownScroll = styled.div`
  display: flex;
  flex-direction: column;
  max-height: 100%;
  overflow: auto;
`;
//

const DropdownOptionType = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 38px;
  padding: 8px;
  background-color: ${(props) => (props.isselected ? "#0099FF" : null)};
  color: ${(props) => (props.isselected ? "#FFFFFF" : "#000000")};
  &:hover {
    background-color: ${(props) =>
      props.isselected ? "#0099FF" : "#e5e5e560"};
    cursor: pointer;
  }
`;

const DropdownOption = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 38px;
  padding: 6px 7px;
  padding-left: ${(props) => props.paddingLeft}px;
  background-color: ${(props) => (props.isselected ? "#0099FF" : null)};
  color: ${(props) => (props.isselected ? "#FFFFFF" : "#000000")};
  &:hover {
    background-color: ${(props) =>
      props.isselected ? "#0099FF" : "#e5e5e560"};
    cursor: pointer;
  }
`;

const Placeholder = styled.div`
  color: #969799;
  font-size: 13px;
  font-style: normal;
`;

const CheckBoxWrapper = styled.div`
  margin-right: 5px;
`;
