import React from 'react'
import styles from './Header3.module.css'
import MyDropdown from '../DropDown/dropdown'
import { useState } from 'react'
import { RiArrowDropDownLine } from "react-icons/ri";
import TreeDropDn from '../TreeDropDn/TreeDropDn';
import styled from 'styled-components';
import { Link, useLocation, useRouteMatch, useNavigate } from 'react-router-dom';
import UserCard from '../UserCard/UserCard';



const Header3 = () => {
  const location = useLocation();
  const [selectedOption, setSelectedOption ] = useState(null);
  const [showDropDn, setShowDropDn] = useState(false)
  const [usermodalIsOpen, setuserModalIsOpen] = useState(false);
  const [showUserProfile, setShowUserProfile] = useState(false);
  const [activeLink, setActiveLink] = useState(location.pathname);

  const navigate = useNavigate();
  const onOptionSelect = (option)=>{
    console.log('hello')
    setSelectedOption(option.label);
  }
  let today = new Date();
  let hour = today.getHours();

  
  let platform_option = [
  { id: 2, label: 'Industries' },
  { id: 3, label: 'Companies'},
  { id: 4, label: 'Brand' },
  { id: 5, label: 'Resources'}

]

  const handleSignOut = ()=>{
    console.log('signout')
    window.localStorage.setItem('loginStatus','false')
    window.localStorage.clear()
    navigate('/')
  }
  const investor_id = window.localStorage.getItem("investor_id")

  console.log(activeLink)

  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
      <header className={styles.header}> {/* Use styles.header to reference the CSS class */}
        <div style = {{cursor:'pointer'}}className={styles.logo}>
          <img src = '/Images/BenchmarksNew.svg' alt='benchmarks' onClick={()=>{navigate('/')}}/>
          <div className={styles.dropdnwrap}>
            <Dropbutton onClick = {()=>{setShowDropDn(!showDropDn)}}><RiArrowDropDownLine/></Dropbutton>
          </div>
        </div>
        {/* set for mobile version */}
      
        <SignOut display = {showDropDn?'flex':'none'}>
                            <div style = {{fontFamily:'Fira Sans'}}onClick = {()=>{setuserModalIsOpen(true)}}>Home</div>
                            <div style = {{fontFamily:'Fira Sans'}}onClick={handleSignOut}>Sectors</div>
                            <div style = {{fontFamily:'Fira Sans'}}onClick={handleSignOut}>Companies</div>
                            <div style = {{fontFamily:'Fira Sans'}}onClick={handleSignOut}>Brands</div>
                            <div style = {{fontFamily:'Fira Sans'}}onClick={handleSignOut}>Resources</div>
        </SignOut>
        <nav className={styles['nav-links']}>
      <Link
          to="/"
          onClick={() => setActiveLink('/')}
          style={{ color: activeLink === '/' ? '#007bff' : 'black' }}
          className = 'HeaderHome'
      >
          Home
      </Link>
      {window.localStorage.getItem('loginStatus') === 'true' ?
          <>
              <HomeDiv onClick={() => setActiveLink('Sectors')}
                       style={{ color: activeLink === 'Sectors' ? '#007bff' : 'black', cursor:'pointer' }}
                       className = 'HeaderSector'
              >
                  Sectors
                  <TreeDropDnDiv><TreeDropDn/></TreeDropDnDiv>
              </HomeDiv>
          </>
          :
          <Link
              to="/industries"
              onClick={() => setActiveLink('/industries')}
              style={{ color: activeLink === '/industries' ? '#007bff' : 'black' }}
              className = 'HeaderIndustry'
          >
              Sectors
          </Link>
      }
      <Link
          to={investor_id? "/company-listing": "/companies" }
          onClick={() => setActiveLink('/companies')}
          style={{ color: activeLink === '/companies' || activeLink === '/company-listing' ? '#007bff' : 'black' }}
          className = 'HeaderCompany'
      >
          Companies
      </Link>
      <Link
          to="/brands"
          onClick={() => setActiveLink('/brands')}
          style={{ color: activeLink === '/brands' ? '#007bff' : 'black' }}
          className = 'HeaderBrands'
      >
          Brands
      </Link>
      </nav>
        {window.localStorage.getItem('loginStatus')==='true'?
        <div style = {{cursor:'pointer'}}>
          {/* <span onClick={()=>{handleSignOut()}}>Sign out <RiArrowDropDownLine/></span>  */}
          <span>{hour<15?'Good Morning ':'Good Evening '}{window.localStorage.getItem('user_name')!==undefined?window.localStorage.getItem('user_name'):window.localStorage.getItem('email').split('@')[0]} <RiArrowDropDownLine onClick = {()=>{setShowUserProfile(!showUserProfile)}}/></span>
        </div>:
        <div style = {{cursor:'pointer'}}onClick={()=>(window.location.href = '/signin3')}>
          Sign in
        </div>}
      </header>
      <div style={{ position:'absolute',display: showUserProfile ? 'block' : 'none'}}><UserCard/></div>
      {/* <div style={{ visibility: showUserProfile ? 'visible' : 'hidden', opacity: showUserProfile ? 1 : 0 }}><UserCard/></div> */}
    </div>
  )
}

export default Header3

const Dropbutton = styled.button`
border-radius:50%;
border:0px;
background-color:white;
outline:none !important;
&:hover{
    background-color:#EFEFEF;
}
`

const SignOut = styled.div`
@media (min-width:768px){
    display:none;
}
display: ${props => props.display};
position:relative;
flex-direction:column;
top:80px;
left:-40px;
align-items:center;
justify-content:center;
background:#F6F6F6;
border-radius: 5px;
box-shadow: 0 0 5px 2px rgba(0, 0, 0, 0.5);
width:120px;
font-size:16px;
/* transition-duration:160ms; */
text-align:center;
div{
    width:100%;
    cursor:pointer;
    border-radius: 5px;
&:hover{
  background-color: #ddd;
}
}
`
const TreeDropDnDiv = styled.div`
  display:none;
  position:absolute;
  top:6.2vh;
  right:1px;
  left:1px;
  color:black;
  background-color:#FFFFFF;
  line-height:20px;
  border:1px solid #c9c9c9;
  border-radius: 0 0 5px 5px;
  width:100vw;
  min-height: 32.5vh;
  font-size:16px;
  /* transition-duration:160ms; */
  /* text-align:center; */

`
const Backdrop = styled.div`
  display: none;  /* Initially hidden */
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(255, 255, 255, 0.5);
  backdrop-filter: blur(5px);
  z-index: 999; /* One less than the dropdown's z-index */
`;

const HomeDiv = styled.div`
margin-right:50px;
width:5vw;
display:flex;
justify-content:center;
  &:hover{
    ${TreeDropDnDiv}{
      display:block;
      z-index: 1000;
    }
  
    ${Backdrop} {
      display: block;
    }
  }
`


