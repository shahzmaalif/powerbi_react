import React from 'react'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Cell, ResponsiveContainer, LabelList } from 'recharts';
import { useEffect, useState } from "react";
import axios from "axios";
import styled from 'styled-components';


const data = [
    {
      name: "Total",
      value:100, // value is the part of the graph we want to show
      pv: 0 // pv is the floating part (transparent)
    },
    {
      name: "Seller",
      value: 60,
      pv: 40 // to get this pv, we use 01/2020's value + pv
    },
    {
      name: "Logistic Cost",
      value: 30,
      pv: 10 // use 02/2020's value + pv, and so forth
    },
    {
      name: "Platform Commision",
      value: 10,
      pv: 0
    },
  ];
  

const WaterFallWalletShare = ({ width, height }) => {
    const [revdata, setrevdata] = useState(null)
    const [graphkeys,setgraphkeys] = useState([])
    const [paramarr, setparamarr] = useState([])
    const [year, setyear] = useState('2023')

    useEffect(()=>{
        const getrevenuedata = (async()=>{
          let industry_id = window.localStorage.getItem('industry_id')
          if(!industry_id){
            industry_id = 10
          }
          const result = await axios.get(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/revpool/?industry_id=${industry_id}`)
          console.log(result.data)
          let input  =result.data[result.data.length-1]
          let param1 = Object.keys(input)[2]
          let param2 = Object.keys(input)[3]
          let param3 = Object.keys(input)[4]
          console.log(Object.keys(input), input, param1, param2, param3)
          setparamarr([param1, param2, param3])
          let input1 = result.data[result.data.length-2]
          let pp_seller = input[param1]-input1[param1]
          let pp_lc = input[param2]-input1[param2]
          let pp_pc = input[param3]-input1[param3]
          console.log(pp_lc, pp_pc, pp_seller)
          setyear(input['name'])
          let total = 100
          const data = [
            {
              name: "Total",
              value: total,
              pv: 0,
              pp: 0
            },
            {
              name: param1,
              value: input[param1],
              pv: total - input[param1],
              pp: pp_seller
            },
            {
              name: param2,
              value: input[param2],
              pv: total - input[param1] - input[param2],
              pp: pp_lc
            },
            {
              name: param3,
              value: input[param3],
              pv: total - input[param1] - input[param2] - input[param3],
              pp: pp_pc
            },
          ];
          setrevdata(data)
        //   setgraphkeys(Object.keys(result.data[0]).slice(2))
        })
        
        getrevenuedata()
      },[])

      const CustomTooltip = ({ active, payload, label }) => {
        if (active && payload && payload.length) {
          const ppValue = payload[1]?.payload?.pp; // Assuming `pp` is in the second payload item
          const ppStyle = {
            color: ppValue < 0 ? 'red' : 'green',
          };
      
          return (
            <div className="custom-tooltip" style={{ backgroundColor: 'white', padding: '10px', border: '1px solid #ccc' }}>
              <p className="label">{`${label} : ${payload[1]?.value}`}</p>
              <p className="intro" style={ppStyle}>{`pp yoy: ${ppValue}`}</p>
            </div>
          );
        }
      
        return null;
      };

  return (
    <div>
        <GraphDimension width={width} height={height}>
        <Title>Revenue Waterfall</Title>
        <div style={{backgroundColor:'white', paddingLeft:'30px', marginTop:'15px', paddingTop:'10px', paddingBottom:'10px'}}>For C.Y. {year}</div>
        {revdata && <div style={{backgroundColor: "white", height: "90%"}}>
        <BarChart
      width={1300}
      height={400}
      data={revdata}
      margin={{
        top: 15,
        right: 0,
        left: 0,
        bottom: 0,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      {/* <Tooltip content={<CustomTooltip />} /> */}
      <Bar dataKey="pv" stackId="a" fill="transparent" barSize={200} />
      <Bar dataKey="value" stackId="a" fill="#82ca9d" barSize={200} >
      <LabelList
        dataKey="value"
        position="top"
        content={({ x, y, width, height, index , value }) => {

          const currentItem = revdata[index];
          const ppColor = currentItem.pp >= 0 ? 'green' : 'red';
          const label = `${value} (${currentItem.pp >= 0 ? '+' : ''}${currentItem.pp}pp yoy)`;
          return(
            <text x={x + width / 2} y={y-4} fill={ppColor} textAnchor="middle" dominantBaseline="bottom">
            {value!==100?label:null}
          </text>
          )
        }}
      />

        {revdata.map((item, index) => {
          if (item.name  === paramarr[0]) {
            return <Cell key={index} fill="#ABC6FC" />;
          }
          if (item.name === "Total") {
            return <Cell key={index} fill="#4CBF6E" />;
          }
          if (item.name === paramarr[1]) {
            return <Cell key={index} fill="#74A1FB" />;
          }else{
            return <Cell key={index} fill="#4781F9" />;
          }
        })}
      </Bar>
        </BarChart>
        </div>
        }
    </GraphDimension>
    <div style={{textAlign:'center', padding:'15px', backgroundColor: "white", width: "92.3vw", marginLeft:'3.5vw'}}>*Assuming overall gmv for the sector is 100</div>
    </div>
  )
}

const GraphDimension = styled.div`
  width: calc(${(props) => props.width} - 60px);
  height: calc(${(props) => props.height}+50px);
  padding-left: 3.5vw;
`;
const Title = styled.div`
  font-size: 25px;
  font-weight: 500;
  height: 10%;
  letter-spacing: 0em;
  text-align: left;
  background-color:#EEF9FF;
`;
export default WaterFallWalletShare