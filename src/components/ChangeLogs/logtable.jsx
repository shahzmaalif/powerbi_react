import React from 'react'
import { useEffect, useState, useRef, useMemo } from 'react'
import { Navigate, useLocation } from 'react-router-dom';
import axios from 'axios'
import styled from 'styled-components'
import Select from 'react-select'
import Header3 from '../Header3/Header3';
import { useNavigate } from "react-router-dom";



const Logtable = (props) => {
    const useQuery = () => new URLSearchParams(useLocation().search);
    const url_industry_id = useQuery().get("industry_id");
    const url_industry_name = useQuery().get("industry_name");
    const url_pass_key = useQuery().get("pass_key");

    const [tabledata, settabledata] = useState([])
    const tabeledataref = useRef(null)
    const [players, setplayers] = useState([])
    const [breadcrumbArr, setbreadcrumbArr] = useState([])

    useEffect(() => {
        const getabledata = async()=>{
            let industry_id = url_industry_id
            if (!industry_id){
                industry_id = 72
            }
            let response = await axios.get(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/tracking/?industry_id=${industry_id}`)

            const formatDate = (dateString) => {
                const options = { year: 'numeric', month: 'long', day: 'numeric' };
                return new Date(dateString).toLocaleDateString('en-US', options);
              };

            response.data = response.data.map(object=>({
                ...object,
                'display': `${object.operation} ${object.parameter_name} from ${object.old_value} to ${object.new_value}`,
                'created_date': formatDate(object.created_date)
            }))
            tabeledataref.current = response.data
            settabledata(response.data)
            const uniqueNames = new Set(response.data.map(item => item.player_name));
            let arr = Array.from(uniqueNames).map(player_name => ({ value: player_name, label: player_name }));
            setplayers(arr)
        }
        getabledata()
    }, [])


    function searchinNodes(node, key, arr){
        if(node.key.toString() === key.toString()){
            arr.push(node)
            return [true,arr]
        }
        if (node.nodes.length===0){
            return [false, arr]
        }
        arr.push(node)
        for(let i=0;i<node.nodes.length;i++){
            let res = searchinNodes(node.nodes[i], key, arr)
            if(res[0]){
                return res
            }else{
                arr = []
            }
        }
        return [false, arr]
    }


    const getNodeNParents = (key)=>{
        let  res = []
        let treedata = window.localStorage.getItem('windowallTreeData')
        treedata = JSON.parse(treedata)
        if (!treedata){
            return []
        }
        let nodes = treedata[0].nodes
        console.log('seachnode = ', nodes)
        let notfound = true
        let i = 0
        while(notfound && i<nodes.length){
            console.log('searchin = ',nodes[i], key)
            res = searchinNodes(nodes[i], key, [])
            if(res[0]){
                notfound = false
            }
            i = i+1
        }
        return res[1]

    }


    useEffect(() => {
        let pass_key = url_pass_key
        if(!pass_key){
            pass_key = 448
        }
        // need node, key, filter, filter_val, industry_id to navigate. get ot from node or from array. why get from node ? already finding it whith useffect just save the result
        // get node and its parents by key

        let arr = getNodeNParents(pass_key)
        arr.push({ key: 3, label: "Logs" })
        arr.splice(0,0,{key:1, label:'Sectors'})
        setbreadcrumbArr(arr)

      }, [url_pass_key]);



    const handleSelectOptions  = (option) =>{
        console.log(tabeledataref.current)
        let arr = tabeledataref.current.filter((val)=>val.player_name===option.value)
        settabledata(arr)

    }

    const handleDateSelect = (option) =>{
        console.log(option)
    }
    const navigate = useNavigate();
    const HOME_URL = "/";
    const treeziparr = [
        { key: 1, name: "Home" },
        { key: 2, name: "Logs" },
      ];

      let handleClickTree = (i, key) => {
        if (i>0 && i<breadcrumbArr.length-1){
            let node = breadcrumbArr[i]
            window.location.href = `/Report3/?val=${node.label}&key=${node.key}&filter=${node.filter}&filter_value=${node.filter_value}&industry_id=${node.industry_id}`
        }
      };

  return (
    <div>
        <Header3/>
        <div style={{paddingLeft:'3.5vw', paddingRight:'3.5vw', paddingTop:'2vh', paddingBottom:'2vh'}}>
            <h3> {url_industry_name} - Data Revision History</h3>
            <>
                    {breadcrumbArr?.map((obj, i) => (
                    <BreadCrumbSpan
                        onClick={(e) => {
                        handleClickTree(i, obj.key);
                        }}
                        key={i}
                    >
                        {obj.label} /{" "}
                    </BreadCrumbSpan>
                    ))}
            </>
        </div>
        <div style={{paddingLeft:'3.5vw', paddingRight:'3.5vw', paddingTop:'2vh', paddingBottom:'2vh', backgroundColor:'#E5F5FF'}}>
            <div style={{padding:'3.5vw', backgroundColor:'white', marginTop:'10px'}}>
                <div style={{display:'flex', justifyContent:'space-between', marginBottom:'40px', gap:'100px'}}>
                    <div>
                        <h5>Welcome to Data Revision History!</h5>
                        Here you can find the information about changes in most recent changes that have been made to this report including data changes new features and other improvements.
                    </div>
                    <div style={{display:'flex', gap:'20px'}}>
                        <div>
                            <Select
                            name="columns"
                            options={players}
                            className="basic-multi-select"
                            classNamePrefix="select"
                            onChange={handleSelectOptions}
                            isSearchable = {true}
                            />
                        </div>
                    </div>
                </div>
                <table>
                    <thead>
                        <tr>
                            <Styledth>Version, Release Date</Styledth>
                            <Styledth>Change</Styledth>
                            <Styledth>Player</Styledth>
                            <Styledth>Change details</Styledth>
                        </tr>
                    </thead>
                    <Styledtbody>
                        {tabledata?tabledata.map((i)=>{
                            return(<tr>
                                <Styledtd>{i.created_date}</Styledtd>
                                <Styledtd>{i.operation}</Styledtd>
                                <Styledtd>{i.player_name}</Styledtd>
                                <Styledtd>{i.display}</Styledtd>
                            </tr>)
                        }):null}
                    </Styledtbody>
                </table>
            </div>
        </div>
    </div>
  )
}

export default Logtable

const Styledth = styled.th`
    border: 1px solid #DBDBDB;
    min-width:250px;
    padding: 15px;
    background-color:#D4EEFF;
`

const Styledtd = styled.td`
    padding:15px;
    border: 1px solid #DBDBDB;
    border-bottom: 0px;
    border-top:0px;
`

const Styledtbody = styled.tbody`
    border: 1px solid #DBDBDB;
`

const BreadCrumbSpan = styled.span`
color:grey;
  &:hover {
    color: #20a6ff;
    cursor: pointer;
  }
`;