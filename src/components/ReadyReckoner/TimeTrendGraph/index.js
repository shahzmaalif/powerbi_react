import React from "react";
import styled from "styled-components";
import {
  ComposedChart,
  LabelList,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
  Line,
} from "recharts";

const TimeTrendGraph = ({
  companyName,
  paramName,
  umbrellaName,
  lineChart = false,
  data = [],
}) => {
  return (
    <PopupWrap>
      <Title>
        {companyName}'s {paramName} in {umbrellaName}
      </Title>
      <PopupBody>
        <CompanyWrapper>
          <CompanyBox>
            <BeadCircle color="#0099FF" />{" "}
            <CompanyName>{companyName}</CompanyName>
          </CompanyBox>
          {lineChart && (
            <CompanyBox>
              <BeadCircle color="#FF3232" />
              <CompanyName>Industry Average</CompanyName>
            </CompanyBox>
          )}
        </CompanyWrapper>
        <ComposedChart
          width={500}
          height={250}
          data={data}
          barSize={30}
          margin={{ top: 25, right: 0, left: 0, bottom: 10 }}
        >
          <XAxis dataKey="label" />
          {/* <YAxis /> */}
          <Tooltip />
          <CartesianGrid stroke="#D1D1D1" horizontal={true} vertical={false} />
          <Bar dataKey="value" fill="#66B3FF" name="Company Value">
            <LabelList
              dataKey="value"
              position="left"
              dy={0}
              // dx={-30}
              fill="#003D66"
              fontSize={12}
              fontWeight="bold"
            />
          </Bar>
          {lineChart && (
            <Line
              dot={{ fill: "#FF3232", stroke: "#FFFFFF", strokeWidth: 2, r: 4 }}
              strokeWidth={2}
              strokeLinecap="round"
              type="linear"
              dataKey="industry_avg"
              stroke="#FF3232"
              legendType="rect"
              name="Industry Average"
            >
              <LabelList
                dataKey="industry_avg"
                position="right"
                dy={-20}
                fill="#DA1E28"
                fontSize={12}
                fontWeight="bold"
              />
            </Line>
          )}
        </ComposedChart>
      </PopupBody>
    </PopupWrap>
  );
};

export default TimeTrendGraph;

const PopupWrap = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #ffffff;
  z-index: 5;
  text-align: center;
  color: #262e40;
`;

const Title = styled.div`
  font-size: 16px;
  font-weight: 600;
  text-align: left;
  padding: 5px 10px 10px;C
`;
const PopupBody = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  border-radius: 10px;
  border: 1.5px solid #f0f0f0;
  padding: 5px 10px;
`;

const CompanyWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: left;
  width: 100%;
`;

const CompanyBox = styled.div`
  display: flex;
  flex-direction: row;
`;

const CompanyName = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: left;
  align-items: center;
  height: 22px;
  font-size: 12px;
  font-weight: 600;
  line-height: 16px;
  text-align: left;
  color: #9e9e9e;
  padding-left: 5px;
  padding-right: 20px;
`;

const BeadCircle = styled.div`
  background: linear-gradient(
    180deg,
    ${(props) => props.color} 100%,
    ${(props) => props.color} 100%
  );
  width: 16px;
  height: 16px;
  border-radius: 8px;
  cursor: pointer;
  position: relative;
`;
