import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import moment from "moment";
import Loader from "../Loader";
import axios from "axios";
import MasterTable from "../MasterTable";
import { READY_RECKONER_ENDPOINT } from "../../constants/constants";
import OptionSelect from "../OptionSelect";
import TimeTrendGraph from "./TimeTrendGraph";
import VectorIcon from "../svg/VectorIcon";

// use it
// <ReadyReckoner width={"100%"} height={"70vh"}/>
const BoxData = ({ value, isMore, isUp, indComparison }) => {
  return (
    <BoxWrapper>
      <BoxValue
        color={!indComparison ? "#000000" : isMore ? "#21AF4A" : "#DA1E28"}
      >
        {value}
      </BoxValue>
      <IconWrapper color={isUp ? "#D3EFDB" : "#F8D2D4"}>
        <VectorIcon
          width={9}
          height={13}
          color={isUp ? "#21AF4A" : "#DA1E28"}
          rotate={isUp ? 0 : 180}
        />
      </IconWrapper>
    </BoxWrapper>
  );
};
const BoxWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const BoxValue = styled.div`
  font-size: 16px;
  font-weight: 600;
  color: ${(props) => props.color};
`;

const IconWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 30px;
  height: 30px;
  border-radius: 24px;
  background-color: ${(props) => props.color};
`;

const ReadyReckoner = ({
  width,
  height,
  company_id,
  frequency = "quarterly", //monthly or quarterly
  title = "Ready Reckoner",
  start_date = null,
  end_date = moment().format("YYYY-MM-DD"),
}) => {
  // const [parentWidth, setParentWidth] = useState(null);
  const options = [
    { key: "city", value: "City" },
    { key: "category", value: "Category" },
  ];
  const [companyName, setCompanyName] = useState(null);
  const [seletedOption, setSelectedOption] = useState(options[0]);
  const [paramsList, setParamsList] = useState([]);
  const [cityData, setCityData] = useState([]);
  const [categoryData, setCategoryData] = useState([]);
  const [styledRowsData, setStyledRowsData] = useState([]);
  const [isLoading, setLoading] = useState(false);
  /* sorting hooks */
  const [sortedColKey, setSortedColKey] = useState(null);
  const [sortedDirection, setSortedDirection] = useState("");

  const getLabel = (dataDate) => {
    const dataDateObj = moment(dataDate, "YYYY-MM-DD");
    if (frequency === "monthly") {
      return dataDateObj.format("MMM'YY");
    } else if (frequency === "quarterly") {
      const quarter = Math.floor((dataDateObj.month() + 3) / 3);
      switch (quarter) {
        case 1:
          return `JFM'${dataDateObj.format("YY")}`;
        case 2:
          return `AMJ'${dataDateObj.format("YY")}`;
        case 3:
          return `JAS'${dataDateObj.format("YY")}`;
        case 4:
          return `OND'${dataDateObj.format("YY")}`;
        default:
          return "Invalid Date";
      }
    }
  };
  useEffect(() => {
    const getTabledata = async () => {
      const postData = {
        company_id: company_id,
        frequency: frequency,
        start_date: start_date !== "null" ? start_date : null,
        end_date: end_date !== "null" ? end_date : null,
      };

      const response = await axios.post(READY_RECKONER_ENDPOINT, postData);
      let readyReckonerData = response.data.param_data;
      let readyReckonerParams = response.data.param;

      const tempParametersDict = {};
      const tempCityDict = {};
      const tempCategoryDict = {};
      readyReckonerData.forEach((data) => {
        data = { ...data, label: getLabel(data.start_date) };
        if (!companyName) {
          setCompanyName(data.company_name);
        }
        if (!tempParametersDict[data.parameter_id]) {
          tempParametersDict[data.parameter_id] = {
            key: data.parameter_id,
            value: data.parameter_name,
            sorting: true,
            range_filter: false,
            data: [data],
          };
        } else {
          tempParametersDict[data.parameter_id] = {
            ...tempParametersDict[data.parameter_id],
            data: [data, ...tempParametersDict[data.parameter_id].data],
          };
        }
        if (data.city_id && !tempCityDict[data.city_id]) {
          tempCityDict[data.city_id] = {
            key: data.city_id,
            value: data.city_name,
          };
        }
        if (data.category_id && !tempCategoryDict[data.category_id]) {
          tempCategoryDict[data.category_id] = {
            key: data.category_id,
            value: data.category_name,
          };
        }
      });

      readyReckonerParams.forEach((param) => {
        if (tempParametersDict[param.parameter]) {
          tempParametersDict[param.parameter] = {
            ...tempParametersDict[param.parameter],
            sequence: param.sequence,
            indComparison: param.industry_comparison,
          };
        }
      });

      setParamsList(
        Object.values(tempParametersDict).sort(
          (a, b) => a.sequence - b.sequence
        )
      );
      setCityData(Object.values(tempCityDict));
      setCategoryData(Object.values(tempCategoryDict));
      setLoading(false);
    };
    setLoading(true);
    getTabledata();
  }, []);

  const accessMonthDataFilter = (dataDate) => {
    const queryMonthObj = moment(end_date, "YYYY-MM-DD");
    const dateObj = moment(dataDate, "YYYY-MM-DD");
    if (queryMonthObj.year() !== dateObj.year()) {
      return false;
    }
    if (frequency === "monthly") {
      if (queryMonthObj.month() !== dateObj.month()) {
        return false;
      } else {
        return true;
      }
    } else if (frequency === "quarterly") {
      const dataMonth = dateObj.month(); // zero-based months
      const queryMonthNo = queryMonthObj.month();
      if (queryMonthNo <= 2) {
        return dataMonth === 0;
      } else if (queryMonthNo <= 5) {
        return dataMonth === 3;
      } else if (queryMonthNo <= 8) {
        return dataMonth === 6;
      } else {
        return dataMonth === 9;
      }
    }
    return false;
  };

  useEffect(() => {
    const makeRowsData = () => {
      if (seletedOption.key === "city") {
        let rowsData = [];
        cityData.forEach((city) => {
          let rowData = {
            city: {
              value: city.value,
              sortingValue: city.value,
              toolTip: null,
            },
          };
          paramsList.forEach((paramAllData) => {
            const paramCityData = paramAllData.data
              .filter((data) => data.city_id === city.key)
              .sort((a, b) => moment(a.start_date).diff(moment(b.start_date)));
            const limitedData =
              paramCityData.length > 5
                ? paramCityData.slice(paramCityData.length - 5)
                : paramCityData;
            const graph = (
              <TimeTrendGraph
                companyName={companyName}
                paramName={paramAllData.value}
                umbrellaName={city.value}
                data={limitedData}
                lineChart={paramAllData.indComparison}
              />
            );
            const paramCityRecentData = paramCityData.filter((data) =>
              accessMonthDataFilter(data.start_date)
            );
            if (paramCityRecentData.length) {
              rowData[paramAllData.key] = {
                value: (
                  <BoxData
                    value={paramCityRecentData[0].value}
                    isMore={
                      paramCityRecentData[0].value -
                        paramCityRecentData[0].industry_avg >=
                      0
                    }
                    isUp={
                      paramCityRecentData[0].increment == null ||
                      paramCityRecentData[0].increment >= 0
                    }
                    indComparison={paramAllData.indComparison}
                  />
                ),
                sortingValue: paramCityRecentData[0].value,
                toolTip: graph,
              };
            }
          });
          if (Object.keys(rowData).length > 1) {
            rowsData.push(rowData);
          }
        });
        setStyledRowsData(
          rowsData.sort((a, b) => {
            const aVal = a[sortedColKey]?.sortingValue || 0;
            const bVal = b[sortedColKey]?.sortingValue || 0;
            if (typeof aVal === "number" && typeof bVal === "number") {
              return sortedDirection === "asc" ? aVal - bVal : bVal - aVal;
            } else if (typeof aVal === "string" && typeof bVal === "string") {
              return sortedDirection === "asc"
                ? aVal.localeCompare(bVal)
                : bVal.localeCompare(aVal);
            } else {
              return 0;
            }
          })
        );
      } else {
        let rowsData = [];
        categoryData.forEach((category) => {
          let rowData = {
            category: {
              value: category.value,
              sortingValue: category.value,
              toolTip: null,
            },
          };
          paramsList.forEach((paramAllData) => {
            const paramCategoryData = paramAllData.data
              .filter((data) => data.category_id === category.key)
              .sort((a, b) => moment(a.start_date).diff(moment(b.start_date)));
            const limitedData =
              paramCategoryData.length > 5
                ? paramCategoryData.slice(paramCategoryData.length - 5)
                : paramCategoryData;
            const graph = (
              <TimeTrendGraph
                companyName={companyName}
                paramName={paramAllData.value}
                umbrellaName={category.value}
                data={limitedData}
                lineChart={paramAllData.indComparison}
              />
            );
            const paramCategoryRecentData = paramCategoryData.filter((data) =>
              accessMonthDataFilter(data.start_date)
            );
            if (paramCategoryRecentData.length) {
              rowData[paramAllData.key] = {
                value: (
                  <BoxData
                    value={paramCategoryRecentData[0].value}
                    isMore={
                      paramCategoryRecentData[0].value -
                        paramCategoryRecentData[0].industry_avg >=
                      0
                    }
                    isUp={
                      paramCategoryRecentData[0].increment == null ||
                      paramCategoryRecentData[0].increment >= 0
                    }
                    indComparison={paramAllData.indComparison}
                  />
                ),
                sortingValue: paramCategoryRecentData[0].value,
                toolTip: graph,
              };
            }
          });
          if (Object.keys(rowData).length > 1) {
            rowsData.push(rowData);
          }
        });
        setStyledRowsData(rowsData);
      }
    };

    makeRowsData();
  }, [paramsList, seletedOption, sortedColKey, sortedDirection]);

  return (
    <Wrapper width={width} height={height}>
      <Title>{title}</Title>
      <GraphShadow width={width} height={height}>
        <GraphDimension width={width} height={height}>
          {isLoading ? (
            <LoadingMessage>Loading...</LoadingMessage>
          ) : (
            <>
              <HeaderWrapper>
                <TableMenuLeft>
                  For the {frequency === "quarterly" ? "Quarter" : "Month"} of{" "}
                  {getLabel(end_date)}
                </TableMenuLeft>
                <OptionSelect
                  options={options}
                  selectedOption={seletedOption}
                  onOptionSelect={setSelectedOption}
                />
              </HeaderWrapper>

              <MasterTable
                columnsData={[
                  {
                    key: seletedOption.key,
                    value: seletedOption.value,
                    sorting: true,
                    range_filter: false,
                    align: "left",
                  },
                  ...paramsList,
                ]}
                rowsData={styledRowsData}
                sortedColKey={sortedColKey}
                setSortedColKey={setSortedColKey}
                sortedDirection={sortedDirection}
                setSortedDirection={setSortedDirection}
                maxHeight={"70vh"}
                sortOptions={true}
              />
            </>
          )}
          {isLoading && (
            <Loader position="absolute" height="100%" bgColor="#FFFFFF60" />
          )}
        </GraphDimension>
      </GraphShadow>
    </Wrapper>
  );
};

export default ReadyReckoner;

const Wrapper = styled.div`
  // width: ${(props) => props.width};
  // height: ${(props) => props.height};
  // background-color: #f7fcff;
  box-sizing: border-box;
  padding: 10px;
`;

const Title = styled.div`
  color: #262e40;
  font-size: 25px;
  font-weight: 500;
  height: 40px;
  letter-spacing: 0em;
  text-align: left;
  margin-bottom: 15px;
`;

const TableMenuLeft = styled.div``;

const GraphShadow = styled.div`
  // width: calc(${(props) => props.width} - 20px);
  // height: calc(${(props) => props.height} - 60px);
  background-color: #ffffff;
  box-shadow: 2px 2px 4px 0px #00000040;
`;

const LoadingMessage = styled.div`
  min-height: 360px;
`;
const GraphDimension = styled.div`
  width: calc(${(props) => props.width});
  height: calc(${(props) => props.height} - 100px);
  padding: 15px;
  position: relative;
`;

const HeaderWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  padding: 0px 0px 30px;
`;
