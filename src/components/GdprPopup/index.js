import React, { useState } from "react";
import styled from "styled-components";
import CrossIcon from "../svg/CrossIcon";

const GdprPopup = ({
  position = "fixed",
  height = "100vh",
  bgColor = "#00000060",
}) => {
  const [isPopupOpen, setPopupOpen] = useState(false);

  return (
    <IconWrapper
      height={height}
      position={position}
      bgColor={bgColor}
      display={isPopupOpen ? "flex" : "none"}
    >
      <PopupWrap>
        <CloseIconWrap>
          <CrossIcon onIconClick={() => setPopupOpen(false)} />
        </CloseIconWrap>
        <PopupBody>
          <Heading>We value your privacy</Heading>
          <Message>
            Our website collects information on how you interact with it to
            enhance the services we provide to you. Please give your consent if
            you are okay with this.
          </Message>
          <SuccessButton>ACCEPT & CONTINUE</SuccessButton>
        </PopupBody>
      </PopupWrap>
    </IconWrapper>
  );
};

export default GdprPopup;

const IconWrapper = styled.div`
  width: 100%;
  height: ${(props) => props.height};
  display: ${(props) => props.display};
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.bgColor};
  position: ${(props) => props.position};
  z-index: 5;
  overflow-y: hidden;
`;

const PopupWrap = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 45%;
  max-width: 600px;
  height: 260px;
  max-height: 60vh;
  background-color: #ffffff;
  z-index: 5;
  box-shadow: 2px 2px 4px 0px #00000040;
  border: 0.75px solid #ededed;
  border-radius: 5px;
  color: #262e40;
  cursor: default;
`;

const CloseIconWrap = styled.div`
  display: flex;
  justify-content: end;
  width: 100%;
  height: 34px;
  padding: 10px 10px 0px 0px;
`;

const PopupBody = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0px 25px 25px;
`;

const Heading = styled.div`
  font-size: 30px;
  font-weight: 500;
  padding: 10px 0px;
`;

const Message = styled.div`
  font-size: 14px;
  padding: 10px 0px;
`;

const SuccessButton = styled.button`
  height: 50px;
  padding: 12px 25px;
  color: #ffffff;
  background-color: #0099ff;
  border: none;
  border-radius: 5px;
  margin-top: 10px;
  border-radius: 50px;
  font-size: 20px;
  font-weight: 600;
`;
