import React from "react";
import styled from "styled-components";
import { BiUserPlus } from "react-icons/bi";
import { useNavigate } from "react-router-dom";
import PiechartIcon from "../svg/PiechartIcon";
import AdminConsoleIcon from "../svg/AdminConsole";

const UserCard = () => {
  const navigate = useNavigate();
  const Invite = () => {
    window.location.href = "/invitations";
  };

  const handleSignOut = () => {
    window.localStorage.setItem("loginStatus", "false");
    console.log("signout");
    window.localStorage.clear();
    navigate("/");
  };

  const investor_id = window.localStorage.getItem("investor_id");
  const client_id = Number(window.localStorage.getItem("clientID"));
  return (
    <UserCardDiv>
      <div
        style={{ display: "flex", justifyContent: "center", marginTop: "30px" }}
      >
        {window.localStorage.getItem("gender_male") === "false" ? (
          <img
            src="/Images/userWomen.png"
            alt=""
            style={{ width: "3.5vw", borderRadius: "40px" }}
          />
        ) : (
          <img
            src="/Images/userMen.png"
            alt=""
            style={{ width: "3.5vw", borderRadius: "40px" }}
          />
        )}
      </div>
      <Namediv>
        {window.localStorage.getItem("user_name") !== undefined
          ? window.localStorage.getItem("user_name")
          : window.localStorage.getItem("email").split("@")[0]}
      </Namediv>
      <Emaildiv>{window.localStorage.getItem("email")}</Emaildiv>
      <Invitediv
        onClick={() => {
          Invite();
        }}
      >
        <img
          style={{ height: "18px", marginRight: "10px" }}
          src="/Images/adduser.png"
          alt=""
        />
        Invite
      </Invitediv>
      {investor_id && (
        <Portfoliodiv
          onClick={() => {
            navigate("/portfolio");
          }}
        >
          <div style={{ height: "18px", marginRight: "10px" }}>
            <PiechartIcon />
          </div>
          My Portfolio
        </Portfoliodiv>
      )}
      {(client_id === 1 || client_id === 53) && (
        <AdminConsolediv
          onClick={() => {
            navigate("/admin-console");
          }}
        >
          <div style={{ height: "18px", marginRight: "10px" }}>
            <AdminConsoleIcon />
          </div>
          Admin Console
        </AdminConsolediv>
      )}
      <SignOutDiv
        onClick={() => {
          handleSignOut();
        }}
      >
        <img
          style={{ height: "18px", marginRight: "5px" }}
          src="/Images/usersignout.png"
          alt=""
        />{" "}
        Sign Out
      </SignOutDiv>
    </UserCardDiv>
  );
};

export default UserCard;

const UserCardDiv = styled.div`
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.5);
  position: relative;
  top: 0px;
  width: 19vw;
  padding: 5px;
  left: 80vw;
  border: 5px;
  z-index: 10;
  background-color: white;
  border-radius: 10px;
`;

const Namediv = styled.div`
  text-align: center;
  font-size: 18px;
  font-weight: 500;
`;

const Emaildiv = styled.div`
  text-align: center;
  font-size: 10px;
  padding-bottom: 20px;
  border-bottom: 1px solid #c9c9c9;
`;

const Invitediv = styled.div`
  color: #20a6ff;
  font-size: 15px;
  cursor: pointer;
  padding: 10px;
`;

const Portfoliodiv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  color: #20a6ff;
  font-size: 15px;
  cursor: pointer;
  padding: 10px;
`;

const SignOutDiv = styled.div`
  color: #20a6ff;
  font-size: 15px;
  cursor: pointer;
  padding: 10px;
`;

const AdminConsolediv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  color: #20a6ff;
  font-size: 15px;
  cursor: pointer;
  padding: 10px;
`;
