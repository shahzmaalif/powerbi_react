import React from 'react'
import { event } from 'react-ga'
import styled from 'styled-components'
import { useNavigate } from 'react-router-dom';
import { AiOutlineShoppingCart } from 'react-icons/ai';
import { BsBagCheck } from 'react-icons/bs';
import { BiFridge } from 'react-icons/bi';
import { MdOutlineHealthAndSafety, MdPersonalVideo } from 'react-icons/md';
import MyContext from '../../utils/contexts/MyContext';
import {
  onSectorReportOpenCT
} from "../../utils/clevertap";

const TreeDropDn = () => {
    const { treeData, setTreeData } = React.useContext(MyContext);
    const navigate = useNavigate();
    let icondic = {
        'Online Retail':<AiOutlineShoppingCart/>,
        'Grocery':<BsBagCheck/>,
        'eHealth':<MdOutlineHealthAndSafety/>,
        'Digital Content':<MdPersonalVideo/>,
        'Discover More':<img src = '/Images/square4.png' alt = '' style={{height:'11px'}}/>,
        'Brandverse':<BiFridge/>
    }

    let data = [
        {
            "key": 9,
            "label": "Consumer Internet",
            "finalized": false,
            "filter_value": null,
            "filter": null,
            "key_val": 9,
            "node_type": null,
            "subscribed": true,
            "has_link": true,
            "nodes": [
                {
                    "key": 25,
                    "label": "Online Retail",
                    "finalized": true,
                    "filter_value": null,
                    "filter": null,
                    "key_val": 25,
                    "node_type": null,
                    "subscribed": true,
                    "has_link": true,
                    "nodes": [
                        {
                            "key": 70,
                            "label": "Horizontals",
                            "finalized": true,
                            "filter_value": null,
                            "filter": null,
                            "key_val": 70,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 43,
                            "label": "Fashion",
                            "finalized": true,
                            "filter_value": "Fashion",
                            "filter": "categories",
                            "key_val": 43,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 49,
                            "label": "Beauty and Personal Care",
                            "finalized": true,
                            "filter_value": "Beauty & Personal Care",
                            "filter": "categories",
                            "key_val": 49,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 52,
                            "label": "Home and Furniture",
                            "finalized": true,
                            "filter_value": "Home & Furniture",
                            "filter": "categories",
                            "key_val": 52,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 263,
                            "label": "Babycare",
                            "finalized": true,
                            "filter_value": "Babycare",
                            "filter": "categories",
                            "key_val": 263,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 61,
                            "label": "Pharma",
                            "finalized": true,
                            "filter_value": null,
                            "filter": null,
                            "key_val": 61,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 280,
                            "label": "Brandverse Electronics",
                            "finalized": false,
                            "filter_value": null,
                            "filter": null,
                            "key_val": 280,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": [
                                {
                                    "key": 270,
                                    "label": "MLE",
                                    "finalized": true,
                                    "filter_value": null,
                                    "filter": null,
                                    "key_val": 270,
                                    "node_type": null,
                                    "subscribed": true,
                                    "has_link": true,
                                    "nodes": [
                                        {
                                            "key": 271,
                                            "label": "Context MLE",
                                            "finalized": true,
                                            "filter_value": null,
                                            "filter": null,
                                            "key_val": 271,
                                            "node_type": null,
                                            "subscribed": true,
                                            "has_link": true,
                                            "nodes": []
                                        },
                                        {
                                            "key": 272,
                                            "label": "Dashboard MLE",
                                            "finalized": true,
                                            "filter_value": null,
                                            "filter": null,
                                            "key_val": 272,
                                            "node_type": null,
                                            "subscribed": true,
                                            "has_link": true,
                                            "nodes": []
                                        },
                                        {
                                            "key": 273,
                                            "label": "Subcategory MLE",
                                            "finalized": true,
                                            "filter_value": null,
                                            "filter": null,
                                            "key_val": 273,
                                            "node_type": null,
                                            "subscribed": true,
                                            "has_link": true,
                                            "nodes": []
                                        },
                                        {
                                            "key": 274,
                                            "label": "Company Profile MLE",
                                            "finalized": true,
                                            "filter_value": null,
                                            "filter": null,
                                            "key_val": 274,
                                            "node_type": null,
                                            "subscribed": true,
                                            "has_link": true,
                                            "nodes": []
                                        }
                                    ]
                                },
                                {
                                    "key": 281,
                                    "label": "SHA",
                                    "finalized": true,
                                    "filter_value": null,
                                    "filter": null,
                                    "key_val": 281,
                                    "node_type": null,
                                    "subscribed": true,
                                    "has_link": true,
                                    "nodes": [
                                        {
                                            "key": 282,
                                            "label": "Context SHA",
                                            "finalized": true,
                                            "filter_value": null,
                                            "filter": null,
                                            "key_val": 282,
                                            "node_type": null,
                                            "subscribed": true,
                                            "has_link": true,
                                            "nodes": []
                                        },
                                        {
                                            "key": 283,
                                            "label": "Dashboard SHA",
                                            "finalized": true,
                                            "filter_value": null,
                                            "filter": null,
                                            "key_val": 283,
                                            "node_type": null,
                                            "subscribed": true,
                                            "has_link": true,
                                            "nodes": []
                                        },
                                        {
                                            "key": 284,
                                            "label": "Subcategory SHA",
                                            "finalized": true,
                                            "filter_value": null,
                                            "filter": null,
                                            "key_val": 284,
                                            "node_type": null,
                                            "subscribed": true,
                                            "has_link": true,
                                            "nodes": []
                                        },
                                        {
                                            "key": 285,
                                            "label": "Company Profile SHA",
                                            "finalized": true,
                                            "filter_value": null,
                                            "filter": null,
                                            "key_val": 285,
                                            "node_type": null,
                                            "subscribed": true,
                                            "has_link": true,
                                            "nodes": []
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                            "key": 67,
                            "label": "Grocery",
                            "finalized": true,
                            "filter_value": "Grocery",
                            "filter": "sector",
                            "key_val": 67,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": [
                                {
                                    "key": 261,
                                    "label": "Quick Commerce",
                                    "finalized": true,
                                    "filter_value": "QC",
                                    "filter": "sector",
                                    "key_val": 261,
                                    "node_type": null,
                                    "subscribed": true,
                                    "has_link": true,
                                    "nodes": []
                                },
                                {
                                    "key": 262,
                                    "label": "Slotted Delivery",
                                    "finalized": true,
                                    "filter_value": "Slotted Delivery",
                                    "filter": "sector",
                                    "key_val": 262,
                                    "node_type": null,
                                    "subscribed": true,
                                    "has_link": true,
                                    "nodes": []
                                },
                                {
                                    "key": 269,
                                    "label": "NDM",
                                    "finalized": true,
                                    "filter_value": "NDM",
                                    "filter": "sector",
                                    "key_val": 269,
                                    "node_type": null,
                                    "subscribed": true,
                                    "has_link": true,
                                    "nodes": []
                                }
                            ]
                        },
                {
                    "key": 72,
                    "label": "Digital Content",
                    "finalized": true,
                    "filter_value": null,
                    "filter": null,
                    "key_val": 72,
                    "node_type": null,
                    "subscribed": true,
                    "has_link": false,
                    "nodes": [
                        {
                            "key": 11,
                            "label": "Audio Streaming",
                            "finalized": true,
                            "filter_value": null,
                            "filter": null,
                            "key_val": 11,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 21,
                            "label": "Video Streaming",
                            "finalized": true,
                            "filter_value": null,
                            "filter": null,
                            "key_val": 21,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 75,
                            "label": "Social Media",
                            "finalized": true,
                            "filter_value": null,
                            "filter": null,
                            "key_val": 75,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 78,
                            "label": "Shortform Video",
                            "finalized": true,
                            "filter_value": null,
                            "filter": null,
                            "key_val": 78,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        }
                    ]
                },
                {
                    "key": 81,
                    "label": "Food Delivery",
                    "finalized": true,
                    "filter_value": null,
                    "filter": null,
                    "key_val": 81,
                    "node_type": null,
                    "subscribed": true,
                    "has_link": true,
                    "nodes": []
                },
                {
                    "key": 84,
                    "label": "Ride Hailing",
                    "finalized": true,
                    "filter_value": null,
                    "filter": null,
                    "key_val": 84,
                    "node_type": null,
                    "subscribed": true,
                    "has_link": true,
                    "nodes": []
                },
                {
                    "key": 87,
                    "label": "Real Money Gaming",
                    "finalized": true,
                    "filter_value": null,
                    "filter": null,
                    "key_val": 87,
                    "node_type": null,
                    "subscribed": true,
                    "has_link": true,
                    "nodes": []
                },
                {
                    "key": 95,
                    "label": "eHealth",
                    "finalized": false,
                    "filter_value": null,
                    "filter": null,
                    "key_val": 95,
                    "node_type": null,
                    "subscribed": true,
                    "has_link": false,
                    "nodes": [
                        {
                            "key": 265,
                            "label": "e-Diagnostics",
                            "finalized": true,
                            "filter_value": "eDiagnostics",
                            "filter": "medical",
                            "key_val": 265,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        },
                        {
                            "key": 104,
                            "label": "e-Pharma and OTC",
                            "finalized": true,
                            "filter_value": "ePharma",
                            "filter": "medical",
                            "key_val": 104,
                            "node_type": null,
                            "subscribed": true,
                            "has_link": true,
                            "nodes": []
                        }
                    ]
                },
                {
                    "key": 112,
                    "label": "Used Cars",
                    "finalized": true,
                    "filter_value": null,
                    "filter": null,
                    "key_val": 112,
                    "node_type": null,
                    "subscribed": true,
                    "has_link": true,
                    "nodes": []
                },
                {
                    "key": 255,
                    "label": "eLogistics",
                    "finalized": true,
                    "filter_value": null,
                    "filter": null,
                    "key_val": 255,
                    "node_type": null,
                    "subscribed": true,
                    "has_link": true,
                    "nodes": []
                },
                {
                    "key": 91,
                    "label": "Online Education",
                    "finalized": true,
                    "filter_value": null,
                    "filter": null,
                    "key_val": 91,
                    "node_type": null,
                    "subscribed": true,
                    "has_link": true,
                    "nodes": []
                }
            ]
        }
    ]

    let node_data  = treeData[0]['nodes']


    let handleClick =  (val, key, filter, filter_value, subscribed, industry_id) =>{
				onSectorReportOpenCT(val, key, subscribed);
        try{
            filter_value = filter_value.replace(/&/g, "and");            
        }catch(err){
            console.log('& absent')
        }
        window.localStorage.setItem('industry_id', industry_id)
        window.localStorage.setItem('report', val)
        
        window.open(`/Report3/?val=${val}&key=${key}&filter_value=${filter_value}&filter=${filter}&industry_id=${industry_id}`);
        // navigate(`/Report3/?val=${val}&key=${key}&filter_value=${filter_value}&filter=${filter}&industry_id=${industry_id}`);
        // becuase report3 wont reload on changing address
        // window.location.reload()
        // use redux to set values . redirect and then use those . can also use context provider. can also use query params.
        // can also use this.props.navigation.navigate('RouteName', { /* params go here */ })

        console.log(val)
    }
    
  return (
    <TreeGridDiv>
            {node_data
            .filter((val)=>val.nodes.length>0 & val.label ==='Online Retail')
            .map((val, index) => (
                <Online key={index}>
                    <h6><span style={{display:'flex', alignItems:'center', gap:'5px', fontSize:'17px'}}>{icondic[val.label]}{val.label}</span></h6>
                    <div>
                        {val.nodes.map((ele, i)=>{
                            const url = `/Report3/?val=${encodeURIComponent(ele.label)}&key=${encodeURIComponent(ele.key_val)}&filter_value=${encodeURIComponent(ele.filter_value)}&filter=${encodeURIComponent(ele.filter)}&industry_id=${encodeURIComponent(ele.industry_id)}`;
                            return(
                                <a 
                                key={i} 
                                href={url} 
                                target="_blank" 
                                rel="noopener noreferrer" 
                                style={{ display: 'block', padding: '5px', fontSize: '14px' }}
                                onClick={(e) => {
                                    // Prevent default if it's a normal click, not CMD/CTRL click
                                    e.preventDefault();
                                    handleClick(ele.label, ele.key_val, ele.filter, ele.filter_value, ele.subscribed, ele.industry_id);
                                    // if (!e.metaKey && !e.ctrlKey) {
                                    //     e.preventDefault();
                                    //     handleClick(ele.label, ele.key_val, ele.filter, ele.filter_value, ele.subscribed, ele.industry_id);
                                    // }
                                }}
                            >
                                {ele.label}{ele.subscribed?<span style={{marginLeft:'5px', backgroundColor: '#0a99fe', fontSize:'10px', color:'white', padding:'2px',borderRadius:'5px'}}>Subscribed</span>:null}
                            </a>
                        )})}
                    </div>
                </Online>
            ))
            }
        {node_data
        .filter((val) => val.nodes.length > 0 & val.label !=='Online Retail') // Filter the array based on the condition
        .map((val, index) => (
            <div key={index}>
                <h6><span style={{display:'flex', alignItems:'center', gap:'5px'}}>{icondic[val.label]}{val.label}</span></h6>
                <div>
                    {val.nodes.map((ele, i)=>{
                        const url = `/Report3/?val=${encodeURIComponent(ele.label)}&key=${encodeURIComponent(ele.key_val)}&filter_value=${encodeURIComponent(ele.filter_value)}&filter=${encodeURIComponent(ele.filter)}&industry_id=${encodeURIComponent(ele.industry_id)}`;
                        return(
                            <a 
                            key={i} 
                            href={url} 
                            target="_blank" 
                            rel="noopener noreferrer" 
                            style={{ display: 'block', padding: '5px', fontSize: '14px' }}
                            onClick={(e) => {
                                // Prevent default if it's a normal click, not CMD/CTRL click
                                e.preventDefault();
                                handleClick(ele.label, ele.key_val, ele.filter, ele.filter_value, ele.subscribed, ele.industry_id);
                                // if (!e.metaKey && !e.ctrlKey) {
                                //     e.preventDefault();
                                //     handleClick(ele.label, ele.key_val, ele.filter, ele.filter_value, ele.subscribed, ele.industry_id);
                                // }
                            }}
                        >
                            {ele.label} {ele.subscribed?<span style={{marginLeft:'5px', backgroundColor: '#0a99fe', fontSize:'10px', color:'white', padding:'2px',borderRadius:'5px'}}>Subscribed</span>:null}
                        </a>
                    )}
                    )}
                </div>
            </div>
        ))}
        <Discoverdiv>
            <h6 >
            <span style={{display:'flex', alignItems:'center', gap:'5px'}}>{icondic['Discover More']}Discover More</span>
            </h6>
            {node_data
            .filter((val)=>val.nodes.length===0)
            .map((val, index)=>{
                const url = `/Report3/?val=${encodeURIComponent(val.label)}&key=${encodeURIComponent(val.key_val)}&filter_value=${encodeURIComponent(val.filter_value)}&filter=${encodeURIComponent(val.filter)}&industry_id=${encodeURIComponent(val.industry_id)}`;
                return(
                    <a 
                    key={index} 
                    href={url} 
                    target="_blank" 
                    rel="noopener noreferrer" 
                    style={{ display: 'block', padding: '5px', fontSize: '14px' }}
                    onClick={(e) => {
                        e.preventDefault();
                        handleClick(val.label, val.key_val, val.filter, val.filter_value, val.subscribed, val.industry_id);
                        // Prevent default if it's a normal click, not CMD/CTRL click
                        // if (!e.metaKey && !e.ctrlKey) {
                        //     e.preventDefault();
                        //     handleClick(val.label, val.key_val, val.filter, val.filter_value, val.subscribed, val.industry_id);
                        // }
                    }}
                >
                    {val.label} {val.subscribed?<span style={{marginLeft:'5px', backgroundColor: '#0a99fe', fontSize:'10px', color:'white', padding:'2px',borderRadius:'5px'}}>Subscribed</span>:null}
                </a>
            )
        })
            }
        </Discoverdiv>
    </TreeGridDiv>
  )
}


export default TreeDropDn

const TreeGridDiv = styled.div`
    padding-left:3.5vw;
    padding-right:5px;
    padding-top:25px;
    padding-bottom:10px;
    display:grid;
  grid-auto-columns: 1fr; 
  grid-auto-rows: 1fr; 
  grid-template-columns: 1fr 1fr 1fr 1fr; 
  grid-template-rows: auto; 
  gap: 10px 70px; 
  font-family:'Fira-Sans', sans-serif;
  grid-template-areas: 
    "Online . . Discoverdiv"
    "Online . . Discoverdiv"; 

    div{
        div{
            padding-left:10px;
            div{
                &:hover{
                background-color:#0099FE;
                color:white;
                cursor: pointer;
            }
            }
            a{
                &:hover{
                background-color:#0099FE;
                color:white;
                cursor: pointer;
            }
            }
        }
    }

`

const Online = styled.div`
    grid-area:Online;

`
const Discoverdiv  = styled.div`
    grid-area:Discoverdiv;
    div{
        &:hover{
                background-color:#0099FE;
                color:white;
                cursor: pointer;
        }
    }
    a{
                &:hover{
                background-color:#0099FE;
                color:white;
                cursor: pointer;
            }
            }
`