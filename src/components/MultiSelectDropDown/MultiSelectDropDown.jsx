// MultiSelectDropdown.jsx
import React, { useState } from 'react';
import styled from 'styled-components';

const DropdownContainer = styled.div`
    width: 250px;
    position: relative;
`;

const DropdownHeader = styled.div`
    padding: 10px;
    border: 1px solid #ccc;
    cursor: pointer;
`;

const DropdownListContainer = styled.div`
    border: 1px solid #ccc;
    position: absolute;
    top: 100%;
    left: 0;
    width: 100%;
    background-color: #fff;
    z-index: 1;
`;

const DropdownList = styled.ul`
    list-style: none;
    padding: 0;
    margin: 0;
`;

const ListItem = styled.li`
    padding: 10px;
    cursor: pointer;
    &:hover {
        background-color: #f5f5f5;
    }
`;

const MultiSelectDropdown = ({ options, styles }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOptions, setSelectedOptions] = useState([]);

    const toggleDropdown = () => setIsOpen(!isOpen);

    const handleOptionClick = (value) => {
        if (selectedOptions.includes(value)) {
            setSelectedOptions(prev => prev.filter(item => item !== value));
        } else {
            setSelectedOptions(prev => [...prev, value]);
        }
    };

    return (
        <DropdownContainer style={styles.container}>
            <DropdownHeader onClick={toggleDropdown} style={styles.header}>
                {selectedOptions.join(', ') || 'Select Metrics'}
            </DropdownHeader>
            {isOpen && (
                <DropdownListContainer>
                    <DropdownList>
                        {options.map(option => (
                            <ListItem 
                                key={option.value}
                                onClick={() => handleOptionClick(option.value)}
                                style={selectedOptions.includes(option.value) ? styles.selectedOption : styles.option}
                            >
                                {option.label}
                            </ListItem>
                        ))}
                    </DropdownList>
                </DropdownListContainer>
            )}
        </DropdownContainer>
    );
};

export default MultiSelectDropdown;
