import React from 'react'
import styled from 'styled-components'
import { Carousel } from 'react-responsive-carousel';
import Sub from '../../pages/Sub';


const NewsletterSlides = (props) => {
    return (
        <NewsletterSlidesContainer>
            {window.localStorage.getItem('clientID')!=='141'?<Carousel showThumbs = {false} infiniteLoop = {true}>
                {props.newsletterSlidesdata.map((val, i) => (
                    <NewsletterContainer key={i}>
                        <div
                                    dangerouslySetInnerHTML={{
                                      __html: val.htmltext,
                                    }}
                                  />
                    </NewsletterContainer>
                ))}
            </Carousel>:
            <Carousel showThumbs = {false} infiniteLoop = {true}>
            {props.newsletterSlidesdata.map((val, i) => (
                <NewsletterContainer key={i}>
                    <div
                                dangerouslySetInnerHTML={{
                                  __html: val.htmltext,
                                }}
                              />
                </NewsletterContainer>
            ))}
            <Sub/>
        </Carousel>
            }
        </NewsletterSlidesContainer>
    );
};

export default NewsletterSlides

const NewsletterContainer = styled.div`

    

`

const NewsletterSlidesContainer = styled.div`
    background-color:white;
    margin:4vh 3.5vw;
    margin-bottom:2vh;
    min-height:100px;
`