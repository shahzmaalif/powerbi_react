import React, { useEffect } from "react";
import styled from "styled-components";
import { useState } from "react";
import axios from "axios";

const QueryFooter = (props) => {
  const [queryData, setQueryData] = useState("");
  const [authorData, setAuthorData] = useState(null);
  const [querySubmit, setQuerySubmit] = useState(true);
  const [error, setError] = useState("");
  const [isValidData, setValidData] = useState(false);
  const handleInputChange = (event) => {
    setQueryData(event.target.value);
    if (event.target.value.length < 10) {
      setError("Include more information to submit.");
      setValidData(false);
    } else {
      setError("");
      setValidData(true);
    }
  };
  const sendQuery = () => {
    console.log(queryData);
    console.log("report= ", window.localStorage.getItem("report"));
    const uploadData = new FormData();
    uploadData.append("email", window.localStorage.getItem("email"));
    uploadData.append("query", queryData);
    uploadData.append("report", window.localStorage.getItem("rep_id"));
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/userquery/`, {
      method: "POST",
      body: uploadData,
    })
      .then((data) => data.json())
      .then((data) => {
        console.log("data = ", data);
      });

    setQuerySubmit(!querySubmit);
  };

  useEffect(() => {
    let report_id = window.localStorage.getItem("rep_id");
    if (!report_id) {
      report_id = 43;
    }
    const getauthordata = async () => {
      let req = await axios.get(
        `${process.env.REACT_APP_API_ENDPOINT}/author/?report_id=${report_id}`
      );
      let data = req.data;
      if (data.length > 0) {
        data = data[0];
        console.log("authordata");
        setAuthorData(data);
      }
    };
    getauthordata();
  }, []);

  return (
    <div>
      {querySubmit ? (
        <div>
          {/* <QueryTextdiv> Query / Feedbacks</QueryTextdiv> */}
          {authorData && (
            <QueryFooterSurrounddiv>
              <Title>Contact our analyst</Title>
              <QueryFooterdiv>
                <div>
                  <Introdiv>
                    <div>
                      <img
                        style={{
                          width: "60px",
                          height: "60px",
                          borderRadius: "50%",
                        }}
                        src={authorData.image.file}
                        alt=""
                      />
                    </div>
                    <div>
                      <h4>{authorData.name}</h4>
                      <div>{authorData.position}</div>
                    </div>
                  </Introdiv>
                  <div>{authorData.displaytext}</div>
                </div>

                <div>
                  {/* <h3>Contact our analyst </h3> */}
                  <QueryBoxdiv
                    onChange={handleInputChange}
                    placeholder="Write here..."
                  />
                  <QueryButtondiv>
                    {error && <div>{error}</div>}
                    <button onClick={() => sendQuery()} disabled={!isValidData}>
                      Send Query
                    </button>
                  </QueryButtondiv>
                </div>
              </QueryFooterdiv>
            </QueryFooterSurrounddiv>
          )}
        </div>
      ) : (
        <QuerySubmitdiv>Your query has been submitted</QuerySubmitdiv>
      )}
    </div>
  );
};

export default QueryFooter;

const QueryFooterdiv = styled.div`
  padding: 20px;
  box-shadow: 2px 2px 4px 0px #00000040;
  /* padding-left:1.4vw;
    padding-right:3.5vw; */
  /* background-color:#EEF9FF; */
  background-color: white;
  display: flex;
  gap: 3vw;
  justify-content: space-between;
`;

const Title = styled.div`
  color: #262e40;
  font-size: 25px;
  font-weight: 500;
  height: 40px;
  letter-spacing: 0em;
  text-align: left;
  margin-bottom: 15px;
`;

const QueryTextdiv = styled.div`
  font-size: 25px;
  padding-left: 3.5vw;
  font-weight: 500;
  padding-bottom: 10px;
`;

const QueryBoxdiv = styled.textarea`
  padding: 10px;
  width: 900px;
  height: 100px;
  border-radius: 10px;
  border: 1px solid black;
`;

const QueryButtondiv = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  align-items: end;
  justify-content: flex-end;
  button {
    background-color: #0099ff;
    color: white;
    border: none;
    border-radius: 10px;
    width: 120px;
    height: 40px;
    :disabled {
      background-color: #f2f2f2;
      border: 1px solid #d9d9d9;
      color: #757575;
      cursor: not-allowed;
    }
  }
  div {
    color: #e93735;
    padding-right: 20px;
  }
`;

const QuerySubmitdiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 20px;
  font-weight: 500;
  padding: 20px;
`;
const QueryFooterSurrounddiv = styled.div`
  background-color: #eef9ff;
  padding: 3.5vw;
  padding-top: 40px;
`;

const Introdiv = styled.div`
  display: flex;
  gap: 10px;
  margin-bottom: 10px;
`;
