import React from 'react'
// import { BarChart, Bar, Cell } from 'recharts';
import { BarChart, Bar, Cell, Rectangle, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { useState, useRef } from 'react';
import styled from 'styled-components';
import MultiSelectDropdown from '../MultiSelectDropDown/MultiSelectDropDown';
import Select from "react-select";
import { useEffect } from 'react';
import axios from 'axios';
import {IoIosArrowUp,IoIosArrowDown } from "react-icons/io";
import { BsFillTriangleFill } from "react-icons/bs";
import { Switch } from 'antd';
import { TbTriangleFilled } from "react-icons/tb";
import { TbTriangleInvertedFilled } from "react-icons/tb";
import MyContext from '../../utils/contexts/MyContext';
import { getNodeText } from '@testing-library/react';
import { RiContrastDropLine } from 'react-icons/ri';

const graphdata = [
    {
      name: 'Nykaa',
      uv: 4000,
      pv: 2400,
      amt: 2400,
    },
  ];

  const CustomTooltip = ({ active, payload , coordinate }) => {
    if (active && payload && payload.length) {
      const value = payload[0].value; // Assumes value is in the first payload item
      const total = 100 // Calculates total from all payload items
      const tooltipY = coordinate.y - 200;
      return (
        <div className="custom-tooltip" >
          <p>Value = {value}</p>
        </div>
      );
    }
  
    return null;
  };
const CustomBarGraph = ({ data }) => (
    <BarChart width={100} height={30} data={[data]} layout="vertical">
        <XAxis type="number" hide={true} domain={[0, 100]}/>
        <YAxis type="category" dataKey="name" hide={true}/>
        <Bar dataKey="value" stackId="rst" fill="#FFA500">
        </Bar>
        <Bar dataKey="total" stackId="rst" fill="yellow">
        </Bar>
        {/* <Tooltip content={<CustomTooltip />} position={{ y: 0 }}/> */}
        {/* <Tooltip content={<CustomTooltip />} cursor={{ fill: "transparent" }} /> */}
    </BarChart>
);

const TableRow = ({ player, sector, gmv, userBase, cmRange }) => (
    <tr>
        <td>{player}</td>
        <td>{sector}</td>
        <td>
            {console.log('gmv= ',gmv)}
            <BarWrapper>
                <CustomBarGraph data={gmv} />
                <GrowthValue growth={gmv.growth}>
                    {gmv.value} {gmv.growth >= 0 ? "↑" : "↓"} {gmv.growth}%
                </GrowthValue>
            </BarWrapper>
        </td>
        <td>
            <BarWrapper>
                <CustomBarGraph data={userBase} />
                <GrowthValue growth={userBase.growth}>
                    {userBase.value} {userBase.growth >= 0 ? "↑" : "↓"} {userBase.growth}%
                </GrowthValue>
            </BarWrapper>
        </td>
        <td>{cmRange}</td>
    </tr>
);

const rowData = [
    { id: 1, fixedCol1: "Nykaa", fixedCol2: "Fashion", fixedCol3: 10, qoqGrowth: { value: '4.2', total:'2.4',growth: 3.2 , name:'Nyka'}, cmRange: { value: '2.1', total:'1.2', growth: 23.2 , name:'Nyka'}, profitability: { value: '2.1', total:'1.2', growth: 23.2 , name:'Nyka'} },
    { id: 2, fixedCol1: "Amazon", fixedCol2: "Fashion", fixedCol3:15, qoqGrowth: { value: '4.2', total:'2.4',growth: 3.2 , name:'Nyka'}, cmRange: { value: '2.1', total:'1.2', growth: 23.2 , name:'Nyka'}, profitability:{ value: '2.1', total:'1.2', growth: 23.2 , name:'Nyka'} },
  ];

function DropdownTable({ columns }) {
    return (
      <table>
        <thead>
          <tr>
            <th>Fixed Col 1</th>
            <th>Fixed Col 2</th>
            <th>Fixed Col 3</th>
            {columns.map((col) => (
              <th key={col.value}>{col.label}</th>
            ))}
          </tr>
        </thead>
        <tbody>
        {rowData.map((row) => (
          <tr key={row.id}>
            <td>{row.fixedCol1}</td>
            <td>{row.fixedCol2}</td>
            <td>{row.fixedCol3}</td>
            {columns.map((col) => (
              <td key={`${row.id}-${col.value}`}>{row[col.value]}</td>
              
            ))}
          </tr>
        ))}
        </tbody>
      </table>
    );
  }
  

const Fintable = (props) => {

    const [selectedOptions, setSelectedOptions] = useState([]);
    const [tabledata, settabledata] = useState([])
    const [dropdndata, setdropdndata] = useState([])
    const [paramtypedata, setparamtypedata] = useState([])
    const [uniqueplayers, setuniqueplayers] = useState([])
    const [defaultValue, setDefaultValue] = useState(null);
    const [sortColumn, setSortColumn] = useState(null); 
    const [sortOrder, setSortOrder] = useState('desc');
    const [showgrid,setshowgrid] = useState(true)
    const [month, setmonth] = useState(null)
    const { treeData, setTreeData } = React.useContext(MyContext);
    let defval = {value: 'Financial', label: 'Financial'};
    const tabledataref = useRef([]);
    useEffect(()=>{
        const getdropdown = async() =>{

                let industry_id = props.industry_id
                if (!industry_id){
                    industry_id = window.localStorage.getItem('industry_id')
                }
                const response = await axios.get(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/parametertype/?industry_id=${industry_id}`)
                // const ddata = await(paramdropdownservice(industry_id))
                let data = response.data
                setparamtypedata(data)
                let uniqueParameterTypes = [...new Set(data.map(item => item.parameter_type))];
                let newArray = uniqueParameterTypes.map(parameterType => ({
                    value: parameterType,
                    label: parameterType
                }));

                console.log('newArray = ', newArray)
                if (newArray.length > 0) {
                    let scale_present = false
                    for (let i =0; i< newArray.length; i++){
                        if (newArray[i].value === 'Scale of business'){
                            handleSelectOptions(newArray[i], data)
                            setDefaultValue(newArray[i]);
                            defval = newArray[i]
                            scale_present = true
                            break
                        }
                    }
                    if(scale_present===false){
                        handleSelectOptions(newArray[0], data)
                        setDefaultValue(newArray[0]);
                        defval = newArray[0]
                    }
                  }
                setdropdndata(newArray)
              }
        getdropdown()
    },[])

    const options = [
        { value: "qoqGrowth", label: "QoQ Growth" ,tyname:'123'},
        { value: "cmRange", label: "CM Range" },
        { value: "profitability", label: "Profitability" }
        // ... add other options here
      ];

    const data = [
        {
            player: 'Nykkaa',
            sector: 'Fashion',
            gmv: { value: '2.1', total:'1.2', growth: 23.2 , name:'Nyka'},
            userBase: { value: '4.2', total:'2.4',growth: 3.2 , name:'Nyka'},
            // cmRange: '2.1M'
        },
    ];

    const dropdownOptions = [
        { value: 'apple', label: 'Apple' },
        { value: 'banana', label: 'Banana' },
        { value: 'cherry', label: 'Cherry' }
    ];
      

    const customStyles = {
        container: { width: '300px' },
        header: { backgroundColor: '#eee' },
        option: { backgroundColor: '#fff' },
        selectedOption: { backgroundColor: '#ddd' }
    };

    function getIndustryAvg(data, industry_id){

        const filteredData = data.filter(entry => entry.industry_id === industry_id);

        // Sum the current_value of these filtered entries
        const total = filteredData.reduce((sum, entry) => {
            return sum + parseFloat(entry.current_value);
        }, 0);

        // Calculate the average. If there are no entries, avoid division by zero.
        const industry_avg = filteredData.length > 0 ? total / filteredData.length : 0;
        
        return industry_avg;


    }

    const handleSelectOptions = async(option, paramdata = [])=>{
        let arr = []
        if (paramdata.length>0){
            arr = paramdata.filter((val)=>val.parameter_type===option.value)
        }else{
            arr = paramtypedata.filter((val)=>val.parameter_type===option.value)
        }
        arr = arr.filter(i=>i.default_selected===true)
        console.log('handleSelect = ',arr)
        arr.sort(function(a, b) {
            var keyA = a.order_by,
              keyB = b.order_by;
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
          });
        let industry_id =  props.industry_id
        if (!industry_id){
            industry_id = 10
        }
        let param_arr = arr.map((val)=>val.parameter)
        const postData = {
            "param_arr":param_arr,
            "industry_id":industry_id
          };
        const response = await axios.post(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/parametervalues/`, postData);
        let data = response.data
        let date = new Date(data[0].start_date);
        const month = date.toLocaleString('default', { month: 'short' });
        const year = date.getFullYear().toString().slice(-2);
        let month_name = `${month}'${year}`;
        setmonth(month_name)
        const parameterSums = {};


        data.forEach(entry => {
            if (!parameterSums[entry.parameter]) {
                parameterSums[entry.parameter] = 0;
            }
            parameterSums[entry.parameter] += parseFloat(entry.current_value);

        });
        data = data.map(entry => {
            const sumForParameter = parameterSums[entry.parameter];
            if(entry.current_value==0){
                entry.market_share = 0
            }else{
                entry.market_share = ((parseFloat(entry.current_value) / sumForParameter)*100).toFixed(2);
            }

            if(entry.display_type=='IndustryAvg'){
                entry.industry_avg =((entry.current_value/getIndustryAvg(data, entry.industry_id))*100).toFixed(2)
            }else{
                entry.industry_avg=0
            }

            return entry;
        });



        settabledata(data)
        tabledataref.current = data
        const players = new Set();
        data.forEach(entry => players.add(entry.player_name));
        let uniq_players = Array.from(players);
        console.log('uniq_players',uniq_players)
        setuniqueplayers(uniq_players)
        sortPlayersByValueData(arr[0].parameter, tabledataref.current, uniq_players)
        setSelectedOptions(arr)
    }

    const getValueForPlayerAndParameter = (player, parameterId, data) => {
        const entry = data.find(item => item.player_name === player && item.parameter === parameterId);
        // return entry ? entry.current_value & entry.growth : "-";
        if (entry) {
            if(entry.display_type==='IndustryAvg'){
                return {
                    player: entry.player,
                    growth: entry.growth,
                    current_value: entry.current_value,
                    graphdata:{'name':entry.player, 'value':entry.industry_avg , 'current_value':entry.current_value, total:100-entry.industry_avg , 'type':'Industry Average'}
                };
            }
            else if(entry.display_type==='MarketShare'){
                return {
                    player: entry.player,
                    growth: entry.growth,
                    current_value: entry.current_value,
                    graphdata:{'name':entry.player, 'value':entry.market_share , 'current_value':entry.current_value, total:100-entry.market_share, 'type': 'Market Share'}
                };
            }
            else{
                return {
                    player: entry.player,
                    growth: entry.growth,
                    current_value: entry.current_value,
                    
                };
            }
        }
        return {
            player: null,
            growth: '-',
            current_value: '-'
        };
    };

    function sortPlayersByValueData(columnParameter, data = [], uniqPlayers = []) {
        console.log('sorting = ', columnParameter,uniqPlayers, data)
        const order = (sortColumn === columnParameter && sortOrder === 'asc') ? 'asc' : 'desc';
        setSortColumn(columnParameter);
        setSortOrder(order);
        if(data.length===0){
            data = tabledata
        }
        if(uniqPlayers.length===0){
            uniqPlayers = uniqueplayers
        }
        const sortedPlayers = [...uniqPlayers].sort((a, b) => {
            const aValueData = getValueForPlayerAndParameter(a, columnParameter, data);
            const bValueData = getValueForPlayerAndParameter(b, columnParameter, data);
    
            if(order === 'asc') {
                return aValueData.current_value - bValueData.current_value;
            } else {
                return bValueData.current_value - aValueData.current_value;
            }
        });
        
        setuniqueplayers(sortedPlayers);
    }

    const onChange = (checked) => {
        console.log(`switch to ${checked}`);
        setshowgrid(checked)
      };

      const getNode = (player, data)=>{
        const searchNode = (nodes) => {
            for (const node of nodes) {
                // Convert both strings to lower case (or upper case) for case-insensitive comparison
                if (node.label.toLowerCase() === player.toLowerCase()) {
                    return node;
                }
    
                if (node.nodes && node.nodes.length > 0) {
                    const foundNode = searchNode(node.nodes);
                    if (foundNode) return foundNode;
                }
            }
    
            return null;
        };
    
        return searchNode(data);
      }

      const handleCompanyClick = (player)=>{
        let node = props.dropDownData.find(item => item.label.toLowerCase() === player.toLowerCase());
        window.localStorage.setItem('report_name', node.label)
        console.log('node = ', node)
        // props.onPlayerClick(node)
        window.open(`/Report3/?val=${node.label}&key=${node.key}&filter=${node.filter}&filter_value=${node.filter_value}&industry_id=${node.industry_id}`)
        // window.location.href = `/Report3/?val=${node.label}&key=${node.key}&filter=${node.filter}&filter_value=${node.filter_value}`
        // window.location.href=`/company-board?industry_id=${window.localStorage.getItem('industry_id')}&company=${player}`;
      }

      const checkPlayer = (player) =>{
        let value = props.dropDownData.find(item => item.name.toLowerCase() === player.toLowerCase());
        return value
      }

      const DisplayTooltip = ({ data }) => {
        return (
            <div className="tooltiptable">
                {data.graphdata?<div><span style = {{color:'#9e9e9e', fontSize:'12px'}}>{data.graphdata.type} - </span><span style={{fontSize:'12px', marginLeft:'23px'}}>{data.graphdata.value}%</span></div>:null}
                <div><span style = {{color:'#9e9e9e', fontSize:'12px'}}>Current Value - </span><span style={{fontSize:'12px', marginLeft:'20px'}}>{data.current_value}</span></div>
                <div><span style = {{color:'#9e9e9e', fontSize:'12px'}}>YOY Growth -  </span><span style={{fontSize:'12px', marginLeft:'28px'}}>{data.growth}% {data.growth>=0? <TbTriangleFilled style={{color:'green'}}/> : <TbTriangleInvertedFilled style={{color:'red'}}/>}</span></div>
            </div>
        );
    };

    return (
        <DashBoarddiv>
            {/* <div style={{'fontSize':'25px', 'fontWeight':'bold', marginBottom:'15px'}}>Platform DashBoard</div>
            <div>
                Grid <Switch defaultChecked onChange={onChange} /> List
            </div> */}
           {showgrid?<Tablediv>
            <Selectdiv>
            <div style={{marginLeft:'15px'}}>For the month of {month}</div>
                <div style={{display:'flex', alignItems:'center'}}>
                    <span style={{'lineHeight':'38px', marginRight:'20px'}}>Select Parameters</span>
                    {defaultValue && <Select
                    name="columns"
                    options={dropdndata}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={handleSelectOptions}
                    defaultValue={defaultValue}
                          />}
                </div>
            </Selectdiv>
                <StyledTableWrapperdiv>
                    <StyledTable>
                        <thead>
                            <tr>
                                <th>Platform</th>
                                <th>Sector</th>
                                
                                {selectedOptions.map((col) => (
                                    <th key={col.parameter}>{col.parameter_display_name} <Sortbutton onClick={() => sortPlayersByValueData(col.parameter)}>{sortOrder === 'asc'?<IoIosArrowUp/>:<IoIosArrowDown/>} 
                                </Sortbutton></th>
                                ))}
                            </tr>
                        </thead>
                            {/* {data.map(row => <TableRow {...row} key={row.player} />)} */}
                            <tbody>
                                {uniqueplayers.map(player => (
                                    <tr key={player}>
                                        {checkPlayer(player)?<td className = "tableplayer" onClick={() => handleCompanyClick(player)}>{player}</td>:<td>{player}</td>}
                                        <td>{props.report}</td>

                                        {selectedOptions.map((col) => {
                                            const valueData = getValueForPlayerAndParameter(player, col.parameter, tabledata);
                                            return (
                                                <td key={col.parameter} className="data-cell">
                                                     <BarWrapper>
                                                        {valueData.graphdata?<CustomBarGraph data={valueData.graphdata} />:null}
                                                        {valueData.current_value}<Growthspan growth={valueData.growth} >{Math.round(valueData.growth)}%{valueData.growth>=0?"↑" : "↓"}</Growthspan>
                                                        <DisplayTooltip data={valueData} />
                                                    </BarWrapper>
                                                </td>
                                            );
                                        })}
                                    </tr>
                                ))}
                            </tbody>
                    </StyledTable>
                </StyledTableWrapperdiv>
                
            </Tablediv>:null}
        </DashBoarddiv>
    );
}

export default Fintable

const DashBoarddiv = styled.div`
padding:3.5vw;
padding-bottom:0px;
padding-top:15px;
`

const StyledTable = styled.table`
    /* width: 80%; */
    border-collapse: collapse;
    margin: 20px 0;
    margin-bottom:80px;
    overflow-x:auto;
    width:auto;

    th, td {
        padding: 15px;
        text-align: left;
        border: 1px solid #d1d1d1;
        width:500px;
    }

    td {
        text-align: left;
    }

    tr:first-child th, tr:first-child td {
        border-top: none;
    }

    tr:last-child th, tr:last-child td {
        border-bottom: none;
    }

    th:first-child, td:first-child {
        border-left: none;
    }

    th:last-child, td:last-child {
        border-right: none;
    }
`;

const BarWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content:space-between;
`;

const GrowthValue = styled.span`
    margin-left: 10px;
    color: ${props => props.growth >= 0 ? 'green' : 'red'};
`;

const Growthspan = styled.span`
    background-color: ${props => props.growth >= 0 ? '#d4efdc' : '#f8d2d4'};
    margin-left: 10px;
    padding:10px;
    border-radius:20px;
`
const Tablediv = styled.div`
    background-color:white;
    /* max-width: 80%;  // Adjust this based on your requirements */
    overflow-x: auto;
    /* background-color:yellow; */
`
const Selectdiv = styled.div`
    display:flex;
    justify-content:space-between;
    margin-top:20px;
    margin-right:20px;
`

const StyledTableWrapperdiv = styled.div`
    overflow-x:auto;
`

const Sortbutton = styled.button`
    border:none ;
   background-color:white;
   outline:none !important;
`