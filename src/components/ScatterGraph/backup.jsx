import React from 'react';
import { ScatterChart, Scatter, XAxis, YAxis, ReferenceLine } from 'recharts';
import Select from "react-select";
import styled from 'styled-components';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { FaCircle } from "react-icons/fa6";
import { Text } from 'recharts';
import RenderCustomShape from './RenderCustomShape';

const data = [
    { name: 'Uber', x: 25, y: 90 },
    { name: 'Zomato', x: 40, y: 65 },
];

const CustomScatterChart = () => {
    const [dropdndata, setdropdndata] = useState([])
    const [defaultValue, setDefaultValue] = useState(null);
    const [paramtypedata, setparamtypedata] = useState([])
    const [uniqueplayers, setuniqueplayers] = useState([])
    const [selectedOptions, setSelectedOptions] = useState([]);
    const [graphdata, setgraphdata] = useState([]);
    const [icondata, seticondata] = useState([]);
    const [paramx, setparamx] = useState('');
    const [paramy, setparamy] = useState('');
    const [xmax,setxmax] = useState(100);
    const [ymax, setymax]= useState(100);
    const [xmin,setxmin] = useState(100);
    const [ymin, setymin]= useState(100);
    const [month, setmonth] = useState(null)
    const [chartWidth, setChartWidth] = useState(0);
    const [colors, setColors] = useState([]);
    const [pairedcolors, setpairedColors] = useState([]);
    const [renderedDots, setRenderedDots] = useState([]);
    const [renderednameDots, setRenderednameDots] = useState([]);
    useEffect(() => {
        const updateWidth = () => {
            const width = Math.max(window.innerWidth * 0.93 + 10, 0);
            setChartWidth(width);
        };

        window.addEventListener('resize', updateWidth);
        updateWidth(); // Initial calculation

        return () => {
            window.removeEventListener('resize', updateWidth);
        };
    }, []);

    useEffect(()=>{
        const getdropdown = async() =>{
                let industry_id = 10
                if (!industry_id){
                    industry_id = 10
                }
                const response = await axios.get(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/alternateparametertype/?industry_id=${industry_id}`)
                // const ddata = await(paramdropdownservice(industry_id))
                let data = response.data
                setparamtypedata(data)
                // console.log('ddata_alt = ', data)
                let uniqueParameterTypes = [...new Set(data.map(item => item.parameter_type))];
                let newArray = uniqueParameterTypes.map(parameterType => ({
                    value: parameterType,
                    label: parameterType
                }));

                // console.log(newArray)
                
                if (newArray.length > 0) {
                    console.log(newArray[0])
                    handleSelectOptions(newArray[0], data)
                    setDefaultValue(newArray[0]);
                    // defval = newArray[0]
                  }
                setdropdndata(newArray)
              }
        getdropdown()
    },[])

    // const handleSelectOptions = ()=>{
    //     console.log('click')
    // }

    const doesOverlap = (newDot, existingDots) => {
      // Simple overlap detection logic
      for (let dot of existingDots) {
        if (Math.abs(dot.cx - newDot.cx) < dot.size / 2 + newDot.size / 2 &&
            Math.abs(dot.cy - newDot.cy) < dot.size / 2 + newDot.size / 2) {
          return true; // Overlap detected
        }
      }
      return false;
    };

    const doesTextOverlap = (newDot, existingDots, textSize = 20) => {
      // console.log('checking text overlap', existingDots)
      const estimatedWidth = textSize * newDot.name.length * 0.6; // Estimate text width
      const estimatedHeight = textSize; // Estimate text height based on font size
    
      for (let dot of existingDots) {
        const dotWidth = textSize * dot.name.length * 0.6;
        const dotHeight = textSize;
        console.log(dot.name, newDot.name,  dotWidth, estimatedWidth)
        if (Math.abs(dot.cx - newDot.cx) < (dotWidth + estimatedWidth) / 2 &&
            Math.abs(dot.cy - newDot.cy) < (dotHeight + estimatedHeight) / 2) {
          return true; // Text overlap detected
        }
      }
      return false;
    };
    
    const adjustPosition = (dot, existingDots) => {
      // Adjust the position of the dot to avoid overlap
      // This is a placeholder for the actual logic you would need to implement based on your requirements
      let newX = dot.cx;
      while (doesTextOverlap({ ...dot, cx: newX }, existingDots)) {
        newX += 10; // Move the dot down by 10 pixels to avoid overlap; adjust this logic as needed
      }
      return newX;
    };
    
    const renderCustomShape = (props) => {
        console.log('render')
        const { cx, cy, payload } = props;
        const imgSrc = payload.image
        let playerColor = payload.color
        let name = payload.name
        const textSize = 20;
        const newDot = { cx, cy, name};
        // console.log(newDot, renderedDots, textSize, renderednameDots)
        if (doesTextOverlap(newDot, renderedDots, textSize)) {
          console.log('newDotx = ', newDot.cx)
          newDot.cx = adjustPosition(newDot, renderedDots);
          console.log('adjusting....', newDot.cx)
        }
      

        // const adjustedPosition = adjustPosition({ ...payload, cx, cy }, renderedDots, textSize);
        // const newDot = { ...payload, cx: adjustedPosition.x, cy: adjustedPosition.y };
    
        
        // // Add the new, adjusted dot to the tracker
        // setRenderednameDots([...renderednameDots, newDot]);
    
    
    
        // return <image x={cx - 10} y={cy - 10} width="30px" height="50px" xlinkHref={imgSrc} />;
        // return <FaCircle x={cx - 10} y={cy - 10} width="10px" height="10px" color= {playerColor} />;
        return (
          <Text
            x={cx}
            y={cy}
            fontSize={'20px'} // Use calculated size for font size
            textAnchor="middle"
            verticalAnchor="middle"
          >
            {payload.name} 
          </Text>)
    }
    
  

    function transformData(data, param_x, param_y, iconData, pairData) {
        let maxX = -Infinity; // Initialize with the lowest possible number
        let maxY = -Infinity; // Initialize with the lowest possible number
        let minX = Infinity
        let minY = Infinity
        const iconsMap = new Map(iconData.map(icon => [icon.player_id, icon.file]));
        const transformedData = data.reduce((acc, item) => {
          let playerEntry = acc.find(entry => entry.name === item.player_name);
          let playerColor = pairData.find(val=>val[0]===item.player_name)
          if(!playerColor){
            playerColor=["","black"]
          }
          if (!playerEntry) {
            playerEntry = { name: item.player_name, x: null, y: null, image: iconsMap.get(item.player) || null, color:playerColor[1] };
            acc.push(playerEntry);
          }
      
          if (item.parameter === param_x) {
            const currentValueX = parseFloat(item.current_value);
            playerEntry.x = currentValueX;
            if (currentValueX > maxX) { // Update maxX if current value is greater
              maxX = currentValueX;
            }
            if (currentValueX < minX) {
              minX = currentValueX
            } // Update minX if current value is lower
          } else if (item.parameter === param_y) {
            const currentValueY = parseFloat(item.current_value);
            playerEntry.y = currentValueY;
            if (currentValueY > maxY) { // Update maxY if current value is greater
              maxY = currentValueY;
            }
            if (currentValueY<minY){
              minY = currentValueY
            }
          }
      
          return acc;
        }, []);
      
        return { transformedData, maxX, maxY, minX, minY };
      }

    const handleSelectOptions = async(option, paramdata = [])=>{
        // console.log('select = ', option.value)
        let arr = []
        if (paramdata.length>0){
            arr = paramdata.filter((val)=>val.parameter_type===option.value)
        }else{
            arr = paramtypedata.filter((val)=>val.parameter_type===option.value)
        }
        // console.log('arr=', arr)
        let industry_id =  window.localStorage.getItem('industry_id')
        if (!industry_id){
            industry_id = 10
        }
        setparamx(arr[0].x_parameter_name)
        setparamy(arr[0].y_parameter_name)
        let param_arr = [arr[0].xparameter, arr[0].yparameter]
        // console.log('param_arr', param_arr)
        const postData = {
            "param_arr":param_arr,
            "industry_id":industry_id
          };
        const response = await axios.post(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/parametervalues/`, postData);
        let data = response.data
        let date = new Date(data[0].start_date);
        const month = date.toLocaleString('default', { month: 'short' });
        const year = date.getFullYear().toString().slice(-2);
        let month_name = `${month}'${year}`;
        setmonth(month_name)
        // console.log('data = ', data)
        const players = new Set();
        data.forEach(entry => players.add(entry.player_name));
        let uniq_players = Array.from(players);
        // const generatedColors = Array.from({ length: uniq_players.length }, generateRandomColor);
        // const generatedColors = generateDistinctColors(uniq_players.length);
        // console.log("generated colors", generatedColors);
        let color_arr = ['rgb(143,0,255)', 'rgb(0,0,255)', 'rgb(0, 255, 0)','rgb(255, 146, 51)', 'rgb(255, 251, 0)', 'rgb(255, 0, 0)']
        const generatedColors = color_arr.slice(0, uniq_players.length);
        while (generatedColors.length < uniq_players.length) {
            generatedColors.push(generateRandomColor());
        }
        const pairedArray = uniq_players.map((player, index) => [player, generatedColors[index]]);
        setpairedColors(pairedArray)

        const playerIds = new Set();
        data.forEach(entry => playerIds.add(entry.player));
        let player_arr = JSON.stringify(Array.from(playerIds));
        const iconresponse = await axios.get(`https://api.benchmarks.digital/icons/?player_arr=${encodeURIComponent(player_arr)}`);
        let icondata = iconresponse.data
        

        let graph_data = transformData(data,arr[0].xparameter,arr[0].yparameter, icondata, pairedArray)
        setgraphdata(graph_data.transformedData)
        setxmax(graph_data.maxX)
        setymax(graph_data.maxY)
        let min_x = 0
        let min_y = 0
        if(0<=graph_data.minX<=1){
          min_x = -5
        }else if(graph_data.minX > 1){
          min_x = -1*graph_data.minX
        }else if (graph_data.minX<0){
          min_x = 2*graph_data.minX
        }

        if(0<=graph_data.minY<=1){
          min_y = -5
        }else if(graph_data.minY > 1){
          min_y = -1*graph_data.minY
        }else if (graph_data.minY<0){
          min_y = 2*graph_data.minY
        }
        setxmin(min_x)
        setymin(min_y)

        
        setuniqueplayers(uniq_players)
        setSelectedOptions(arr)

        // console.log('icondata=',icondata)
        seticondata(icondata)

    }

    const generateRandomColor = () => {
      const red = Math.floor(Math.random() * 256);
      const green = Math.floor(Math.random() * 256);
      const blue = Math.floor(Math.random() * 256);
      return `rgb(${red}, ${green}, ${blue})`;
    };

    const generateDistinctColors = (numColors) => {
      const colors = [];
      const step = Math.floor(256 / numColors);
    
      for (let i = 0; i < numColors; i++) {
        const red = (step * i) % 256;
        const green = (step * (i + 1)) % 256;
        const blue = (step * (i + 2)) % 256;
        colors.push(`rgb(${red}, ${green}, ${blue})`);
      }
    
      return colors;
    };

    return(
        <div style={{margin:'3.5vw',marginTop:'15px', paddingTop:'10px', background:'white'}}>

            <Selectdiv>
            <div style={{marginLeft:'15px'}}>For the month of {month}</div>

                <div  style={{display:'flex', alignItems:'center'}}>
                  <span style={{'lineHeight':'38px', marginRight:'20px'}}>Select Parameters</span>
                  {defaultValue && <Select
                  name="columns"
                  options={dropdndata}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={handleSelectOptions}
                  defaultValue={defaultValue}
                        />}
                </div>
            </Selectdiv>
            {/* <LegendDiv>
              {pairedcolors.map((val, i)=>{
                return(
                  <span style={{'marginRight':'30px'}}><FaCircle color={val[1]}/><span style={{'marginLeft':'5px'}}>{val[0]}</span></span>
                )
              })}
            </LegendDiv> */}
            <ScatterChartWrapperdiv>
                <ScatterChart width={chartWidth} height={600}   margin={{ top: 20, right: 50, bottom: 30, left: 20 }}>
                    <ReferenceLine x={(-5+xmax+0.2*(xmax))/2} stroke="black" strokeDasharray="3 3" />
                    <ReferenceLine y={(-5+ymax+0.2*(ymax))/2} stroke="black" strokeDasharray="3 3" />
                    <XAxis dataKey="x" type="number" domain={[-5, Math.round((xmax+0.2*(xmax))*100)/100]} allowDataOverflow={true}  label={{ value: paramx, position: 'insideBottom',offset:-10 }} />
                    <YAxis dataKey="y" type="number" domain={[-5, Math.round((ymax+0.2*(ymax))*100)/100]} allowDataOverflow={true} label={{ value: paramy, angle: -90, position: 'insideLeft' }}/>
                    <Scatter data={graphdata}  shape={renderCustomShape}/>
                </ScatterChart>
            </ScatterChartWrapperdiv>
        </div>
    )
};

export default CustomScatterChart;

const Selectdiv = styled.div`
    display:flex;
    justify-content:space-between;
    margin-top:2px;
    margin-right:20px;
`

const ScatterChartWrapperdiv = styled.div`
    display:flex;
    align-items:center;
    justify-content:center;
    background-color:white;
`

const LegendDiv = styled.div`
  margin-left:70px;
`