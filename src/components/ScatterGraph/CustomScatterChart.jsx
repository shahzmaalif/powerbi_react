import React from 'react';
import { ScatterChart, Scatter, XAxis, YAxis, ReferenceLine } from 'recharts';
import Select from "react-select";
import styled from 'styled-components';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { FaCircle } from "react-icons/fa6";
import { Text } from 'recharts';
import { useRef } from 'react';
import RenderCustomShape from './RenderCustomShape';
import SChart from './SChart';
import { FaBullseye } from 'react-icons/fa';


const data = [
    { name: 'Uber', x: 25, y: 90 },
    { name: 'Zomato', x: 40, y: 65 },
];

const CustomScatterChart = () => {
    const [dropdndata, setdropdndata] = useState([])
    const [zdropdndata, setzdropdndata] = useState([])
    const [zselect, setzselect] = useState([])
    const [zdefault, setzdefault] = useState([])
    const [xdropdndata, setxdropdndata] = useState([])
    const [ydropdndata, setydropdndata] = useState([])
    const [xselected, setxselected] = useState(false)
    const [yselected, setyselected] = useState(false)
    const [defaultValue, setDefaultValue] = useState(null);
    const [paramtypedata, setparamtypedata] = useState([])
    const [zparamtypedata, setzparamtypedata] = useState([])
    const [xparamtypedata, setxparamtypedata] = useState([])
    const [yparamtypedata, setyparamtypedata] = useState([])
    const [showCustom, setshowCustom] = useState(false)
    const [uniqueplayers, setuniqueplayers] = useState([])
    const [selectedOptions, setSelectedOptions] = useState([]);
    const [graphdata, setgraphdata] = useState([]);
    const [xgraphdata, setxgraphdata] = useState([]);
    const [ygraphdata, setygraphdata] = useState([]);
    const [icondata, seticondata] = useState([]);
    const [paramx, setparamx] = useState('');
    const [paramy, setparamy] = useState('');
    const [paramxID, setparamxID] = useState('');
    const [paramyID, setparamyID] = useState('');
    // const [xmax,setxmax] = useState(100);
    // const [ymax, setymax]= useState(100);
    // const [xmin,setxmin] = useState(100);
    // const [ymin, setymin]= useState(100);
    const [month, setmonth] = useState(null)
    const [chartWidth, setChartWidth] = useState(0);
    const [colors, setColors] = useState([]);
    const [pairedcolors, setpairedColors] = useState([]);
    const [renderedDots, setRenderedDots] = useState([]);
    const [renderednameDots, setRenderednameDots] = useState([]);
    const [graphdataUpdated, setGraphdataUpdated] = useState(false);
    const [showLog, setShowLog] = useState(false);

    const refxmax = useRef(null);
    const refymax = useRef(null);
    const refxmin = useRef(null);
    const refymin = useRef(null);
    const chartRef = useRef();
    const xpx = useRef();
    const ypx = useRef();
    const xwdpx= useRef();



    useEffect(() => {
        const updateWidth = () => {
            const width = Math.max(window.innerWidth * 0.93 + 10, 0);
            setChartWidth(width);
        };

        window.addEventListener('resize', updateWidth);
        updateWidth(); // Initial calculation

        return () => {
            window.removeEventListener('resize', updateWidth);
        };
    }, []);

    useEffect(() => {
      if (graphdataUpdated) { // You'll need a state variable to track if graphdata has been updated
        handleSelectZOptions(zdropdndata[0], zparamtypedata).then(() => {
          setGraphdataUpdated(false); // Set it back to false after handling
        });
      }
    }, [graphdataUpdated, zdropdndata, zparamtypedata]); 

    useEffect(()=>{
        const getdropdown = async() =>{
                let industry_id = window.localStorage.getItem('industry_id')
                if (!industry_id){
                    industry_id = 10
                }
                const response = await axios.get(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/alternateparametertype/?industry_id=${industry_id}`)
                const zresponse = await axios.get(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/zparams/?industry_id=${industry_id}`)
                const xresponse = await axios.get(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/xparams/?industry_id=${industry_id}`)
                const yresponse = await axios.get(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/yparams/?industry_id=${industry_id}`)
                // const ddata = await(paramdropdownservice(industry_id))
                let data = response.data
                let zdata = zresponse.data
                let xdata = xresponse.data
                let ydata = yresponse.data
                setxparamtypedata(xdata)
                setyparamtypedata(ydata)
                setparamtypedata(data)
                setzparamtypedata(zdata)
                let uniqueParameterTypes = [...new Set(data.map(item => item.parameter_type))];
                let newArray = uniqueParameterTypes.map(parameterType => ({
                    value: parameterType,
                    label: parameterType
                }));

                let uniqueZParameterTypes = [...new Set(zdata.map(item => item.parameter_type))];
                let newZArray = uniqueZParameterTypes.map(parameterType => ({
                    value: parameterType,
                    label: parameterType
                }));
                let uniqueXParameterTypes = [...new Set(xdata.map(item => item.parameter_type))];
                let newXArray = uniqueXParameterTypes.map(parameterType => ({
                    value: parameterType,
                    label: parameterType
                }));

                let uniqueYParameterTypes = [...new Set(ydata.map(item => item.parameter_type))];
                let newYArray = uniqueYParameterTypes.map(parameterType => ({
                    value: parameterType,
                    label: parameterType
                }));

                setxdropdndata(newXArray)
                setydropdndata(newYArray)
                setzdropdndata(newZArray)

                console.log('zarr =  ', newZArray)

                
                if (newArray.length > 0) {
                    await handleSelectOptions(newArray[0], data)
                    console.log(newArray[0])
                    setDefaultValue(newArray[0]);
                    // defval = newArray[0]
                  }
                  newArray.push({value:'Custom',
                    label: 'Custom'})
                setdropdndata(newArray)

                if (newZArray.length > 0) {
                  console.log(newZArray[0])
                  // handleSelectZOptions(newZArray[0], zdata)
                  setzdefault(newZArray[0]);
                }
              }
        getdropdown()
    },[])

    useEffect(() => {
      if (chartRef.current) {
        // Get the internal state of the chart, which includes the scale functions
        const { xAxisMap, yAxisMap } = chartRef.current.state;
        

        if(xAxisMap && yAxisMap){
          console.log('xAxisMap = ',xAxisMap, yAxisMap)
        const xAxis = xAxisMap[0];
        const yAxis = yAxisMap[0];
        xwdpx.current = xAxis.width
        // Get the scale functions
        const xScale = xAxis.scale;
        const yScale = yAxis.scale;
  
        // Convert pixel to data value
        const pixelX = 100; // Example pixel value for x
        const pixelY = 100; // Example pixel value for y
        let dataX = xScale.invert(pixelX-100);
        let dataY = yScale.invert(pixelY-20);
        if (dataX<0){
          dataX = -dataX
        }
        if (dataY<0){
          dataY = -dataY
        }

        xpx.current = pixelX
        ypx.current = pixelY
  
        console.log(`Data X value for pixel X ${pixelX}: ${dataX}`);
        console.log(`Data Y value for pixel Y ${pixelY}: ${dataY}`);
        }
       
      }
    }, [graphdata]);


    // const doesOverlap = (newDot, existingDots) => {
    //   // Simple overlap detection logic
    //   for (let dot of existingDots) {
    //     if (Math.abs(dot.cx - newDot.cx) < dot.size / 2 + newDot.size / 2 &&
    //         Math.abs(dot.cy - newDot.cy) < dot.size / 2 + newDot.size / 2) {
    //       return true; // Overlap detected
    //     }
    //   }
    //   return false;
    // };

    // const doesTextOverlap = (newDot, existingDots, textSize = 20) => {
    //   console.log('checking text overlap', existingDots)
    //   const estimatedWidth = textSize * newDot.name.length * 0.6; // Estimate text width
    //   const estimatedHeight = textSize; // Estimate text height based on font size
    
    //   for (let dot of existingDots) {
    //     const dotWidth = textSize * dot.name.length * 0.6;
    //     const dotHeight = textSize;
    //     console.log(dot.name, newDot.name,  dotWidth, estimatedWidth)
    //     if (Math.abs(dot.cx - newDot.cx) < (dotWidth + estimatedWidth) / 2 &&
    //         Math.abs(dot.cy - newDot.cy) < (dotHeight + estimatedHeight) / 2) {
    //       return true; // Text overlap detected
    //     }
    //   }
    //   return false;
    // };
    
    // const adjustPosition = (dot, existingDots) => {
    //   // Adjust the position of the dot to avoid overlap
    //   // This is a placeholder for the actual logic you would need to implement based on your requirements
    //   let newX = dot.cx;
    //   while (doesTextOverlap({ ...dot, cx: newX }, existingDots)) {
    //     newX += 10; // Move the dot down by 10 pixels to avoid overlap; adjust this logic as needed
    //   }
    //   return newX;
    // };
    
    const renderCustomShape = (props) => {
        console.log('render')
        const { cx, cy, payload } = props;
        const imgSrc = payload.image
        let playerColor = payload.color
        let name = payload.name
        const textSize = payload.size;
        const newDot = { cx, cy, name};
        // console.log(newDot, renderedDots, textSize, renderednameDots)
        // if (doesTextOverlap(newDot, renderedDots, textSize)) {
        //   console.log('newDotx = ', newDot.cx)
        //   newDot.cx = adjustPosition(newDot, renderedDots);
        //   console.log('adjusting....', newDot.cx)
        // }
      

        // const adjustedPosition = adjustPosition({ ...payload, cx, cy }, renderedDots, textSize);
        // const newDot = { ...payload, cx: adjustedPosition.x, cy: adjustedPosition.y };
    
        
        // // Add the new, adjusted dot to the tracker
        // setRenderednameDots([...renderednameDots, newDot]);
    
    
    
        // return <image x={cx - 10} y={cy - 10} width="30px" height="50px" xlinkHref={imgSrc} />;
        // return <FaCircle x={cx - 10} y={cy - 10} width="10px" height="10px" color= {playerColor} />;
        return (
          <Text
            x={cx}
            y={cy}
            fontSize={payload.size.height} // Use calculated size for font size
            textAnchor="middle"
            verticalAnchor="middle"
            fontWeight="bold"
          >
            {payload.name} 
          </Text>)
    }
    
    const estimateTextSize = (name, fontSize) => ({ width: name.length * fontSize * 0.45, height: fontSize });

    const resolveOverlaps = (data, maxX = refxmax.current, maxY = refymax.current, minX = refxmin.current, minY = refymin.current) => {
      let xrange = maxX - minX;
      let yrange = maxY - minY;
      let effectivehtpx = 520; // You should use a dynamic or passed-in value here
      let effectivewidthpx = xwdpx.current; // You should use a dynamic or passed-in value here
      let pxtoxmultiplier = effectivewidthpx / xrange;
      let pxtoymultiplier = effectivehtpx / yrange;
    
      let yblockheightpx = (520 / 10); // You should use a dynamic or passed-in value here
      let xblockwidthpx = ((chartWidth - 140) / 50); // You should use a dynamic or passed-in value here
      let yblockheighty = yrange / yblockheightpx;
      let xblockwidthx = xrange / xblockwidthpx;
    
      const adjustedData = [...data]; // Create a copy to avoid directly mutating the input
      let overlap;
      const maxIterations = 100; // Set a sensible limit to iterations
      let iterations = 0;
    
      do {
        overlap = false;
        iterations++; // Increment the number of iterations
        if (iterations >= maxIterations) {
          console.log('Maximum iterations reached, some overlaps may still exist.');
          break; // Exit the loop if the maximum iterations is reached
        }
        for (let i = 0; i < adjustedData.length; i++) {
          for (let j = i + 1; j < adjustedData.length; j++) {
            let dx = adjustedData[j].x - adjustedData[i].x;
            let dy = adjustedData[j].y - adjustedData[i].y;
            let requiredDistanceX = xblockwidthx; // Assuming this is the minimum x distance to not overlap
            let requiredDistanceY = yblockheighty; // Assuming this is the minimum y distance to not overlap
    
            if (Math.abs(dx) < requiredDistanceX && Math.abs(dy) < requiredDistanceY) {
              overlap = true; // Set overlap to true to indicate that we need another iteration
              let adjustX = (requiredDistanceX - Math.abs(dx)) / 2;
              let adjustY = (requiredDistanceY - Math.abs(dy)) / 2;
    
              // Determine direction of adjustment
              if (dx > 0) { // If j is to the right of i, push j further right and i further left
                adjustedData[i].x -= adjustX;
                adjustedData[j].x += adjustX;
              } else { // If j is to the left of i, push j further left and i further right
                adjustedData[i].x += adjustX;
                adjustedData[j].x -= adjustX;
              }
    
              if (dy > 0) { // If j is below i, push j further down and i further up
                adjustedData[i].y -= adjustY;
                adjustedData[j].y += adjustY;
              } else { // If j is above i, push j further up and i further down
                adjustedData[i].y += adjustY;
                adjustedData[j].y -= adjustY;
              }
            }
          }
        }
      } while (overlap);
    
      return adjustedData;
    };

    // const resolveOverlaps = (data,maxX=refxmax.current, maxY=refymax.current, minX=refxmin.current, minY=refymin.current) => {
    //   // const adjustedData = data.map(item => ({
    //   //   ...item,
    //   //   size: estimateTextSize(item.name, item.size.height), // Add text size estimation
    //   // }));
    //   console.log('resolvedata = ', data, xwdpx.current, chartWidth-130)
    //   let xrange = maxX - minX;
    //   let yrange = maxY - minY;
    //   let effectivehtpx = 520
    //   let effectivewidthpx = xwdpx.current
    //   let pxtoxmultiplier = effectivewidthpx/xrange
    //   let pxtoymultiplier = effectivehtpx/yrange


    //   let yblockheightpx = (520/10)
    //   let xblockwidthpx = ((chartWidth-140)/50)
    //   let yblockheighty = yrange/yblockheightpx
    //   let xblockwidthx = xrange/xblockwidthpx

    //   const adjustedData = data
    //   let overlap;
    //   do {
    //     overlap = false;
        
    //     for (let i = 0; i < adjustedData.length; i++) {
    //       for (let j = i + 1; j < adjustedData.length; j++) {
    //         let dx = adjustedData[j].x - adjustedData[i].x;
    //         let dy = adjustedData[j].y - adjustedData[i].y;
    //         if (dx < 0) {
    //           dx = -dx;
    //         }
    //         if (dy < 0) {
    //           dy = -dy;
    //         }
    //         // const distance = Math.sqrt(dx * dx + dy * dy);
    
    //         // Estimate required distance to prevent overlap (considering some padding)
    //         let requiredDistance = (adjustedData[i].size.width*(pxtoxmultiplier) + adjustedData[j].size.width*(pxtoxmultiplier)) / 2;
    //         let requiredDistancey = (adjustedData[i].size.height*(pxtoymultiplier) + adjustedData[j].size.height*(pxtoymultiplier)) / 2;
    //         requiredDistance = xblockwidthx
    //         requiredDistancey = yblockheighty

    //         if (dx< requiredDistance && dy < requiredDistancey) {
    //           console.log(adjustedData[i].name, adjustedData[j].name, dx, requiredDistance, dy, requiredDistancey)
    //           // overlap = true;
    //           // // Adjust positions to resolve overlap
    //           // // const adjust = (requiredDistance - distance) / 2;
    //           const adjustx = (requiredDistance-dx)/2
    //           const adjusty = (requiredDistancey-dy)/2
    //           // check for x value is positive
    //           adjustedData[i].x -= adjustx;
    //           adjustedData[j].x += adjustx;
    //           adjustedData[i].y -= adjusty;
    //           adjustedData[j].y += adjusty;
    //         }
    //       }
    //     }
    //   } while (overlap);
    //   console.log('adjustedData = ', adjustedData)
    //   return adjustedData; // Return adjusted data without size info
    // };

    // const resolveOverlaps = (data, maxX = refxmax.current, maxY = refymax.current, minX = refxmin.current, minY = refymin.current) => {
    //   console.log('resolvedata = ', data, xwdpx.current, chartWidth-130)
    
    //   // Define the grid size
    //   const gridCols = 50;  // Define how many columns you want in the grid
    //   const gridRows = 10;  // Define how many rows you want in the grid
    
    //   let xrange = maxX - minX;
    //   let yrange = maxY - minY;
    
    //   let effectivehtpx = 520;
    //   let effectivewidthpx = xwdpx.current;
    //   let pxtoxmultiplier = effectivewidthpx / xrange;
    //   let pxtoymultiplier = effectivehtpx / yrange;
    
    //   // Define block sizes in pixels
    //   let xblockwidthpx = (chartWidth - 140) / gridCols;
    //   let yblockheightpx = 520 / gridRows;
    
    //   // Define block sizes in the chart's coordinate system
    //   let xblockwidthx = xrange / gridCols;
    //   let yblockheighty = yrange / gridRows;
    
    //   const adjustedData = data.map(item => ({
    //     ...item,
    //     gridX: Math.floor((item.x - minX) / xblockwidthx), // Calculate grid X position
    //     gridY: Math.floor((item.y - minY) / yblockheighty), // Calculate grid Y position
    //   }));

    //   console.log('addata = ', adjustedData)
    
    //   // Create a 2D array to track the grid's occupation status
    //   const grid = Array.from({ length: gridRows }, () => Array(gridCols).fill(false));
    
    //   adjustedData.forEach(item => {
    //     let x = item.gridX;
    //     let y = item.gridY;
    //     console.log(item, x, y, grid[x][y])
    //     // Check if the current grid position is occupied
    //     if (grid[y][x]!==undefined) {
    //       while (grid[y][x]) {
    //         // Move to the next grid block in a clockwise direction
    //         if (x < gridCols - 1 && !grid[y][x + 1]) { // move right
    //           x++;
    //         } else if (y < gridRows - 1 && !grid[y + 1][x]) { // move down
    //           y++;
    //         } else if (x > 0 && !grid[y][x - 1]) { // move left
    //           x--;
    //         } else if (y > 0 && !grid[y - 1][x]) { // move up
    //           y--;
    //         } else {
    //           // If there's no available space, keep the original position (or handle as needed)
    //           console.error('No available space to resolve overlap for item:', item.name);
    //           break;
    //         }
    //       }
    //     }
    
    //     // Update the item's position in the chart's coordinate system based on the new grid position
    //     item.x = minX + x * xblockwidthx + xblockwidthx / 2; // Center in the grid block
    //     item.y = minY + y * yblockheighty + yblockheighty / 2; // Center in the grid block
    
    //     // Mark this grid position as occupied
    //     grid[y][x] = true;
    //   });
    
    //   console.log('adjustedData = ', adjustedData);
    //   return adjustedData; // Return adjusted data
    // };

    function transformData(data, param_x, param_y, iconData = [], pairData) {
        let maxX = -Infinity; // Initialize with the lowest possible number
        let maxY = -Infinity; // Initialize with the lowest possible number
        let minX = Infinity
        let minY = Infinity
        // const iconsMap = new Map(iconData.map(icon => [icon.player_id, icon.file]));
        const transformedData = data.reduce((acc, item) => {
          let playerEntry = acc.find(entry => entry.name === item.player_name);
          let playerColor = pairData.find(val=>val[0]===item.player_name)
          if(!playerColor){
            playerColor=["","black"]
          }
          if (!playerEntry) {
            playerEntry = { name: item.player_name, x: null, y: null, image: null, color:playerColor[1] };
            acc.push(playerEntry);
          }
      
          if (item.parameter === param_x) {
            const currentValueX = parseFloat(item.current_value);
            playerEntry.x = currentValueX;
            if (currentValueX > maxX) { // Update maxX if current value is greater
              maxX = currentValueX;
            }
            if (currentValueX < minX) {
              minX = currentValueX
            } // Update minX if current value is lower
          } else if (item.parameter === param_y) {
            const currentValueY = parseFloat(item.current_value);
            playerEntry.y = currentValueY;
            if (currentValueY > maxY) { // Update maxY if current value is greater
              maxY = currentValueY;
            }
            if (currentValueY<minY){
              minY = currentValueY
            }
          }
      
          return acc;
        }, []);
      
        return { transformedData, maxX, maxY, minX, minY };
      }
    
    function transformIndiData(data, param_x, param_y) {
      let maxX = -Infinity; // Initialize with the lowest possible number
      let maxY = -Infinity; // Initialize with the lowest possible number
      let minX = Infinity
      let minY = Infinity
      console.log('datainid = ', data)
      const transformedData = data.reduce((acc, item) => {
        let playerEntry = acc.find(entry => entry.name === item.player_name);
        if (!playerEntry) {
          playerEntry = { name: item.player_name, x: null, y: null};
          acc.push(playerEntry);
        }
        console.log('matching',item.parameter, param_x, param_y)
        if (item.parameter === param_x) {
          const currentValueX = parseFloat(item.current_value);
          playerEntry.x = currentValueX;
          if (currentValueX > maxX) { // Update maxX if current value is greater
            maxX = currentValueX;
          }
          if (currentValueX < minX) {
            minX = currentValueX
          } // Update minX if current value is lower
        } else if (item.parameter === param_y) {
          const currentValueY = parseFloat(item.current_value);
          playerEntry.y = currentValueY;
          if (currentValueY > maxY) { // Update maxY if current value is greater
            maxY = currentValueY;
          }
          if (currentValueY<minY){
            minY = currentValueY
          }
        }
    
        return acc;
      }, []);
    
      return { transformedData, maxX, maxY, minX, minY };
    }


    const handleSelectOptions = async(option, paramdata = [])=>{
        setzselect(zdefault)
        if(option.value ==='Custom'){
          console.log('Click')
          setshowCustom(true)
          return
        }else{
          setshowCustom(false)
          setxselected(false)
          setyselected(false)
          setxgraphdata([])
          setygraphdata([])
        }

        let arr = []
        if (paramdata.length>0){
            arr = paramdata.filter((val)=>val.parameter_type===option.value)
        }else{
            arr = paramtypedata.filter((val)=>val.parameter_type===option.value)
        }
        // console.log('arr=', arr)
        let industry_id =  window.localStorage.getItem('industry_id')
        if (!industry_id){
            industry_id = 10
        }
        setparamx(arr[0].x_parameter_name)
        setparamy(arr[0].y_parameter_name)
        let param_arr = [arr[0].xparameter, arr[0].yparameter]
        // console.log('param_arr', param_arr)
        const postData = {
            "param_arr":param_arr,
            "industry_id":industry_id
          };
        console.log('select = ', option.value)
        const response = await axios.post(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/parametervalues/`, postData);
        let data = response.data
        let date = new Date(data[0].start_date);
        const month = date.toLocaleString('default', { month: 'short' });
        const year = date.getFullYear().toString().slice(-2);
        let month_name = `${month}'${year}`;
        setmonth(month_name)
        // console.log('data = ', data)
        const players = new Set();
        data.forEach(entry => players.add(entry.player_name));
        let uniq_players = Array.from(players);
        // const generatedColors = Array.from({ length: uniq_players.length }, generateRandomColor);
        // const generatedColors = generateDistinctColors(uniq_players.length);
        // console.log("generated colors", generatedColors);
        let color_arr = ['rgb(143,0,255)', 'rgb(0,0,255)', 'rgb(0, 255, 0)','rgb(255, 146, 51)', 'rgb(255, 251, 0)', 'rgb(255, 0, 0)']
        const generatedColors = color_arr.slice(0, uniq_players.length);
        while (generatedColors.length < uniq_players.length) {
            generatedColors.push(generateRandomColor());
        }
        const pairedArray = uniq_players.map((player, index) => [player, generatedColors[index]]);
        setpairedColors(pairedArray)

        const playerIds = new Set();
        data.forEach(entry => playerIds.add(entry.player));
        let player_arr = JSON.stringify(Array.from(playerIds));
        // const iconresponse = await axios.get(`https://api.benchmarks.digital/icons/?player_arr=${encodeURIComponent(player_arr)}`);
        // let icondata = iconresponse.data
        
        console.log('originaldata = ', data)
        let graph_data = transformData(data,arr[0].xparameter,arr[0].yparameter, [], pairedArray)
        // check for  overlaps in graph_data.transformedData
        console.log('graph_data = ', graph_data)
        let adjustData = graph_data.transformedData.map(item => ({
          ...item,
          size: estimateTextSize(item.name, 10), // Add text size estimation
        }));


        adjustData= resolveOverlaps(adjustData, graph_data.maxX, graph_data.maxY, graph_data.minX, graph_data.minY);   
        console.log('adjustedGraphData = ', adjustData)
        
        let items = adjustData
        const minItemX = adjustData.reduce((min, item) => item.x < min.x ? item : min, items[0]);
        const minItemY = adjustData.reduce((min, item) => item.y < min.y ? item : min, items[0]);

        const maxItemX = adjustData.reduce((max, item) => item.x > max.x ? item : max, items[0]);
        const maxItemY = adjustData.reduce((max, item) => item.y > max.y ? item : max, items[0]);
        
        console.log(minItemX, minItemY, maxItemX, maxItemY)

        setgraphdata(adjustData)
        // setxmax(graph_data.maxX)
        // setymax(graph_data.maxY)
        // setxmax(maxItemX.x)
        // setymax(maxItemY.y)
        refxmax.current = maxItemX.x+0.2*maxItemX.x
        refymax.current = maxItemY.y+0.2*maxItemY.y
        let min_x = 0
        let min_y = 0

        if(0===minItemX.x){
          min_x = 0
        }else{
          min_x = minItemX.x-0.2*minItemX.x
        }

        if(0===minItemY.y){
          min_y = 0
        }else{
          min_y = minItemY.y-0.2*minItemY.y
        }

        // if(0<=graph_data.minX){
        //   min_x = 0
        // }else{
        //   min_x = graph_data.minX+0.2*graph_data.minX
        // }

        // if(0<=graph_data.minY){
        //   min_y = 0
        // }else{
        //   min_y = graph_data.minY+0.2*graph_data.minY
        // }
        // setxmin(min_x)
        // setymin(min_y)
        refxmin.current = min_x
        refymin.current = min_y
        setGraphdataUpdated(true);
        
    }

    const handleSelectZOptions = async(option, paramdata = [])=>{
      setzselect(option)
      let arr = []
      if (paramdata.length>0){
          arr = paramdata.filter((val)=>val.parameter_type===option.value)
      }else{
          arr = zparamtypedata.filter((val)=>val.parameter_type===option.value)
      }

      let industry_id =  window.localStorage.getItem('industry_id')
      if (!industry_id){
          industry_id = 10
      }

      let param_arr = [arr[0].zparameter]
      const postData = {
          "param_arr":param_arr,
          "industry_id":industry_id
        };
      
      const response = await axios.post(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/parametervalues/`, postData);
      let data = response.data
      data.sort((a,b)=>(parseFloat(a.current_value)-parseFloat(b.current_value)))
      let minval = data[0].current_value
      let maxval = data[data.length-1].current_value
      console.log('zdata = ',data, maxval, minval, maxval/minval, parseFloat(maxval/minval)<15)
      let heightMap = {};
      let currentHeight = 10; // Starting height
      data.forEach((item,i) => {
        item.name = item.player_name;
        if(parseFloat(maxval/minval)<12 && 1<parseFloat(maxval/minval)){
          currentHeight = 10+(item.current_value/minval)
          setShowLog(false)
        }else{
          currentHeight = 10+Math.log(item.current_value/minval);
          setShowLog(true)
        }
        heightMap[item.player_name] = currentHeight;
        // console.log(item.name, currentHeight, item.current_value, Math.log(item.current_value));
      });
      let graph_data = graphdata
      let newGraphData = graphdata.map(item => {
        if (heightMap[item.name] !== undefined) {
          return {
            ...item,
            size: estimateTextSize(item.name, heightMap[item.name])
          };
        }
        return item;
      });
      console.log('prev = ', graph_data)
      console.log('next = ', newGraphData)
      let newadjustedGraphData = newGraphData
      newadjustedGraphData = resolveOverlaps(newGraphData); 
      // Update the state with the new graph data array
      setgraphdata(newadjustedGraphData);

    }

    const handleSelectOptionsX = async(option, paramdata = [])=>{
      console.log('click')
      let arr = []
      if (paramdata.length>0){
        console.log('paramdata = ', paramdata)
        arr = paramdata.filter((val)=>val.parameter_type===option.value)
    }else{
        arr = xparamtypedata.filter((val)=>val.parameter_type===option.value)
    }
    console.log('xarr = ',arr)
    setparamx(arr[0].x_parameter_name)
    setparamxID(arr[0].xparameter)
    let industry_id =  window.localStorage.getItem('industry_id')
    if (!industry_id){
        industry_id = 10
    }

    let param_arr = [arr[0].xparameter]
    const postData = {
        "param_arr":param_arr,
        "industry_id":industry_id
      };
    const response = await axios.post(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/parametervalues/`, postData);
    let data = response.data
    console.log('datax = ', data)
    setxgraphdata(data)
    if(data && ygraphdata.length>0){
      let wholedata = data.concat(ygraphdata)
      let graph_data = transformIndiData(wholedata,arr[0].xparameter, paramyID)
      console.log('xgraphdata = ', graph_data)
      let adjustData = graph_data.transformedData.map(item => ({
        ...item,
        size: estimateTextSize(item.name, 10), // Add text size estimation
      }));
      let adjustedGraphData = adjustData
      adjustedGraphData = resolveOverlaps(adjustData, graph_data.maxX, graph_data.maxY, graph_data.minX, graph_data.minY);        


      console.log('graph_data = ', graph_data)
      setgraphdata(adjustedGraphData)
      let items = adjustData
        const minItemX = adjustData.reduce((min, item) => item.x < min.x ? item : min, items[0]);
        const minItemY = adjustData.reduce((min, item) => item.y < min.y ? item : min, items[0]);

        const maxItemX = adjustData.reduce((max, item) => item.x > max.x ? item : max, items[0]);
        const maxItemY = adjustData.reduce((max, item) => item.y > max.y ? item : max, items[0]);
        
        console.log(minItemX, minItemY, maxItemX, maxItemY)

        // setxmax(graph_data.maxX)
        // setymax(graph_data.maxY)
        // setxmax(maxItemX.x)
        // setymax(maxItemY.y)
        refxmax.current = maxItemX.x+0.2*maxItemX.x
        refymax.current = maxItemY.y+0.2*maxItemY.y
        let min_x = 0
        let min_y = 0

        if(0===minItemX.x){
          min_x = 0
        }else{
          min_x = minItemX.x-0.2*minItemX.x
        }

        if(0===minItemY.y){
          min_y = 0
        }else{
          min_y = minItemY.y-0.2*minItemY.y
        }

      // setxmax(graph_data.maxX)
      // setymax(graph_data.maxY)
      // let min_x = 0
      // let min_y = 0
      // if(0<=graph_data.minX){
      //   min_x = 0
      // }else{
      //   min_x = graph_data.minX+0.2*graph_data.minX
      // }

      // if(0<=graph_data.minY){
      //   min_y = 0
      // }else{
      //   min_y = graph_data.minY+0.2*graph_data.minY
      // }
      // setxmin(min_x)
      // setymin(min_y)
      refxmin.current = min_x
      refymin.current = min_y
      setSelectedOptions(arr)
    }
    }


    const handleSelectOptionsY = async(option, paramdata = []) =>{
      let arr = []
      if (paramdata.length>0){
        arr = paramdata.filter((val)=>val.parameter_type===option.value)
    }else{
        arr = yparamtypedata.filter((val)=>val.parameter_type===option.value)
    }
      console.log('ydata = ', arr)
      setparamy(arr[0].y_parameter_name)
      setparamyID(arr[0].yparameter)
      if (paramdata.length>0){
        arr = paramdata.filter((val)=>val.parameter_type===option.value)
    }else{
        arr = yparamtypedata.filter((val)=>val.parameter_type===option.value)
    }

    let industry_id =  window.localStorage.getItem('industry_id')
    if (!industry_id){
        industry_id = 10
    }

    let param_arr = [arr[0].yparameter]
    const postData = {
        "param_arr":param_arr,
        "industry_id":industry_id
      };
    const response = await axios.post(`${process.env.REACT_APP_WEBFORM_ENDPOINT}/parametervalues/`, postData);
    let data = response.data
    setygraphdata(data)
    console.log('datay = ', data, xgraphdata)
    if(data && xgraphdata.length>0){
      let wholedata = data.concat(xgraphdata)
      console.log('wholedata = ', wholedata,paramxID, arr[0].yparameter)
      let graph_data = transformIndiData(wholedata,paramxID, arr[0].yparameter)
      console.log('ygraphdata = ', graph_data)
      let adjustData = graph_data.transformedData.map(item => ({
        ...item,
        size: estimateTextSize(item.name, 10), // Add text size estimation
      }));
      let adjustedGraphData = adjustData;
      adjustedGraphData = resolveOverlaps(adjustData, graph_data.maxX, graph_data.maxY, graph_data.minX, graph_data.minY);        


      console.log('graph_data = ', graph_data)
      setgraphdata(adjustedGraphData)

        let items = adjustData
        const minItemX = adjustData.reduce((min, item) => item.x < min.x ? item : min, items[0]);
        const minItemY = adjustData.reduce((min, item) => item.y < min.y ? item : min, items[0]);

        const maxItemX = adjustData.reduce((max, item) => item.x > max.x ? item : max, items[0]);
        const maxItemY = adjustData.reduce((max, item) => item.y > max.y ? item : max, items[0]);
        
        console.log(minItemX, minItemY, maxItemX, maxItemY)

        // setxmax(graph_data.maxX)
        // setymax(graph_data.maxY)
        // setxmax(maxItemX.x)
        // setymax(maxItemY.y)
        refxmax.current = maxItemX.x+0.2*maxItemX.x
        refymax.current = maxItemY.y+0.2*maxItemY.y
        let min_x = 0
        let min_y = 0

        if(0===minItemX.x){
          min_x = 0
        }else{
          min_x = minItemX.x-0.2*minItemX.x
        }

        if(0===minItemY.y){
          min_y = 0
        }else{
          min_y = minItemY.y-0.2*minItemY.y
        }

      // setxmax(graph_data.maxX)
      // setymax(graph_data.maxY)
      // let min_x = 0
      // let min_y = 0
      // if(0<=graph_data.minX){
      //   min_x = 0
      // }else{
      //   min_x = graph_data.minX+0.2*graph_data.minX
      // }

      // if(0<=graph_data.minY){
      //   min_y = 0
      // }else{
      //   min_y = graph_data.minY+0.2*graph_data.minY
      // }
      // setxmin(min_x)
      // setymin(min_y)
      refxmin.current = min_x
      refymin.current = min_y
      setGraphdataUpdated(true)
      setSelectedOptions(arr)
    }
    }
      

    const generateRandomColor = () => {
      const red = Math.floor(Math.random() * 256);
      const green = Math.floor(Math.random() * 256);
      const blue = Math.floor(Math.random() * 256);
      return `rgb(${red}, ${green}, ${blue})`;
    };

    // const generateDistinctColors = (numColors) => {
    //   const colors = [];
    //   const step = Math.floor(256 / numColors);
    
    //   for (let i = 0; i < numColors; i++) {
    //     const red = (step * i) % 256;
    //     const green = (step * (i + 1)) % 256;
    //     const blue = (step * (i + 2)) % 256;
    //     colors.push(`rgb(${red}, ${green}, ${blue})`);
    //   }
    
    //   return colors;
    // };

    return(
        <div>
          {true?<div style={{margin:'3.5vw',marginTop:'15px', paddingTop:'10px', background:'white'}}>
              <Selectdiv>
              <div style={{marginLeft:'15px'}}>For the month of {month}</div>
              
                <div style={{display:'flex', alignItems:'center'}}>
                  <span style={{'lineHeight':'38px', marginRight:'20px', marginLeft:'20px'}}>Plot by : </span>
                      {defaultValue && <Select
                      name="columns"
                      options={dropdndata}
                      className="basic-multi-select"
                      classNamePrefix="select"
                      onChange={handleSelectOptions}
                      defaultValue={defaultValue}
                            />}
                </div>

                  <div  style={{display:'flex', alignItems:'center'}}>
                    <span style={{'lineHeight':'38px', marginRight:'20px'}}>Text Size :</span>
                      {defaultValue && zdefault && <Select
                      name="columns"
                      options={zdropdndata}
                      className="basic-multi-select"
                      classNamePrefix="select"
                      onChange={handleSelectZOptions}
                      defaultValue={zdefault}
                      value = {zselect}
                            />}
                  </div>
              </Selectdiv>
              
              {showCustom &&
              <Customdiv>
              <span style={{'lineHeight':'38px', marginRight:'20px', marginLeft:'20px'}}></span><Select
                    name="columns"
                    options={xdropdndata}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={handleSelectOptionsX}
                          />
              <span  style={{'lineHeight':'38px',  marginLeft:'40px'}}>vs</span>
              {true?<div style={{display:'flex' , marginLeft:'20px'}}>
              <span style={{'lineHeight':'38px', marginRight:'20px', marginLeft:'20px'}}></span><Select
                      name="columns"
                      options={ydropdndata}
                      className="basic-multi-select"
                      classNamePrefix="select"
                      onChange={handleSelectOptionsY}
                      isDisabled = {xgraphdata.length>0?false:true}
                            />
              </div>:null}
              </Customdiv>}
              <ScatterChartWrapperdiv>
                  <ScatterChart width={chartWidth} height={600}   margin={{ top: 20, right: 50, bottom: 30, left: 20 }} ref={chartRef}>
                          <ReferenceLine x={(refxmin.current+refxmax.current)/2} stroke="black" strokeDasharray="3 3" />
                          <ReferenceLine y={(refymin.current+refymax.current)/2} stroke="black" strokeDasharray="3 3" />
                          <XAxis dataKey="x" padding={{ left: 100, right: 20 }} type="number" domain={[refxmin.current, refxmax.current]} allowDataOverflow={true}  label={{ value: paramx, position: 'insideBottom',offset:-10 }} ticks={[parseFloat(refxmin.current).toFixed(2), parseFloat(refxmax.current).toFixed(2)]}/>
                          <YAxis dataKey="y" padding={{ top: 20, bottom: 20 }} type="number" domain={[refymin.current, refymax.current]} allowDataOverflow={true} label={{ value: paramy, angle: -90, position: 'inside' }} ticks = {[parseFloat(refymin.current).toFixed(2), parseFloat(refymax.current).toFixed(2)]}/>
                          <Scatter data={graphdata}  shape={renderCustomShape}/>
                  </ScatterChart>
              </ScatterChartWrapperdiv>
              {showLog?<div style={{marginLeft:'15px', paddingBottom:'10px'}}>Note : Logarithmic Scale utilised for Optimal Text Size Representation</div>:null}
          </div>:
          <div>
          Loading
          </div>
                }
        </div>
    )
};

export default CustomScatterChart;

const Selectdiv = styled.div`
    display:flex;
    justify-content:space-between;
    margin-top:2px;
    margin-right:20px;
`
const Customdiv = styled.div`
  display:flex;
  justify-content:center;
  margin-top:10px;
`

const ScatterChartWrapperdiv = styled.div`
    display:flex;
    align-items:center;
    justify-content:center;
    background-color:white;
`

const LegendDiv = styled.div`
  margin-left:70px;
`