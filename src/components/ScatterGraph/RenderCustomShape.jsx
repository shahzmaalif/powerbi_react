import React, { useState, useEffect } from 'react';
import { Text } from 'recharts'; // Assuming Text comes from recharts
import { useMemo } from 'react';

const doesTextOverlap = (newDot, existingDots, textSize) => {
  const estimatedWidth = textSize * newDot.name.length * 0.6; // Estimate text width
  const estimatedHeight = textSize; // Estimate text height based on font size

  for (let dot of existingDots) {
    const dotWidth = textSize * dot.name.length * 0.6;
    const dotHeight = textSize;

    if (Math.abs(dot.cx - newDot.cx) < (dotWidth + estimatedWidth) / 2 &&
        Math.abs(dot.cy - newDot.cy) < (dotHeight + estimatedHeight) / 2) {
      return true; // Text overlap detected
    }
  }
  return false;
};

const adjustTextPosition = (dot, existingDots, textSize) => {
  let newX = dot.cx;
  let newY = dot.cy;
  while (doesTextOverlap({ ...dot, cx: newX, cy: newY }, existingDots, textSize)) {
    newX += 10; // Adjust x position
    newY += 10; // Adjust y position simultaneously
  }
  return { x: newX, y: newY };
};

const RenderCustomShape = (props) => {
  const [renderedDots, setRenderedDots] = useState([]);
  const textSize = 20; // Define text size (font size)

  const { cx, cy, payload } = props;

  // Adjust position if there's an overlap
  const adjustedPosition = adjustTextPosition({ ...payload, cx, cy }, renderedDots, textSize);
  const newDot = useMemo(() => ({ ...payload, cx: adjustedPosition.x, cy: adjustedPosition.y }), [payload, adjustedPosition.x, adjustedPosition.y]);
  // Update state only once using useEffect
  useEffect(() => {
    setRenderedDots(existingDots => [...existingDots, newDot]);
  }, [newDot]);

  return (
    <Text
      x={newDot.cx}
      y={newDot.cy}
      fontSize={`${textSize}px`}
      textAnchor="middle"
      verticalAnchor="middle"
    >
      {payload.name}
    </Text>
  );
};

export default RenderCustomShape;