import React from 'react'
import { useState } from 'react'


const Form = () => {
    const [formdata,setFormData] = useState({
        fname:'',
        lname:'',
        email:'',
        password:''
    })

    const handleSubmit = ()=>{
        console.log(formdata)
        console.log('hellow')
    }

    const handleChange = () =>{
        console.log('hello')
    }

  return (
    <div>
        <form onSubmit={handleSubmit}>
            <div>
                <label>
                    <input value={formdata.fname} onChange = {(e)=>{setFormData.fname(e.target.value)}}  name = 'fname' placeholder='firstname'/>
                </label>
                <input value={formdata.lname} onChange = {handleChange}  name = 'lname' placeholder='lname'/>
                <input value={formdata.email} onChange = {handleChange}  name = 'email' placeholder='email'/>
                <input value={formdata.password} onChange = {handleChange}  name = 'password' placeholder='password'/>
            </div>

        </form>

    </div>
  )
}

export default Form