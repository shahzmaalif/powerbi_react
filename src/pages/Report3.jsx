import React from 'react'
import styled from 'styled-components'
import { useParams, useLocation, Navigate, useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import Header3 from '../components/Header3/Header3';
import PowerBiFrame from '../components/PowerBiFrame/PowerBiFrame';
import PowerBiFrame3 from '../components/PowerBiFrame3/PowerBiFrame3';
import MyContext from '../utils/contexts/MyContext';
import browsee from '@browsee/web-sdk';
import { useState } from 'react';
import { AUTO_LOGOUT_TIME } from "../constants/constants"

// import 

const Report3 = () => {
const search = useLocation().search;
const { treeData, setTreeData } = React.useContext(MyContext);
const [lastActivity, setLastActivity] = useState(Date.now());
const val = new URLSearchParams(search).get('val')
const key = new URLSearchParams(search).get('key')

window.localStorage.setItem('report' , val)
window.localStorage.setItem('rep_id', key)
const filter = new URLSearchParams(search).get('filter')
const filter_value  = new URLSearchParams(search).get('filter_value')
const industry_id = new URLSearchParams(search).get('industry_id')
const email = new URLSearchParams(search).get('email')
const source = new URLSearchParams(search).get('source')
const content_id = new URLSearchParams(search).get('content_id')
const content_type = new URLSearchParams(search).get('content_type')
const newsletter_name = new URLSearchParams(search).get('newsletter_name')
const newsletter_month = new URLSearchParams(search).get('newsletter_month')
const newsletter_user = new URLSearchParams(search).get('newsletter_user')
const newsletter_email = new URLSearchParams(search).get('newsletter_email')

console.log('parent_filter_value  = ',val, key, filter, filter_value)
let init_node = key?.toString()
const navigate = useNavigate();

const client_id = new URLSearchParams(search).get('client_id')
const backend_token = new URLSearchParams(search).get('backend_token')
const pseudo_email = new URLSearchParams(search).get('pseudo_email')
const user_name = new URLSearchParams(search).get('name')
const is_admin = new URLSearchParams(search).get('is_admin')

if (client_id){
  console.log('backend_client_id=', client_id)
  window.localStorage.setItem("clientID", client_id);
  window.localStorage.setItem('loginStatus','true')
}
if (backend_token){
  console.log('backend_token=', backend_token)
  window.localStorage.setItem("token", backend_token);
  window.localStorage.setItem('loginStatus', 'true')
}
if (pseudo_email){
  console.log(pseudo_email)
  window.localStorage.setItem("pseudo_email", pseudo_email);
}
if (email){
  console.log(email)
  window.localStorage.setItem("email", email);
}

if(user_name){
  window.localStorage.setItem('user_name', user_name)
} 
if (is_admin) {
  console.log("IS ADMIN--------", is_admin);
  window.localStorage.setItem("is_admin", is_admin === "True" ? true : false);
}


let gettype = (email) =>{
  if(email.split('@')[1]==='redseerconsulting.com'){
    return 'internal'
  }else{
    if(window.localStorage.getItem('clientID')==141){
      return 'freemium'
    }else{
      return 'premium'
    }
  }
}

if(`${process.env.REACT_APP_API_ENDPOINT}`==='https://api.benchmarks.digital' && window.localStorage.getItem('loginStatus')==='true'){
        console.log('browsee works')
        browsee.init({ apiKey: `${process.env.REACT_APP_API_BROWSEE_KEY}` });
        if(email){
    
          console.log('browseeemailm1= ', email)
          let val = gettype(email)
          browsee.identify(email,
             {
              email: email,
              name: val
              })
            console.log('val=', val)
        }else{
          console.log('browseeemailm2= ', window.localStorage.getItem('email'))
          let val = gettype(window.localStorage.getItem('email'))
          browsee.identify( window.localStorage.getItem('email'),
             {
              email: window.localStorage.getItem('email'),
              name:val
              })
  
              console.log('val=  ', val)
          
        }
      }else{
        
        console.log('browsee not work')
      }


function findParents(data, label, key_val, parents = []) {
  console.log('findParent', data, label, key_val, parents)
  for (let node of data) {
      // If the current node matches the given label and key_val
      if ((node.key_val).toString() === key_val.toString()) {
          // Add the current node's label and key to the parents array
          parents.push([node.label,node.key] );
          return [parents, node.industry_id]; // return the parents including current node
      }

      // If the current node has children, recurse
      if (node.nodes && node.nodes.length) {
          const newParents = [...parents, [node.label,node.key]];
          const found = findParents(node.nodes, label, key_val, newParents);
          if (found) {
            console.log('found=', found)
            return found
          };
      }
  }
  return null; // If not found
}

let handleSignOut = ()=>{
  console.log('signout')
  let prop_email = window.localStorage.getItem("email")
  let prop_token = window.localStorage.getItem("token")
  fetch(`${process.env.REACT_APP_API_ENDPOINT}/logout/?email=${prop_email}`,{
      method:'GET',
      headers:{
        'Content-Type': 'application/json',
        Authorization: `Token ${prop_token}`
      }
    }).then((res) => res.json())
    .then(
      res => {
          console.log('logout= ', res)
      }
      )
      .catch( error => {
        console.error(error)
      })
  localStorage.clear();
  navigate('/')
}

// useEffect(()=>{
//   const interval = setTimeout(() => {
//     console.log('Logs every minute');
//     handleSignOut()
//   }, AUTO_LOGOUT_TIME);

//   return () => clearInterval(interval);
// },[])

// useEffect(() => {
//   function updateLastActivity() {
//     setLastActivity(Date.now());
//   }
//   window.addEventListener('mousemove', updateLastActivity);
//   window.addEventListener('keydown', updateLastActivity);
//   window.addEventListener('scroll', updateLastActivity);
//   return () => {
//     window.removeEventListener('mousemove', updateLastActivity);
//     window.removeEventListener('keydown', updateLastActivity);
//     window.removeEventListener('scroll', updateLastActivity);
//   };
// }, []);

// useEffect(() => {

//   const interval = setInterval(() => {
//     const currentTime = Date.now();
//     const timeDifference = currentTime - lastActivity;
    
//     // If 30 minutes (or 1800000 milliseconds) has passed since the last activity, logout the user
//     if (timeDifference > 60*60*1000) {
//       console.log('User logged out due to inactivity');

//       clearInterval(interval);
//       window.localStorage.setItem('loginStatus','false')
//       navigate('/')
//     }
//   }, 10000); 

//   return () => clearInterval(interval); // Clear interval on unmount

// }, [lastActivity,]);

const result = findParents(JSON.parse(window.localStorage.getItem('windowallTreeData')), val, key);

  
if(window.localStorage.getItem('loginStatus')!=='true'){
  if(source){
    console.log('source = ', source)
    if(source==='newsletter'){
      console.log('newsletter_redirect')
      if(source) window.localStorage.setItem('source', source)
      if(val) window.localStorage.setItem('newsletter_val',val)
      if(filter) window.localStorage.setItem('newsletter_filter', filter)
      if(filter) window.localStorage.setItem('newsletter_filter_val', filter)
      if(key) window.localStorage.setItem('newsletter_key', key)
      if(content_id) window.localStorage.setItem('content_id', content_id)
      if(content_type) window.localStorage.setItem('content_type', content_type)
      if(newsletter_name) window.localStorage.setItem('newsletter_name', newsletter_name)
      if(newsletter_month) window.localStorage.setItem('newsletter_month', newsletter_month)
      if(newsletter_user) window.localStorage.setItem('newsletter_user', newsletter_user)
      if(newsletter_email) window.localStorage.setItem('newsletter_email', newsletter_email)
      return <Navigate to = "/otp/?source=newsletter"/>
    }
  }else{
    console.log('source = ', source)
    return <Navigate to = "/signin3"/>
  }

  

}
  if(!key && source==='newsletter'){
    if(newsletter_name) window.localStorage.setItem('newsletter_name', newsletter_name)
    if(newsletter_month) window.localStorage.setItem('newsletter_month', newsletter_month)
    if(newsletter_user) window.localStorage.setItem('newsletter_user', newsletter_user)
    if(newsletter_email) window.localStorage.setItem('newsletter_email', newsletter_email)
    if(source) window.localStorage.setItem('source', source)
    if(content_id) window.localStorage.setItem('content_id', content_id)
    if(content_type) window.localStorage.setItem('content_type', content_type)
    return <Navigate to = '/redirect'/>
  } else {
    return (
      <div>
        <Header3 />
        <PowerBiFrame3
          initialOpenNodes={[init_node]}
          headarr={["Sectors"]}
          treeziparr={result[0]}
          root_rep={val}
          value={val}
          key={key}
          filter={filter}
          filter_value={filter_value}
          content_id={content_id}
          industry_id={result[1]}
        />
      </div>
    );
  }
}

export default Report3