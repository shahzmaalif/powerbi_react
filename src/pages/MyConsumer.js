// MyConsumer.js
import React from 'react';
import MyContext from '../utils/contexts/MyContext';

const MyConsumer = () => {
  // Access the context using the useContext hook
  const { count, increment, decrement } = React.useContext(MyContext);

  return (
    <div>
      <h2>Count: {count}</h2>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
};

export default MyConsumer;
