import React from 'react'
import Fintable from '../components/FinTable/Fintable'
import CustomScatterChart from '../components/ScatterGraph/CustomScatterChart'
import { useState } from 'react'
import { Switch } from 'antd';
import ExcelLikeTable from './test';

const Testing = (props) => {

  const [showgrid,setshowgrid] = useState(true)

  const onChange = (checked) => {
    console.log(`switch to ${checked}`);
    setshowgrid(checked)
  };
  const data = [     { Name: 'John Doe', Age: 30, Email: 'john@example.com' },     { Name: 'Jane Doe', Age: 25, Email: 'jane@example.com' },  ]
  return (
    
    <div>
      <div style={{'padding':'3.5vw', paddingBottom:'0px', paddingTop:'10px'}}>
        <div style={{'fontSize':'25px', 'fontWeight':'500', marginBottom:'15px'}}>Platform Listing</div>
              <div>
                  Magic Quadrant <Switch defaultChecked onChange={onChange} /> List
              </div>
      </div>
       {showgrid?<Fintable industry_id = {props.industry_id} report = {props.report}dropDownData = {props.dropDownData}  onPlayerClick = {props.onPlayerClick}/>:<CustomScatterChart/>}
        {/* <ExcelLikeTable data = {data}/> */}
    </div>
  )
}

export default Testing