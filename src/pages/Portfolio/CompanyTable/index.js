import React, { useEffect, useState } from "react";
import styled from "styled-components";
import MultiSelect from "../../../components/MultiSelect";
import Pagination from "../../../components/Pagination";
import MasterTable from "../../../components/MasterTable";

const CompanyTable = ({
  allParameters = [],
  rowsData = [],
  dataMonth,

  industryList = [],
  setSelectedIndustry = () => null,

  seriesList = [],
  setSelectedSeries = () => null,

  sortedParameter,
  setSortedParameter = () => null,
  sortedDirection,
  setSortedDirection = () => null,
}) => {
  // API whole data
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const dataPerPage = 10;

  // useEffect(() => {
  //   setFilteredCompDataList(
  //     rowsData.filter((comp) => {
  //       let allParameterInRange = true;
  //       for (const key in parameterRange) {
  //         if (parameterRange[key] && parameterRange[key].length) {
  //           if (!comp.paramData) {
  //             /* have filter but no data */
  //             return false;
  //           } else if (!comp.paramData[key]) {
  //             /* have filter but no data */
  //             return false;
  //           } else {
  //             /* check if this parameter in range */
  //             if (
  //               !parameterRange[key].some(
  //                 (range) =>
  //                   comp.paramData[key].current_value >= range.low &&
  //                   comp.paramData[key].current_value <= range.high
  //               )
  //             ) {
  //               /* not found in any range */
  //               allParameterInRange = false;
  //             }
  //           }
  //         }
  //       }
  //       return allParameterInRange;
  //     })
  //   );
  // }, [visibaleCompDataList, parameterRange]);

  useEffect(() => {
    setCurrentPage(1);
    setTotalPage(Math.ceil(rowsData.length / dataPerPage));
  }, [rowsData]);

  const startIndex = (currentPage - 1) * dataPerPage;
  const currentRowsData = rowsData.slice(startIndex, startIndex + dataPerPage);

  return (
    <Wrapper>
      <ShadowWrap>
        <ShadowBox>
          <TableMenu>
            <TableMenuLeft>For the last 12 months</TableMenuLeft>
            <TableMenuRight>
              <Label>Filter by Sectors:</Label>
              <MultiSelect
                options={industryList}
                onSelectedChange={setSelectedIndustry}
                keyFieldName="industry_id"
                valueFieldName="industry_name"
                selectedOptions={industryList.filter((obj) => obj.selected)}
                mutileSelect={true}
                sortOptions={true}
                placeholder="Filter by Sectors..."
                width="250px"
              />
              <Label>Filter by Series:</Label>
              <MultiSelect
                options={seriesList}
                onSelectedChange={setSelectedSeries}
                keyFieldName="id"
                valueFieldName="name"
                selectedOptions={seriesList.filter((obj) => obj.selected)}
                mutileSelect={true}
                placeholder="Filter by Series..."
                width="250px"
              />
            </TableMenuRight>
          </TableMenu>
          <MasterTable
            columnsData={allParameters}
            rowsData={rowsData}
            sortedColKey={sortedParameter}
            setSortedColKey={setSortedParameter}
            sortedDirection={sortedDirection}
            setSortedDirection={setSortedDirection}
            headerBackground={"#D4EEFF"}
            toolGap={5}
          />
        </ShadowBox>
      </ShadowWrap>
      {/* <PaginationWrapper>
        <Pagination
          currentPage={currentPage}
          totalPage={totalPage}
          setCurrentPage={setCurrentPage}
        />
      </PaginationWrapper> */}
    </Wrapper>
  );
};

export default CompanyTable;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const ShadowWrap = styled.div`
  min-height: 400px;
  max-width: 100%;
`;

const ShadowBox = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: 0px 2px 4px 0px #00000014;
`;

const CompanySearch = styled.div`
  display: flex;
  flex-direction: column;
  padding: 50px 0 30px;
  align-items: center;
  background-color: #ffffff;
`;

const CompanyInputWrapper = styled.div`
  display: flex;
  width: 60%;
  height: 60px;
  position: relative;
`;

const CompanyInput = styled.input`
  width: 100%;
  height: 100%;
  padding: 14px 32px 14px 56px;
  align-items: center;
  flex-shrink: 0;
  border-radius: 30px;
  color: #3c4043;
  font-family: Fira Sans;
  font-size: 20px;
  font-style: normal;
  font-weight: 500;
  line-height: normal;
  border: 1px solid var(--grey-dark, #d1d1d1);
  ::placeholder {
    color: #bfbfbf;
    font-size: 20px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  :hover,
  :focus {
    border: 1px solid #09f;
    background: var(--white, #fff);
    outline: none;
  }
`;

const SearchWrapper = styled.div`
  display: flex;
  width: 24px;
  height: 24px;
  position: absolute;
  left: 22px;
  top: 18px;
`;

const TagsWrapper = styled.div`
  display: flex;
  width: 100%;
  padding: 24px 20% 24px;
  flex-wrap: wrap;
  background-color: #ffffff;
  justify-content: center;
`;

const TagDiv = styled.div`
  height: 28px;
  font-size: 12px;
  font-weight: 400;
  line-height: 16px;
  letter-spacing: 0em;
  text-align: left;
  padding: 6px 10px 6px 10px;
  background-color: ${(props) => (props.selected ? "#0099FF" : "#f2f2f2")};
  color: ${(props) => (props.selected ? "#FFFFFF" : "#262e40")};
  margin: 6px 6px;
  border-radius: 24px;
  cursor: pointer;
`;

const GridWraper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 24px 60px 50px;
`;

const Header1 = styled.div`
  background-color: white;
  padding: 3.5vw;
  padding-top: 2vw;
  padding-bottom: 5px;
  display: flex;
  justify-content: space-between;
`;

const BreadCrumbSpan = styled.span`
  &:hover {
    color: #20a6ff;
    cursor: pointer;
  }
`;

const LHSTitlediv = styled.div`
  font-weight: 500;
  font-size: 30px;
`;

const RHSTitlediv = styled.div`
  display: flex;
  gap: 10px;
`;

const TableMenu = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  min-height: 56px;
  padding: 15px 0px 20px;
  background-color: #ffffff;
  align-items: center;
`;

const TableMenuLeft = styled.div`
  margin-left: 15px;
`;

const TableMenuRight = styled.div`
  display: flex;
  align-items: center;
  padding: 0 20px;
`;

const Label = styled.div`
  margin: 0 5px 0px 10px;
`;

const SelectWraper = styled.div`
  margin-left: 10px;
`;

const PaginationWrapper = styled.div`
  padding: 20px 0 10px;
`;
