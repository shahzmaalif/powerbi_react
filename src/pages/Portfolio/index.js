import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useLocation } from "react-router-dom";
import Header3 from "../../components/Header3/Header3";
import {
  WEBFORM_TOKEN,
  HOME_URL,
  INVESTOR_ENDPOINT,
  TAG_ENDPOINT,
  INVESTOR_PORTFOLIO_ENDPOINT,
} from "../../constants/constants";
import MultiSelect from "../../components/MultiSelect";
import SearchIcon from "../../components/svg/SearchIcon";
import { useNavigate } from "react-router-dom";
import Loader from "../../components/Loader";
import axios from "axios";
import moment from "moment";
import CompanyTable from "./CompanyTable";
import ArrowIcon from "../../components/svg/ArrowIcon";
import VectorIcon from "../../components/svg/VectorIcon";
import { TbTriangleFilled } from "react-icons/tb";
import { TbTriangleInvertedFilled } from "react-icons/tb";

const BoxData = ({ value, value2, value3, isUp }) => {
  const bgColor = isUp ? "#D3EFDB" : "#F8D2D4";
  return (
    <BoxWrapper2>
      <BoxWrapper3>
        <BoxValue>{value}</BoxValue>
        {value2 ? (
          <IconBox color={bgColor}>
            <BoxValue2 color={"#444444"}>{value2}%</BoxValue2>
            <VectorIcon
              width={9}
              height={13}
              color={"#444444"}
              rotate={isUp ? 0 : 180}
            />
          </IconBox>
        ) : null}
      </BoxWrapper3>
      {value3 ? <BoxValue5>Rank {value3}</BoxValue5> : null}
    </BoxWrapper2>
  );
};

const BoxData4 = ({ value, value2, isUp }) => {
  const bgColor = isUp ? "#D3EFDB" : "#F8D2D4";
  const formattedValue1 =
    typeof value === "number"
      ? Number(value.toFixed(0)).toLocaleString("en-US", {
          minimumFractionDigits: 0,
          maximumFractionDigits: 0,
        })
      : value;
  const formattedValue2 =
    typeof value2 === "number"
      ? Number(value2.toFixed(2)).toLocaleString("en-US", {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })
      : value2;
  return (
    <BoxWrapper2>
      <BoxWrapper>
        <BoxValue4>{formattedValue1}</BoxValue4>
        {value2 ? (
          <IconBox color={bgColor}>
            <BoxValue2 color={"#444444"}>{formattedValue2}%</BoxValue2>
            <VectorIcon
              width={9}
              height={13}
              color={"#444444"}
              rotate={isUp ? 0 : 180}
            />
          </IconBox>
        ) : null}
      </BoxWrapper>
    </BoxWrapper2>
  );
};

const BoxData2 = ({ value, value2 }) => {
  return (
    <BoxWrapper2>
      <BoxValue>{value}</BoxValue>
      {value2 ? <BoxValue>({value2})</BoxValue> : null}
    </BoxWrapper2>
  );
};

const BoxData3 = ({ value, value2 }) => {
  return (
    <BoxWrapper2>
      <BoxValue3>{value}</BoxValue3>
      {value2 ? <SecondBoxValue3>({value2})</SecondBoxValue3> : null}
    </BoxWrapper2>
  );
};
const BoxWrapper = styled.div`
  width: 85%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const BoxWrapper3 = styled.div`
  width: 90%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const BoxWrapper2 = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
`;

const IconBox = styled.div`
  display: flex;
  flex-direction: col;
  align-items: center;
  background-color: ${(props) => props.color};
  border-radius: 20px;
  margin-left: 10px;
  padding: 4px 11px;
`;

const BoxValue = styled.div`
  font-size: 16px;
  font-weight: 400;
`;

const BoxValue4 = styled.div`
  font-size: 16px;
  font-weight: 400;
  width: 30%;
  text-align: right;
`;

const BoxValue5 = styled.div`
  font-size: 16px;
  font-weight: 400;
  margin-top: 15px;
`;

const BoxValue3 = styled.div`
  font-size: 14px;
  font-weight: 400;
`;

const SecondBoxValue3 = styled.div`
  font-size: 14px;
  font-weight: 400;
  margin-top: 15px;
`;

const BoxValue2 = styled.div`
  font-size: 16px;
  line-height: 21px;
  font-weight: 500;
  color: ${(props) => props.color};
  margin-right: 4px;
`;

const Icon = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 30px;
  height: 30px;
  border-radius: 24px;
  background-color: ${(props) => props.color};
`;

const Portfolio = () => {
  // API whole data
  const [allInvestor, setAllInvestor] = useState([]);

  // current investor profile
  const [clientId, setClientId] = useState(null);
  const [currentInvestorId, setCurrentInvestorId] = useState(null);
  const [currentInvestorName, setCurrentInvestorName] = useState("");

  // selected investor
  const [selectedInvestor, setSelectedInvestor] = useState({});
  const [allCompanies, setAllCompanies] = useState([]);
  const [visibleCompanies, setVisibleCompanies] = useState([]);
  const [isPortfolioLoaded, setPortfolioLoaded] = useState(false);

  const getNameHeader = (count) => {
    if (count <= 1) {
      return (
        <CountTextWrap>
          <div>Name</div>
          <CountText>({count} Result)</CountText>
        </CountTextWrap>
      );
    }
    return (
      <CountTextWrap>
        <div>Name</div>
        <CountText>({count} Results)</CountText>
      </CountTextWrap>
    );
  };

  const [columnsData, setColumnsData] = useState([
    {
      key: "player",
      value: getNameHeader(0),
      sorting: false,
      range_filter: false,
    },
    // {
    //   key: "sector",
    //   value: "Sector",
    //   sorting: false,
    //   range_filter: false,
    // },
  ]);
  const [isColumnsLoaded, setColumnsLoaded] = useState(false);
  const [styledRowsData, setStyledRowsData] = useState([]);

  const [indReportMapping, setIndReportMapping] = useState([]);
  const [indWiseCompReport, setIndWiseCompReport] = useState([]);
  const [isReportMappingLoaded, setReportMappingLoaded] = useState(false);

  const [dataMonth, setDataMonth] = useState(null);

  // user cutomized data
  const useQuery = () => new URLSearchParams(useLocation().search);
  const [compSearchString, setCompSearchString] = useState(
    useQuery().get("company") || ""
  );
  const [tags, setTags] = useState([]);
  const [industryList, setIndustryList] = useState([]);
  const [seriesList, setSeriesList] = useState([]);

  // const [visibleParameter, setVisibleParameter] = useState([]);
  const [sortedParameter, setSortedParameter] = useState(null);
  const [sortedDirection, setSortedDirection] = useState("");
  const navigate = useNavigate();

  const treeziparr = [
    { key: 1, name: "Home" },
    { key: 2, name: "Platform Listing" },
    { key: 3, name: "Portfolio" },
  ];

  let handleClickTree = (index) => {
    if (treeziparr[index].key === 1) {
      navigate(HOME_URL);
    }
    if (treeziparr[index].key === 2) {
      navigate("/company-listing");
    }
  };

  // const onSelectParameter = (selectedParam) => {
  //   setVisibleParameter(selectedParam);
  // };

  const setSelectedIndustry = (selectedIndustry) => {
    setIndustryList(
      industryList.map((ind) => {
        return {
          ...ind,
          selected: selectedIndustry.some(
            (selectedInd) => selectedInd.industry_id === ind.industry_id
          ),
        };
      })
    );
  };

  const setSelectedSeries = (selectedSeriesList) => {
    setSeriesList(
      seriesList.map((series) => {
        return {
          ...series,
          selected: selectedSeriesList.some(
            (selectedSeries) => selectedSeries.id === series.id
          ),
        };
      })
    );
  };

  useEffect(() => {
    let client_id = window.localStorage.getItem("clientID");
    if (client_id) {
      setClientId(client_id);
    } else {
      navigate("/");
    }
  }, []);

  // useEffect(() => {
  //   axios
  //     .get(`${TAG_ENDPOINT}/?type=portfolio`)
  //     .then((response) => response.data)
  //     .then((tagData) => {
  //       setTags(
  //         tagData.map((obj) => ({
  //           ...obj,
  //           selected: false,
  //         }))
  //       );
  //     })
  //     .catch((error) => {
  //       console.error("Error in fetching tags-", error);
  //     });
  // }, []);

  useEffect(() => {
    axios
      .get(`${INVESTOR_ENDPOINT}/?active_portfolio=true`)
      .then((response) => response.data)
      .then((response) => {
        const investor_id =
          Number(window.localStorage.getItem("investor_id")) || null;
        setAllInvestor(
          response.map((investor) => ({
            ...investor,
            name: investor.id === investor_id ? "My Portfolio" : investor.name,
          }))
        );
      })
      .catch((error) => {
        console.error("Error in fetching investors-", error);
      });
  }, []);

  useEffect(() => {
    const fetchInvestorDetail = async () => {
      const investor_id = window.localStorage.getItem("investor_id") || null;
      setCurrentInvestorId(Number(investor_id));
      axios
        .get(`${INVESTOR_ENDPOINT}/${investor_id}`)
        .then((response) => response.data)
        .then((investorData) => {
          setCurrentInvestorName(investorData.name);
          setSelectedInvestor({ ...investorData, name: "My Portfolio" });
          const companySet = new Set();
          investorData.deal_list.forEach((deal) => {
            if (deal.company_id) companySet.add(deal.company_id);
          });
          fetchPostfolioData(Array.from(companySet), investorData.id);
        })
        .catch((error) => {
          if (error.response && error.response.status === 404) {
            console.error("Investor not found");
            window.localStorage.removeItem("investor_id");
            navigate("/");
          } else {
            console.error("Error in fetching industries-", error);
          }
        });
    };
    if (clientId) {
      fetchInvestorDetail();
    }
  }, [clientId]);

  const fetchPostfolioData = (player_id_list, investor_id) => {
    setPortfolioLoaded(false);
    const data = {
      player_id_list: player_id_list,
    };
    const url = `${INVESTOR_PORTFOLIO_ENDPOINT}/`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${WEBFORM_TOKEN}`,
    };
    axios
      .post(url, data, { headers })
      .then((response) => response.data)
      .then((companiesData) => {
        if (!isColumnsLoaded && companiesData.length) {
          let updatedParameters = [
            {
              key: "player",
              value: getNameHeader(companiesData.length),
              sorting: false,
              range_filter: false,
            },
            // {
            //   key: "sector",
            //   value: "Sector",
            //   sorting: false,
            //   range_filter: false,
            // },
          ];
          companiesData[0].param_data.map((param) => {
            if (param.id === 7074 || param.id === 7075) {
              updatedParameters.push({
                key: param.id,
                value: (
                  <HeaderWrapper>
                    <HeaderText>{param.name}</HeaderText>
                    {param.unit ? (
                      <HeaderText>({param.unit})</HeaderText>
                    ) : null}
                  </HeaderWrapper>
                ),
                sorting: true,
                range_filter: false,
                min_width: param.id === 7074 ? 400 : null,
              });
            } else {
              if (
                param.id !== 7079 &&
                param.id !== 7073 &&
                param.id !== 8124 &&
                param.id !== 7080
              ) {
                updatedParameters.push({
                  key: param.id,
                  value: (
                    <HeaderWrapper>
                      <HeaderText>{param.name}</HeaderText>
                      {param.unit ? (
                        <HeaderText>({param.unit})</HeaderText>
                      ) : null}
                    </HeaderWrapper>
                  ),
                  sorting: [7074, 7077, 7080].includes(param.id) ? true : false,
                  range_filter: false,
                  min_width:
                    param.id === 8122 ? 260 : param.id === 8124 ? 200 : null,
                });
              }
            }
          });
          setColumnsData(updatedParameters);
          setColumnsLoaded(true);
        }
        if (allInvestor.length) {
          const updateInvestor = allInvestor.map((investor) => {
            if (investor.id === investor_id) {
              return { ...investor, companiesData: companiesData };
            }
            return investor;
          });
          setAllInvestor(updateInvestor);
        }
        setAllCompanies(companiesData);
        setVisibleCompanies(companiesData);

        const industry_id_set = new Set();
        let industryDataList = [];
        const series_set = new Set();
        companiesData.forEach((obj) => {
          series_set.add(obj.series);
          if (!industry_id_set.has(obj.industry_id)) {
            industry_id_set.add(obj.industry_id);
            industryDataList.push({
              industry_id: obj.industry_id,
              industry_name: obj.industry_name,
              selected: false,
            });
          }
        });
        setIndustryList(industryDataList);
        setSeriesList(
          Array.from(series_set)
            .map((series) => ({
              id: series,
              name: series,
              selected: false,
            }))
            .sort((a, b) => a.name.localeCompare(b.name))
        );
        setPortfolioLoaded(true);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const onPortfolioChange = (filteredInvestor) => {
    setSelectedInvestor(filteredInvestor[0]);
    const companySet = new Set();
    filteredInvestor[0].deal_list.forEach((deal) => {
      if (deal.company_id) companySet.add(deal.company_id);
    });

    if (filteredInvestor[0].companiesData) {
      if (!isColumnsLoaded && filteredInvestor[0].companiesData.length) {
        let updatedParameters = [
          {
            key: "player",
            value: getNameHeader(filteredInvestor[0].companiesData.length),
            sorting: false,
            range_filter: false,
          },
          // {
          //   key: "sector",
          //   value: "Sector",
          //   sorting: false,
          //   range_filter: false,
          // },
        ];
        filteredInvestor[0].companiesData[0].param_data.map((param) => {
          if (param.id === 7074 || param.id === 7075) {
            updatedParameters.push({
              key: param.id,
              value: (
                <HeaderWrapper>
                  <HeaderText>{param.name}</HeaderText>
                  {param.unit ? <HeaderText>({param.unit})</HeaderText> : null}
                </HeaderWrapper>
              ),
              sorting: true,
              range_filter: false,
              min_width: param.id === 7074 ? 400 : null,
            });
          } else {
            if (
              param.id !== 7079 &&
              param.id !== 7073 &&
              param.id !== 8124 &&
              param.id !== 7080
            ) {
              updatedParameters.push({
                key: param.id,
                value: (
                  <HeaderWrapper>
                    <HeaderText>{param.name}</HeaderText>
                    {param.unit ? (
                      <HeaderText>({param.unit})</HeaderText>
                    ) : null}
                  </HeaderWrapper>
                ),
                sorting: [7074, 7077, 7080].includes(param.id) ? true : false,
                range_filter: false,
                min_width:
                  param.id === 8122 ? 260 : param.id === 8124 ? 200 : null,
              });
            }
          }
        });
        setColumnsData(updatedParameters);
        setColumnsLoaded(true);
      }
      setAllCompanies(filteredInvestor[0].companiesData);
      setVisibleCompanies(filteredInvestor[0].companiesData);

      const industry_id_set = new Set();
      let industryDataList = [];
      const series_set = new Set();
      filteredInvestor[0].companiesData.forEach((obj) => {
        series_set.add(obj.series);
        if (!industry_id_set.has(obj.industry_id)) {
          industry_id_set.add(obj.industry_id);
          industryDataList.push({
            industry_id: obj.industry_id,
            industry_name: obj.industry_name,
            selected: false,
          });
        }
      });
      setIndustryList(industryDataList);
      setSeriesList(
        Array.from(series_set)
          .map((series) => ({
            id: series,
            name: series,
            selected: false,
          }))
          .sort((a, b) => a.name.localeCompare(b.name))
      );
    } else {
      fetchPostfolioData(Array.from(companySet), filteredInvestor[0].id);
    }
  };

  // const onTagClick = (tagId) => {
  //   const updatedTags = tags.map((obj) => {
  //     return {
  //       ...obj,
  //       selected: obj.id === tagId ? !obj.selected : obj.selected,
  //     };
  //   });
  //   const commaSeperatedTagList = updatedTags
  //     .filter((obj) => obj.selected)
  //     .map((obj) => obj.id)
  //     .join(",");
  //   window.localStorage.setItem("comp_selected_tags", commaSeperatedTagList);

  //   setTags(
  //     tags.map((obj) => {
  //       return {
  //         ...obj,
  //         selected: obj.id === tagId ? !obj.selected : obj.selected,
  //       };
  //     })
  //   );
  // };

  useEffect(() => {
    let rowsData = [];
    visibleCompanies.forEach((company) => {
      let cpReportData = null;
      let spReports = company.sector_profile_id
        ? indReportMapping.filter(
            (indReport) => indReport.key === company.sector_profile_id
          )
        : [];
      const spReport = spReports.length > 0 ? spReports[0] : null;
      if (company.industry_id) {
        cpReportData =
          indWiseCompReport[company.industry_id]?.filter((indReport) =>
            indReport.label
              ?.toLowerCase()
              .includes(company.player_name.toLowerCase())
          )?.[0] || null;
      }

      let rowData = {
        player: {
          value: (
            <div>
              <CpProfileLink
                islink={Boolean(cpReportData) ? "true" : "false"}
                onClick={() => {
                  if (Boolean(cpReportData)) {
                    // navigate(
                    //   `/Report3/?val=${cpReportData.label}&key=${cpReportData.key}&filter=${cpReportData.filter}&filter_value=${cpReportData.filter_value}`
                    // );
                    const url = `/Report3/?val=${cpReportData.label}&key=${cpReportData.key}&filter=${cpReportData.filter}&filter_value=${cpReportData.filter_value}`;
                    window.open(url, "_blank");
                  }
                }}
              >
                {company.player_name}
              </CpProfileLink>
              <SpProfileLink
                islink={Boolean(spReport) ? "true" : "false"}
                onClick={() => {
                  if (Boolean(spReport)) {
                    const url = `/Report3/?val=${spReport.label}&key=${spReport.key}&filter=${spReport.filter}&filter_value=${spReport.filter_value}`;
                    window.open(url, "_blank");
                  }
                }}
              >
                {company.industry_name}
              </SpProfileLink>
            </div>
          ),
          sortingValue: company.player_name,
          toolTip: null,
        },
        sector: {
          value: company.industry_name,
          sortingValue: company.industry_name,
          toolTip: null,
        },
      };

      let marketSize = {
        id: null,
        value: null,
        value2: null,
        sortingValue: null,
      };

      let marketShareRange = {
        id: null,
        value: null,
        value2: null,
        value3: null,
        sortingValue: null,
      };

      let marketGrowth = {
        id: null,
        value: null,
        value2: null,
        sortingValue: null,
      };

      let quarterOutlook = {
        id: null,
        value: null,
        value2: null,
        sortingValue: null,
      };

      company.param_data.forEach((param) => {
        const formattedValue =
          typeof param.value === "number"
            ? Number(param.value.toFixed(2)).toLocaleString("en-US", {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })
            : param.value;
        if (param.id === 7074) {
          marketShareRange.id = param.id;
          marketShareRange.value = formattedValue;
        } else if (param.id === 7079) {
          marketShareRange.value2 = formattedValue;
          marketShareRange.sortingValue = param.value;
        } else if (param.id === 7077) {
          marketSize.id = param.id;
          marketSize.value = param.value;
          marketSize.sortingValue = param.value;
        } else if (param.id === 7080) {
          marketSize.value2 = param.value;
          marketGrowth.id = param.id;
          marketGrowth.value = formattedValue;
          marketGrowth.sortingValue = param.value;
        } else if (param.id === 7073) {
          marketGrowth.value2 = formattedValue;
          marketShareRange.value3 = formattedValue;
        } else if (param.id === 8122) {
          quarterOutlook.id = param.id;
          quarterOutlook.value = formattedValue;
        } else if (param.id === 8124) {
          quarterOutlook.value2 = formattedValue;
        } else {
          rowData[param.id] = {
            value: formattedValue,
            sortingValue: param.value,
          };
        }
        rowData[marketShareRange.id] = {
          value: (
            <BoxData
              value={marketShareRange.value}
              value2={marketShareRange.value2}
              value3={marketShareRange.value3}
              isUp={marketShareRange.value2 > 0}
            />
          ),
          sortingValue: marketShareRange.sortingValue,
          toolTip:
            marketShareRange.value ||
            marketShareRange.value2 ||
            marketShareRange.value3 ? (
              <div>
                {marketShareRange.value && (
                  <TooltipRow>
                    <span style={{ color: "#9e9e9e", fontSize: "12px" }}>
                      Current Range -{" "}
                    </span>
                    <span style={{ fontSize: "12px", marginLeft: "15px" }}>
                      {marketShareRange.value}
                    </span>
                  </TooltipRow>
                )}
                {marketShareRange.value2 && (
                  <TooltipRow>
                    <span style={{ color: "#9e9e9e", fontSize: "12px" }}>
                      YOY Growth -{" "}
                    </span>
                    <span style={{ fontSize: "12px", marginLeft: "28px" }}>
                      {marketShareRange.value2}%{" "}
                      {marketShareRange.value2 >= 0 ? (
                        <TbTriangleFilled style={{ color: "green" }} />
                      ) : (
                        <TbTriangleInvertedFilled style={{ color: "red" }} />
                      )}
                    </span>
                  </TooltipRow>
                )}
                {marketShareRange.value3 && (
                  <TooltipRow>
                    <span style={{ color: "#9e9e9e", fontSize: "12px" }}>
                      Rank{" "}
                    </span>
                    <span style={{ fontSize: "12px", marginLeft: "15px" }}>
                      {marketShareRange.value3}
                    </span>
                  </TooltipRow>
                )}
              </div>
            ) : null,
        };

        rowData[marketGrowth.id] = {
          value: (
            <BoxData2 value={marketGrowth.value} value2={marketGrowth.value2} />
          ),
          sortingValue: marketGrowth.sortingValue,
          toolTip: null,
        };

        const marketSizeValue =
          typeof marketSize.value === "number"
            ? Number(marketSize.value.toFixed(0)).toLocaleString("en-US", {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0,
              })
            : marketSize.value;

        const marketSizeValue2 =
          typeof marketSize.value2 === "number"
            ? Number(marketSize.value2.toFixed(2)).toLocaleString("en-US", {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })
            : marketSize.value2;

        rowData[marketSize.id] = {
          value: (
            <BoxData4
              value={marketSize.value}
              value2={marketSize.value2}
              isUp={marketSize.value2 > 0}
            />
          ),
          sortingValue: marketSize.sortingValue,
          toolTip:
            marketSize.value || marketSize.value2 ? (
              <div>
                {marketSize.value && (
                  <TooltipRow>
                    <span style={{ color: "#9e9e9e", fontSize: "12px" }}>
                      Market Size -{" "}
                    </span>
                    <span style={{ fontSize: "12px", marginLeft: "15px" }}>
                      {marketSizeValue}
                    </span>
                  </TooltipRow>
                )}
                {marketSize.value2 && (
                  <TooltipRow>
                    <span style={{ color: "#9e9e9e", fontSize: "12px" }}>
                      YOY Growth -{" "}
                    </span>
                    <span style={{ fontSize: "12px", marginLeft: "28px" }}>
                      {marketSizeValue2}%{" "}
                      {marketSize.value2 >= 0 ? (
                        <TbTriangleFilled style={{ color: "green" }} />
                      ) : (
                        <TbTriangleInvertedFilled style={{ color: "red" }} />
                      )}
                    </span>
                  </TooltipRow>
                )}
              </div>
            ) : null,
        };

        rowData[quarterOutlook.id] = {
          value: (
            <BoxData3
              value={quarterOutlook.value}
              value2={quarterOutlook.value2}
            />
          ),
          sortingValue: null,
          toolTip: null,
        };
      });

      rowsData.push(rowData);
    });
    // const rowsDataDefaultSort = rowData

    setStyledRowsData(
      rowsData.sort((a, b) => {
        const aVal = a[7077]?.sortingValue || 0;
        const bVal = b[7077]?.sortingValue || 0;
        if (typeof aVal === "number" && typeof bVal === "number") {
          return bVal - aVal;
        } else if (typeof aVal === "string" && typeof bVal === "string") {
          return bVal.localeCompare(aVal);
        } else {
          return 0;
        }
      })
    );

    // setStyledRowsData(rowsData);
    setSortedParameter(7077);
    setSortedDirection("desc");
  }, [visibleCompanies, isReportMappingLoaded]);

  const setSortedColumn = (sortedCol) => {
    setSortedParameter(sortedCol);
  };

  useEffect(() => {
    if (sortedParameter) {
      setStyledRowsData(
        [...styledRowsData].sort((a, b) => {
          const aVal = a[sortedParameter]?.sortingValue;
          const bVal = b[sortedParameter]?.sortingValue;
          if (aVal == null && bVal == null) {
            return 0;
          } else if (aVal == null) {
            return 1;
          } else if (bVal == null) {
            return -1;
          }

          if (typeof aVal === "number" && typeof bVal === "number") {
            return sortedDirection === "asc" ? aVal - bVal : bVal - aVal;
          } else if (typeof aVal === "string" && typeof bVal === "string") {
            return sortedDirection === "asc"
              ? aVal.localeCompare(bVal)
              : bVal.localeCompare(aVal);
          } else {
            return 0;
          }
        })
      );
    }
  }, [sortedParameter, sortedDirection]);

  const stringContains = (target, query) => {
    const targetString = target.replace(/\s/g, "").toLowerCase();
    const queryString = query.replace(/\s/g, "").toLowerCase();
    return targetString.includes(queryString);
  };

  const tagsCompare = (comTags, selectedTag) => {
    return comTags.some((obj1) =>
      selectedTag.some((obj2) => obj1.id === obj2.id)
    );
  };

  useEffect(() => {
    let selectedTags = tags.filter((tag) => tag.selected) || tags;
    const selectedIndustryIdList = industryList
      .filter((ind) => ind.selected)
      .map((ind) => ind.industry_id);

    const selectedSeriesIdList = seriesList
      .filter((series) => series.selected)
      .map((series) => series.id);

    const newFilteredCompanies = allCompanies
      .filter((comp) => stringContains(comp.player_name, compSearchString))
      .filter((comp) =>
        selectedTags.length === 0 ? true : tagsCompare(comp.tags, selectedTags)
      )
      .filter((comp) =>
        selectedIndustryIdList.length === 0
          ? true
          : selectedIndustryIdList.includes(comp.industry_id)
      )
      .filter((comp) =>
        selectedSeriesIdList.length === 0
          ? true
          : selectedSeriesIdList.includes(comp.series)
      );
    setVisibleCompanies(newFilteredCompanies);
    setColumnsData(
      columnsData.map((col) => {
        return {
          ...col,
          value:
            col.key === "player"
              ? getNameHeader(newFilteredCompanies.length)
              : col.value,
        };
      })
    );
  }, [tags, compSearchString, industryList, seriesList]);

  // Fetching CP powerbi profiles
  useEffect(() => {
    const savedTreeData = JSON.parse(localStorage.getItem("windowTreeData"));
    const sectorCategoryNodes = savedTreeData?.[0]?.nodes;
    let newIndustryReportMapping = [];
    if (sectorCategoryNodes) {
      sectorCategoryNodes.forEach((category) => {
        category.nodes?.forEach((node) => {
          if (node.industry_id) {
            newIndustryReportMapping.push(node);
          }
        });
      });
      setIndReportMapping(newIndustryReportMapping);
    }

    // API CALL
    let newIndustryWiseCompanyReport = {};
    newIndustryReportMapping.forEach((indReportData) => {
      fetch(
        `${process.env.REACT_APP_API_ENDPOINT}/nodechildren/?key=${indReportData.key}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
        .then((res) => res.json())
        .then((res) => {
          if (res.length >= 1) {
            let oldReport =
              newIndustryWiseCompanyReport[indReportData.industry_id] || [];
            const newRes = res.map((obj) => ({
              ...obj,
              sectorNode: indReportData,
            }));
            let allReport = [...oldReport, ...newRes];
            newIndustryWiseCompanyReport[indReportData.industry_id] = allReport;
          }
        });
    });
    setIndWiseCompReport(newIndustryWiseCompanyReport);
    setReportMappingLoaded(true);
  }, []);

  return (
    <PageWrapper>
      <Header3 />
      <div>
        <div style={{ backgroundColor: "#FFFFFF", minHeight: "10vh" }}>
          <Header1>
            {currentInvestorId === selectedInvestor.id ? (
              <LHSTitlediv>
                <div>My Portfolio</div>
                <PortfolioName>{`(${currentInvestorName})`}</PortfolioName>
              </LHSTitlediv>
            ) : (
              <LHSTitlediv>
                {selectedInvestor.name || "My"} Portfolio
              </LHSTitlediv>
            )}
            <RHSTitlediv>
              {/* <NavigationRow onClick={() => navigate("/company-listing")}>
                <LinkText>View Platform Listing</LinkText>
                <LinkIcon>
                  <ArrowIcon />
                </LinkIcon>
              </NavigationRow> */}

              <Label>Select Portfolio:</Label>
              <MultiSelect
                options={allInvestor}
                keyFieldName="id"
                valueFieldName="name"
                onSelectedChange={onPortfolioChange}
                selectedOptions={[selectedInvestor]}
                mutileSelect={false}
                placeholder="Select Parameter.."
                width="250px"
                showCheckBox={false}
              />
            </RHSTitlediv>
          </Header1>
          <div
            style={{
              marginLeft: "3.5vw",
              marginBottom: "10px",
              color: "#4A4A4A",
            }}
          >
            <>
              {treeziparr?.map((obj, i) => (
                <BreadCrumbSpan
                  onClick={(e) => {
                    handleClickTree(i, obj.key);
                  }}
                  key={i}
                >
                  {obj.name} /{" "}
                </BreadCrumbSpan>
              ))}
            </>
          </div>
        </div>
        {/* <CompanySearch>
          <CompanyInputWrapper>
            <CompanyInput
              placeholder="Search Company Name"
              type="search"
              value={compSearchString}
              onChange={(e) => {
                setCompSearchString(e.target.value);
              }}
            ></CompanyInput>
            <SearchWrapper>
              <SearchIcon />
            </SearchWrapper>
          </CompanyInputWrapper>
        </CompanySearch> */}
        {/* <TagsWrapper>
          {tags.map((tag) => (
            <TagDiv
              id={tag.id}
              key={tag.id}
              selected={tag.selected}
              onClick={() => {
                onTagClick(tag.id);
              }}
            >
              {tag.name}
            </TagDiv>
          ))}
        </TagsWrapper> */}
        <GridWraper>
          {isPortfolioLoaded ? (
            <>
              <ShadowWrap>
                <CompanyTable
                  allParameters={columnsData}
                  rowsData={styledRowsData}
                  dataMonth={dataMonth}
                  industryList={industryList}
                  setSelectedIndustry={setSelectedIndustry}
                  seriesList={seriesList}
                  setSelectedSeries={setSelectedSeries}
                  sortedParameter={sortedParameter}
                  setSortedParameter={setSortedColumn}
                  sortedDirection={sortedDirection}
                  setSortedDirection={setSortedDirection}
                />
              </ShadowWrap>
            </>
          ) : (
            <Loader />
          )}
        </GridWraper>
      </div>
    </PageWrapper>
  );
};

export default Portfolio;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #f7fcff;
  min-height: 100vh;
`;

const CompanySearch = styled.div`
  display: flex;
  flex-direction: column;
  padding: 50px 0 30px;
  align-items: center;
  background-color: #ffffff;
`;

const CompanyInputWrapper = styled.div`
  display: flex;
  width: 60%;
  height: 60px;
  position: relative;
`;

const CompanyInput = styled.input`
  width: 100%;
  height: 100%;
  padding: 14px 32px 14px 56px;
  align-items: center;
  flex-shrink: 0;
  border-radius: 30px;
  color: #3c4043;
  font-family: Fira Sans;
  font-size: 20px;
  font-style: normal;
  font-weight: 500;
  line-height: normal;
  border: 1px solid var(--grey-dark, #d1d1d1);
  ::placeholder {
    color: #bfbfbf;
    font-size: 20px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
  :hover,
  :focus {
    border: 1px solid #09f;
    background: var(--white, #fff);
    outline: none;
  }
`;

const SearchWrapper = styled.div`
  display: flex;
  width: 24px;
  height: 24px;
  position: absolute;
  left: 22px;
  top: 18px;
`;

const TagsWrapper = styled.div`
  display: flex;
  width: 100%;
  padding: 24px 20% 24px;
  flex-wrap: wrap;
  background-color: #ffffff;
  justify-content: center;
`;

const TagDiv = styled.div`
  height: 28px;
  font-size: 12px;
  font-weight: 400;
  line-height: 16px;
  letter-spacing: 0em;
  text-align: left;
  padding: 6px 10px 6px 10px;
  background-color: ${(props) => (props.selected ? "#0099FF" : "#f2f2f2")};
  color: ${(props) => (props.selected ? "#FFFFFF" : "#262e40")};
  margin: 6px 6px;
  border-radius: 24px;
  cursor: pointer;
`;

const TooltipRow = styled.div`
  display: flex;
  flex-direction: row;
  height: 20px;
  align-items: center;
`;

const GridWraper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 3.5vw 0px;
`;

const ShadowWrap = styled.div`
  display: flex;
  flex-direction: column;
`;
const Header1 = styled.div`
  background-color: white;
  padding: 3.5vw;
  padding-top: 2vw;
  padding-bottom: 5px;
  display: flex;
  justify-content: space-between;
`;

const BreadCrumbSpan = styled.span`
  &:hover {
    color: #20a6ff;
    cursor: pointer;
  }
`;

const LHSTitlediv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  font-weight: 500;
  font-size: 30px;
`;

const PortfolioName = styled.div`
  font-weight: 500;
  font-size: 15px;
  padding: 8px 0px 0px 5px;
`;

const RHSTitlediv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 10px;
`;

const Label = styled.div`
  margin: 0 5px 0px 10px;
`;

const TableMenu = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  height: 56px;
  min-height: 56px;
  background-color: #ffffff;
  align-items: center;
`;

const TableMenuLeft = styled.div`
  margin-left: 24px;
`;

const TableMenuRight = styled.div`
  display: flex;
  align-items: center;
  padding: 0 20px;
`;

const SelectWraper = styled.div`
  margin-left: 10px;
`;

const NavigationRow = styled.div`
  display: flex;
  flex-direction: row;
`;

const LinkText = styled.div`
  color: #0099ff;
  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  line-height: 24px;
  cursor: pointer;
`;

const LinkIcon = styled.div`
  color: #0099ff;
  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  line-height: 24px;
  cursor: pointer;
`;

const CpProfileLink = styled.div`
  color: ${(props) => (props.islink === "true" ? "#0099FF" : "#000000")};
  cursor: ${(props) => (props.islink === "true" ? "pointer" : "default")};
  :hover {
    font-weight: ${(props) => (props.islink === "true" ? 500 : 400)};
  }
`;

const SpProfileLink = styled.div`
  color: ${(props) => (props.islink === "true" ? "#A0A6A9" : "#A0A6A9")};
  text-decoration: ${(props) =>
    props.islink === "true" ? "underline" : "none"};
  font-size: 12px;
  font-style: italic;
  font-weight: 350;
  line-height: 16px;
  letter-spacing: 0em;
  text-align: left;
  cursor: ${(props) => (props.islink === "true" ? "pointer" : "default")};
  :hover {
    font-weight: ${(props) => (props.islink === "true" ? 500 : 400)};
    color: ${(props) => (props.islink === "true" ? "#A0A0A0" : "#A0A6A9")};
  }
`;

const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

const HeaderText = styled.div`
  color: #444444;
  font-size: 16px;
  font-weight: 700px;
`;

const CountTextWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const CountText = styled.div`
  color: #444444;
  font-family: "Fira Sans";
  font-size: 16px;
  font-weight: 400;
  line-height: 24px;
  padding-left: 10px;
`;
