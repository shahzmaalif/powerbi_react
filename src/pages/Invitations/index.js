import styled from "styled-components";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import Header3 from "../../components/Header3/Header3";
import InvitationPage from "./InvitationPage";
import { HOME_URL } from "../../constants/constants";

const Invitations = (parent_props) => {
  const [selectedpage, setSelectedPage] = useState("Send Invitations");
  const [treeziparr, setTreeziparr] = useState([
    { key: 1, name: "Home" },
    { key: 2, name: "Send Invitations" },
  ]);
  const navigate = useNavigate();

  let handleClickTree = (index) => {
    if (treeziparr[index].key == 1) {
      navigate(HOME_URL)
    }
  };

  return (
    <div>
      <Header3 />
      <div style={{ backgroundColor: "#FFFFFF", minHeight: "10vh" }}>
        <Header1>
          <LHSTitlediv>{selectedpage}</LHSTitlediv>
          <RHSTitlediv></RHSTitlediv>
        </Header1>
        <div
          style={{
            marginLeft: "3.5vw",
            marginBottom: "10px",
            color: "grey",
          }}
        >
          <>
            {treeziparr?.map((obj, i) => (
              <BreadCrumbSpan
                onClick={(e) => {
                  handleClickTree(i, obj.key);
                }}
                key={i}
              >
                {obj.name} /{" "}
              </BreadCrumbSpan>
            ))}
          </>
        </div>
      </div>
      <PageContent>
        <InvitationPage />
      </PageContent>
    </div>
  );
};

export default Invitations;

const PageContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: #f7fcff;
  height: 83vh;
`;

const Header1 = styled.div`
  background-color: white;
  padding: 3.5vw;
  padding-top: 2vw;
  padding-bottom: 5px;
  display: flex;
  justify-content: space-between;
`;

const LHSTitlediv = styled.div`
  font-weight: 500;
  font-size: 30px;
`;

const RHSTitlediv = styled.div`
  display: flex;
  gap: 10px;
`;

const BreadCrumbSpan = styled.span`
  &:hover {
    color: #20a6ff;
    cursor: pointer;
  }
`;
