import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
const DemoTable = () => {

    const [data,setdata] = useState([])
    const [tododata, settododata] = useState([])

    useEffect(()=>{
        fetchData()
        fetchtodoData()
    }, [])

    const fetchData = async() =>{
        const response = await fetch('https://jsonplaceholder.typicode.com/users');
        const jsondata = await response.json();
        console.log(jsondata)
        setdata(jsondata)
    }

    const fetchtodoData = async() =>{
        const response  =await fetch('https://jsonplaceholder.typicode.com/todos');
        const jsondata = await response.json()
        console.log(jsondata)
        settododata(jsondata)
    }

    const completed = (id)=>{
        // console.log(id)
        let count = 0
        for(let i=0;i<tododata.length; i++){
            // console.log(tododata.userId)
            if(tododata[i].userId === id){
                if (tododata[i].completed===true)
                {count = count+1}
            }
        }
        return count
    }

  return (
    <div>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>UserName</th>
                    <th>Email</th>
                    <th>Completed</th>
                </tr>
            </thead>
            <tbody>
                {data.map((item)=>(
                    <tr key={item.id}>
                        <td>{item.id}</td>
                        <td>{item.name}</td>
                        <td>{item.username}</td>
                        <td>{item.email}</td>
                        <td>{completed(item.id)}</td>
                    </tr>
                ))}
            </tbody>
        </table>

    </div>
  )
}

export default DemoTable